package friskakarlstad.com.pojos;

public class Credentials {
	
	// Username for the 
	private String userName;
    private String password;
    private String sessionId;
    private String projectId;
    private String argument1;
    private String argument2;
    
    public Credentials() {
    	super();
    }
    
    public Credentials(String userName, String password, String sessionId, String projectId) {
		super();
		setUserName(userName);
		setPassword(password);
		setPassword(sessionId);
		setProjectId(projectId);
	}
    
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getArgument1() {
		return argument1;
	}

	public void setArgument1(String argument1) {
		this.argument1 = argument1;
	}

	public String getArgument2() {
		return argument2;
	}

	public void setArgument2(String argument2) {
		this.argument2 = argument2;
	}
}
