package friskakarlstad.com.pojos;

import java.io.Serializable;
import java.util.Date;

public class MapObject implements Serializable {

    private String mapName;
    private String mapLink;
    private Date timeStamp;

    public MapObject() {
   }

    public MapObject(String mapName, String mapLink) {
        super();
        setMapName(mapName);
        setMapLink(mapLink);
    }

	public String getMapName() {
		return mapName;
	}

	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

	public String getMapLink() {
		return mapLink;
	}

	public void setMapLink(String mapLink) {
		this.mapLink = mapLink;
	}

    public Date getTimeStamp() {
        return timeStamp;
    }
    
    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}