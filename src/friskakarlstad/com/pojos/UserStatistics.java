package friskakarlstad.com.pojos;

/**
 * Class that represents a statistics object for a specific user. Used to display lists of user results.
 * 
 * @author jonast42@yahoo.com
 */
public class UserStatistics implements Comparable {

	// This users username.
	private String userName;
	
	// The total number of controls this user has registered.
	private int numberOfControlsRegistered;
	
	// Indicates whether the user wants its result published or not. If not, do not
	// include this users result in the statistics.
	private boolean wantsToBePublished;
	
	// The type of user account the user has. E,g, privatperson, tieto, other company.
	private String userCompanyName;
	
	
	/**
	 * Constructor for class UserStatistic
	 */
	public UserStatistics() {
		
	} // end UserStatistic()


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public int getNumberOfControlsRegistered() {
		return numberOfControlsRegistered;
	}


	public void setNumberOfControlsRegistered(int numberOfControlsRegistered) {
		this.numberOfControlsRegistered = numberOfControlsRegistered;
	}


	public boolean isWantsToBePublished() {
		return wantsToBePublished;
	}


	public void setWantsToBePublished(boolean wantsToBePublished) {
		this.wantsToBePublished = wantsToBePublished;
	}


	public String getUserCompanyName() {
		return userCompanyName;
	}


	public void setUserCompanyName(String userCompanyName) {
		this.userCompanyName = userCompanyName;
	}
	
	@Override
	public int compareTo(Object another) {
		
        if (Integer.valueOf(this.numberOfControlsRegistered) == Integer.valueOf( ((UserStatistics)another).numberOfControlsRegistered) ) {
            return 0;
        }
        else if (Integer.valueOf(this.numberOfControlsRegistered) > Integer.valueOf( ((UserStatistics)another).numberOfControlsRegistered) ) {
            return 1;
        }
        else {
            return -1;
        }
	}
} // end class UserStatistic
