package friskakarlstad.com.pojos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class RegisteredMobileUser implements Serializable {
	//Project
    private String projectName;
    private String projectDescription;
    private Integer controlInfosVersion;
	private MapObject mapObject;
	// User
	private String firstName;
	private String lastName;
	private String address;
	private String postNr;
	private String postAddress;
	private String role;
	private String phoneNumber;
	private String mobilePhoneNumber;
	private String userName;
	private String password;
	private String age;
    private String timeStamp;
	private Boolean wantsNewsLetter;
	private Boolean wantsToBePublished;
	private String sessionId;
	private String email;
	private Boolean codeHasBeenVerified;

	private ArrayList<ControlInfo> controlInfos;
	private ArrayList<NewsFlash> newsFlashes;
	private ArrayList<Company> companies;
	private ArrayList<Control> controls;
	private ArrayList<Control> registerFailedControls;

    public RegisteredMobileUser() {
    }

	public MapObject getMapObject() {
		return mapObject;
	}

	public void setMapObject(MapObject mapObject) {
		this.mapObject = mapObject;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectDescription() {
		return projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	public Integer getControlInfosVersion() {
		return controlInfosVersion;
	}

	public void setControlInfosVersion(Integer controlInfosVersion) {
		this.controlInfosVersion = controlInfosVersion;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPostNr() {
		return postNr;
	}

	public void setPostNr(String postNr) {
		this.postNr = postNr;
	}

	public String getPostAddress() {
		return postAddress;
	}

	public void setPostAddress(String postAddress) {
		this.postAddress = postAddress;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}

	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public Boolean getWantsNewsLetter() {
		return wantsNewsLetter;
	}

	public void setWantsNewsLetter(Boolean wantsNewsLetter) {
		this.wantsNewsLetter = wantsNewsLetter;
	}

	public Boolean getWantsToBePublished() {
		return wantsToBePublished;
	}

	public void setWantsToBePublished(Boolean wantsToBePublished) {
		this.wantsToBePublished = wantsToBePublished;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getCodeHasBeenVerified() {
		return codeHasBeenVerified;
	}

	public void setCodeHasBeenVerified(Boolean codeHasBeenVerified) {
		this.codeHasBeenVerified = codeHasBeenVerified;
	}

	public ArrayList<ControlInfo> getControlInfos() {
		if (controlInfos == null) {
			controlInfos = new ArrayList<ControlInfo>();
		}
		return controlInfos;
	}

	public void setControlInfos(ArrayList<ControlInfo> controlInfos) {
		this.controlInfos = controlInfos;
		
	}

	public ArrayList<NewsFlash> getNewsFlashes() {
		if (newsFlashes == null) {
			newsFlashes = new ArrayList<NewsFlash>();
		}
		return newsFlashes;
	}

	public void setNewsFlashes(ArrayList<NewsFlash> newsFlashes) {
		this.newsFlashes = newsFlashes;
	}

	public ArrayList<Company> getCompanies() {
		return companies;
	}

	public void setCompanies(ArrayList<Company> companies) {
		this.companies = companies;
	}

	public ArrayList<Control> getControls() {
		if (controls == null) {
			controls = new ArrayList<Control>();
		}
		return controls;
	}

	public void setControls(ArrayList<Control> controls) {
		this.controls = controls;
	}

	public ArrayList<Control> getRegisterFailedControls() {
		if (registerFailedControls == null) {
			registerFailedControls = new ArrayList<Control>();
		}
		return registerFailedControls;
	}

	public void setRegisterFailedControls(ArrayList<Control> registerFailedControls) {
		this.registerFailedControls = registerFailedControls;
	}
    
	
}