package friskakarlstad.com.pojos;

import java.io.Serializable;
import java.util.Date;

public class Control implements Serializable, Comparable<Control> {

    private String controlNumber;
    private Date timeStamp;
    private String clientType;
    private String projectName;

    public Control() {
    }

    public Control(String controlNumber, Date timeStamp, String clientType, String projectName) {
        this.controlNumber = controlNumber;
        this.timeStamp = timeStamp;
        this.clientType = clientType;
        this.projectName = projectName;
    }

        
    public Date getTimeStamp() {
        return timeStamp;
    }
    public void setTimeStamp(Date newTimeStamp) {
    	this.timeStamp = newTimeStamp;
    }

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getControlNumber() {
		return controlNumber;
	}

	public void setControlNumber(String controlNumber) {
		this.controlNumber = controlNumber;
	}
	
	@Override
	public int compareTo(Control another) {
		
        if (Integer.valueOf(this.controlNumber) == Integer.valueOf(another.controlNumber)) {
            return 0;
        }
        else if (Integer.valueOf(this.controlNumber) > Integer.valueOf(another.controlNumber)) {
            return 1;
        }
        else {
            return -1;
        }
	}
	
	@Override
	public boolean equals(Object other) {
		
		if (this == other) {
			return true;
		}
	    if ( (other == null) || (other.getClass() !=  this.getClass()) ) {
	        return false;
	    }
	    Control otherControl = (Control)other;
		// TODO Complete this with all attributes of the class, not only controlNumber
	    return this.controlNumber.equals(otherControl.controlNumber);
	} // end equals
	
	
	@Override
	public int hashCode(){
		int hash = 7;
		hash = 31 * hash + Integer.valueOf(controlNumber);
		//hash = 31 * hash + (null == controlNumber ? 0 : controlNumber.hashCode());
		return hash;
	} // end hashCode
}