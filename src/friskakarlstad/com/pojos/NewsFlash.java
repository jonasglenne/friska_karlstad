package friskakarlstad.com.pojos;

import java.io.Serializable;
import java.util.Date;

public class NewsFlash implements Serializable, Comparable {
    private String type;
    private String header;
    private String description;
    private Date timeStamp;
    
    public NewsFlash() {
    }

    public NewsFlash(int newsFlashId, String type, String header, String description, Date timeStamp) {
        setType(type);
        setHeader(header);
        setDescription(description);
        setTimeStamp(timeStamp);
    }

    // Accessors for the fields. JPA doesn't use these, but your application does.

    
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}
	
	@Override
	public int compareTo(Object another) {
        return timeStamp.compareTo( ((NewsFlash)another).timeStamp );
	}
        
}

