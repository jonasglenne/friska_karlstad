package friskakarlstad.com.utilities;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import friskakarlstad.com.pojos.Company;
import friskakarlstad.com.pojos.Control;
import friskakarlstad.com.pojos.ControlInfo;
import friskakarlstad.com.pojos.MapObject;
import friskakarlstad.com.pojos.NewsFlash;
import friskakarlstad.com.pojos.RegisteredMobileUser;

/**
 * Converts an xml object to a pojo for the class RegisteredMobileUser
 * 
 * @author jonast42@yahoo.com
 *
 */
public class RegisteredUserXmlHandler {

	private SAXParserFactory saxXmlfactory = SAXParserFactory.newInstance();
	private SAXParser xmlSaxParser;
	
	
	/*
	 * Constructor for class XmlParser
	 */
	public RegisteredUserXmlHandler() {
		
	} // end constructor XmlParser()
	
	
	public RegisteredMobileUser parseXml(String xmlString) {
		
		RegisteredMobileUser compiledRegisteredMobileUser = null;
		
		try {
			xmlSaxParser = saxXmlfactory.newSAXParser();
			XmlHandler theXmlHandler = new XmlHandler();
			xmlSaxParser.parse(new InputSource (new StringReader(xmlString)), theXmlHandler);
			compiledRegisteredMobileUser = theXmlHandler.getMobileUnregisteredUserObject();
			}
		catch (ParserConfigurationException e) {
		}
		catch (SAXException e) {
		}
		catch (IOException e) {
		}
		
		return compiledRegisteredMobileUser;
	} // end method parseXml()
	
	
	/**
	 * Inner class that handles the actual parsing of the xml data.
	 * 
	 * @author jonast42@yahoo.com
	 */
	protected class XmlHandler extends DefaultHandler {
		
		private RegisteredMobileUser mobileUser;
		private MapObject mapObject;
		private ArrayList<ControlInfo> controlInfos;
		private ControlInfo currentControlnfo;
		private ArrayList<Control> controls;
		private Control currentControl;
		private ArrayList<NewsFlash> newsFlashes;
		private NewsFlash currentNewsFlash;
		private ArrayList<Company> companies;
		private Company currentCompany;
	    private StringBuilder builder;
    	private SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	    
	    // To be able to differ between elements that have identical sub nodes e.g. <id>
	    private String currentElement;
	    
	    /**
	     * Constructor for class XMLHandler
	     */
	    protected XmlHandler() {
	    	currentElement = "None";
	    } // end constructor XmlHandler()
		
	    public RegisteredMobileUser getMobileUnregisteredUserObject() {
	    	return mobileUser;
	    } // end getMobileUnregisteredUserObject()
	    
	    
		@Override
	    public void characters(char[] ch, int start, int length) throws SAXException {
	        super.characters(ch, start, length);
	        builder.append(ch, start, length);
	    } // end characters()
		

	    @Override
	    public void endElement(String uri, String localName, String name) throws SAXException {
	    	
	        super.endElement(uri, localName, name);
	        if (this.mobileUser != null){
	        	
	            if (localName.equalsIgnoreCase("projectName")){
	            	if (currentElement.equals("Control")) {
	            		currentControl.setProjectName(builder.toString());
	            	}
	            	else {
	            		mobileUser.setProjectName(builder.toString());
	            	}
	            } 
	            else if (localName.equalsIgnoreCase("projectDescription")){
	            	mobileUser.setProjectDescription(builder.toString());
	            } 
	            else if (localName.equalsIgnoreCase("controlInfosVersion")){
	            	mobileUser.setControlInfosVersion(Integer.valueOf(builder.toString()));
	            }  
	            else if (localName.equalsIgnoreCase("mapObject")){
	            	mobileUser.setMapObject(mapObject);
	            }  
	            else if (localName.equalsIgnoreCase("mapName")){
	            	mapObject.setMapName(builder.toString());
	            }  
	            else if (localName.equalsIgnoreCase("mapLink")){
	            	mapObject.setMapLink(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("mapObject")){
	            	// No support for multiple map objects currently
	            }
	            else if (localName.equalsIgnoreCase("controlNumber")){
	            	if (currentElement.equals("Control")) {
	            		currentControl.setControlNumber(builder.toString());
	            	}
	            	if (currentElement.equals("ControlInfo")) {
	            		currentControlnfo.setControlNumber(builder.toString());
	            	}
	            }  
	            else if (localName.equalsIgnoreCase("code")){
	            	currentControlnfo.setControlCode(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("latitude")){
	            	currentControlnfo.setLatitude(Double.valueOf(builder.toString()));
	            }
	            else if (localName.equalsIgnoreCase("longitude")){
	            	currentControlnfo.setLongitude(Double.valueOf(builder.toString()));
	            }
	            else if (localName.equalsIgnoreCase("imageLink")){
	            	currentControlnfo.setImageLink(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("webLink")){
	            	currentControlnfo.setWebLink(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("color")){
	            	currentControlnfo.setControlColor(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("controlPoint")){
	            	currentControlnfo.setControlPoint(Integer.valueOf(builder.toString()));
	            }
	            else if (localName.equalsIgnoreCase("startDate")){
	            	currentControlnfo.setStartDate(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("endDate")){
	            	currentControlnfo.setEndDate(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("availableTo")){
	            	currentControlnfo.setAvailableTo(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("lightControlInfos")){
	            	mobileUser.setControlInfos(controlInfos);
	            }
	            else if (localName.equalsIgnoreCase("lightControlInfo")){
	            	controlInfos.add(currentControlnfo);
	            }
	            else if (localName.equalsIgnoreCase("type")){
	            	currentNewsFlash.setType(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("header")){
	            	currentNewsFlash.setHeader(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("description")){
	            	if (currentElement.equals("NewsFlash")) {
	            		currentNewsFlash.setDescription(builder.toString());
	            	}
	            	if (currentElement.equals("ControlInfo")) {
	            		currentControlnfo.setDescription(builder.toString());
	            	}
	            	
	            }
	            else if (localName.equalsIgnoreCase("timeStamp")){
	            	// Parse the timestamp which is stored as a string on the server.

            		Date serverTimeStamp = null;
            		try {
            			serverTimeStamp = serverDateFormat.parse(builder.toString());

            			if (currentElement.equals("NewsFlash")) {
    						currentNewsFlash.setTimeStamp(serverTimeStamp);
    	            	}
    	            	if (currentElement.equals("MapObject")) {
    	            		// TODO Check that this really works. This handling requires that the timestamps arrive in time
    	            		if (mapObject.getTimeStamp() == null) {
    	            			mapObject.setTimeStamp(serverTimeStamp);
    	            		}
    	            	}
    	            	if (currentElement.equals("Control")) {
    	            		currentControl.setTimeStamp(serverTimeStamp);
    	            	}	
					} catch (ParseException e) {
						e.printStackTrace();
					}
	            }
	            else if (localName.equalsIgnoreCase("lightNewsFlashes")){
	            	mobileUser.setNewsFlashes(newsFlashes);
	            }
	            else if (localName.equalsIgnoreCase("lightNewsFlash")){
	            	newsFlashes.add(currentNewsFlash);
	            }
	            else if (localName.equalsIgnoreCase("id")){
	            	if (currentElement.equals("MapObject")) {
	            	}
	            	if (currentElement.equals("Company")) {
	            	}
	            	if (currentElement.equals("Control")) {
	            		currentControlnfo.setProjectId(builder.toString());
	            	}
	            }
	            else if (localName.equalsIgnoreCase("companyName")){
	            	currentCompany.setCompanyName(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("company")){
	            	companies.add(currentCompany);
	            }
	            else if (localName.equalsIgnoreCase("companies")){
	            	mobileUser.setCompanies(companies);
	            }
	            else if (localName.equalsIgnoreCase("clientType")){
	            	currentControl.setClientType(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("firstName")){
	            	mobileUser.setFirstName(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("lastName")){
	            	mobileUser.setLastName(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("address")){
	            	mobileUser.setAddress(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("postNr")){
	            	mobileUser.setPostNr(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("postAddress")){
	            	mobileUser.setPostAddress(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("role")){
	            	mobileUser.setRole(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("phoneNumber")){
	            	mobileUser.setPhoneNumber(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("mobilePhoneNumber")){
	            	mobileUser.setMobilePhoneNumber(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("userName")){
	            	mobileUser.setUserName(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("password")){
	            	mobileUser.setPassword(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("age")){
	            	mobileUser.setAge(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("timeStamp")){
	            	mobileUser.setTimeStamp(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("wantsNewsLetter")){
	            	mobileUser.setWantsNewsLetter(Boolean.valueOf(builder.toString()));
	            }
	            else if (localName.equalsIgnoreCase("wantsToBePublished")){
	            	mobileUser.setWantsToBePublished(Boolean.valueOf(builder.toString()));
	            }
	            else if (localName.equalsIgnoreCase("codeHasBeenVerified")) {
	            	mobileUser.setCodeHasBeenVerified(Boolean.valueOf(builder.toString()));
	            }
	            else if (localName.equalsIgnoreCase("sessionId")){
	            	mobileUser.setSessionId(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("email")){
	            	mobileUser.setEmail(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("control")){
	            	controls.add(currentControl);
	            }
	            else if (localName.equalsIgnoreCase("controls")){
	            	mobileUser.setControls(controls);
	            }

	            builder.setLength(0); 
	        }
	    } // end endElement()

	    
	    @Override
	    public void startDocument() throws SAXException {
	        super.startDocument();
	        builder = new StringBuilder();
	    } // end startDocument()

	    
	    @Override
	    public void startElement(String uri, String localName, String name,
	            Attributes attributes) throws SAXException {
	        super.startElement(uri, localName, name, attributes);
	        if (localName.equalsIgnoreCase("registeredMobileObject")){
	            this.mobileUser = new RegisteredMobileUser();
	        }
	        else if (localName.equalsIgnoreCase("mapObject")){
	            this.mapObject = new MapObject();
	            currentElement = "MapObject";
	        }
	        else if (localName.equalsIgnoreCase("controls")) {
	        	this.controls = new ArrayList<Control>();
	        }
	        else if (localName.equalsIgnoreCase("control")) {
	        	this.currentControl = new Control();
	            currentElement = "Control";
	        }
	        else if (localName.equalsIgnoreCase("lightControlInfos")) {
	        	this.controlInfos = new ArrayList<ControlInfo>();
	        }
	        else if (localName.equalsIgnoreCase("lightControlInfo")) {
	        	this.currentControlnfo = new ControlInfo();
	            currentElement = "ControlInfo";
	        }
	        else if (localName.equalsIgnoreCase("lightNewsFlashes")) {
	        	this.newsFlashes= new ArrayList<NewsFlash>(); 
	        }
	        else if (localName.equalsIgnoreCase("companies")) {
	        	this.companies = new ArrayList<Company>(); 
	        }
	        else if (localName.equalsIgnoreCase("lightNewsFlash")) {
	        	this.currentNewsFlash= new NewsFlash(); 
	            currentElement = "NewsFlash";
	        }

	        else if (localName.equalsIgnoreCase("company")) {
	        	this.currentCompany = new Company(); 
	            currentElement = "Company";
	        }
	    }
	} // end inner class XmlHandler
	
} // end class XmlParser
