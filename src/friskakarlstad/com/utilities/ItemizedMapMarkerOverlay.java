package friskakarlstad.com.utilities;

import java.util.ArrayList;



import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;

import friskakarlstad.com.activities.RegisterControlView;

/**
 * Subclass to ItemizedOverlay. Implemented to handle onTap events where we want
 * to send the user to the correct registration form for the selected control.
 * 
 * @author jonast42@yahoo.com
 */

public class ItemizedMapMarkerOverlay extends ItemizedOverlay<OverlayItem> {

	// An arraylist to hold all OverlayItems.
	private ArrayList<OverlayItem> mOverlays;
	
	// The Context in which this MarkerOverlay exists.
	private Context appContext;
	
	
    /**
	 * Constructor for class MapItemsOverlay.
	 * 
	 * @param defaultMarker a Drawable that is the marker to point out where the user is
	 * @param context a Context which is the context in which this class is operating.
	 */
	public ItemizedMapMarkerOverlay(Drawable defaultMarker, Context theContext) {
		super(boundCenter(defaultMarker));
		appContext = theContext;
		mOverlays = new ArrayList<OverlayItem>();
	} // end MapItemsOverlay()

	/**
	 * Adds an OverlayItem to the ArrayList of overlays.
	 * 
	 * @param overlay the OverlayItem to add
	 */
	public void addOverlay(OverlayItem overlay) {
		//overlay.setMarker(boundCenter(overlay.getMarker(0)));
	    mOverlays.add(overlay);
	} // end addOverlay()

	
	public Drawable centerOverlay(Drawable drawableToCenter) {
		return boundCenter(drawableToCenter);
	}
	
	
	public void populateItemizedOverlay() {
		populate();		
	} // end populateItemizedOverlay()
	
	
	@Override
	protected OverlayItem createItem(int i) {
		return mOverlays.get(i);
	} // end createItem()
	
	
	// Overridden for the only reason to remove shadow for markers.
	@Override
	public void draw(Canvas arg0, MapView arg1, boolean arg2) {
		super.draw(arg0, arg1, false);
	}


	@Override
	protected boolean onTap(int index) {
		// Start the view used to scan the control QR-code.
		Intent scanControlIntent = new Intent(appContext, RegisterControlView.class);
		scanControlIntent.putExtra("control_number", mOverlays.get(index).getTitle());
		appContext.startActivity(scanControlIntent);
		return super.onTap(index);
	} // end onTap()
	
	
	@Override
	public int size() {
		return mOverlays.size();
	} // end size()

} // end class MapItemsOverlay
