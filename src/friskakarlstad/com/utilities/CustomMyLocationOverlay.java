package friskakarlstad.com.utilities;

import android.content.Context;
import android.util.Log;

import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;

// This class subclasses (extends) MyLocationOverlay so that 
// we can override its dispatchTap method
// to handle tap events on the present location dot.

public class CustomMyLocationOverlay extends MyLocationOverlay {

	private static String CLASSNAME = "CustomMyLocationOverlay";
    private Context context;

    public CustomMyLocationOverlay(Context context, MapView mapView) {
        super(context, mapView);
        this.context = context;   
    }

    protected boolean dispatchTap(){
    	Log.i(CLASSNAME, "Dispatching");
    	// More to add later
        return false;
    }
} // end class CustomMyLocationOverlay