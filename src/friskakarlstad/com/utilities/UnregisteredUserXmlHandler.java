package friskakarlstad.com.utilities;

import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import friskakarlstad.com.pojos.Company;
import friskakarlstad.com.pojos.Control;
import friskakarlstad.com.pojos.ControlInfo;
import friskakarlstad.com.pojos.MapObject;
import friskakarlstad.com.pojos.NewsFlash;
import friskakarlstad.com.pojos.UnregisteredMobileUser;


import android.util.Log;


/**
 * Converts xml-data to an object of class UnregisteredMobileUser representing an unregistered user.
 * 
 * @author jonast42@yahoo.com
 */
public class UnregisteredUserXmlHandler {

	private SAXParserFactory saxXmlfactory = SAXParserFactory.newInstance();
	private SAXParser xmlSaxParser;
	
	
	/*
	 * Constructor for class XmlParser
	 */
	public UnregisteredUserXmlHandler() {
	} // end constructor XmlParser()
	
	
	public UnregisteredMobileUser parseXml(String xmlString) {
		
		UnregisteredMobileUser compiledMobileUnregisteredUser = null;
		
		try {
			xmlSaxParser = saxXmlfactory.newSAXParser();
			XmlHandler theXmlHandler = new XmlHandler();
			xmlSaxParser.parse(new InputSource (new StringReader(xmlString)), theXmlHandler);
			compiledMobileUnregisteredUser = theXmlHandler.getMobileUnregisteredUserObject();
			}
		catch (Exception e) {
			Log.i("UnregisteredUserXMLHandler", "Something went wrong: " + e);
		}
		
		return compiledMobileUnregisteredUser;
	} // end method parseXml()
	
	
	/**
	 * Inner class that handles the actual parsing of the xml data.
	 * 
	 * @author jge
	 */
	protected class XmlHandler extends DefaultHandler {
		
		private UnregisteredMobileUser mobileUser;
		private MapObject mapObject;
		private ControlInfo currentControlInfo;
		private ArrayList<ControlInfo> controlInfos;
		private Control currentControl;
		private ArrayList<NewsFlash> newsFlashes;
		private NewsFlash currentNewsFlash;
		private ArrayList<Company> companies;
		private Company currentCompany;
	    private StringBuilder builder;
    	private SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

	    // To be able to differ between elements that have identical sub nodes e.g. <id>
	    private String currentElement;
	    
	    protected XmlHandler() {
	    	currentElement = "None";
	    }
		
	    public UnregisteredMobileUser getMobileUnregisteredUserObject() {
	    	return mobileUser;
	    }
	    
		@Override
	    public void characters(char[] ch, int start, int length) throws SAXException {
	        super.characters(ch, start, length);
	        builder.append(ch, start, length);
	    }

	    @Override
	    public void endElement(String uri, String localName, String name)
	            throws SAXException {
	        super.endElement(uri, localName, name);
	        if (this.mobileUser != null){
	            if (localName.equalsIgnoreCase("projectName")){
	            		mobileUser.setProjectName(builder.toString());
	            } 
	            else if (localName.equalsIgnoreCase("projectDescription")){
	            	mobileUser.setProjectDescription(builder.toString());
	            } 
	            else if (localName.equalsIgnoreCase("controlInfosVersion")){
	            	mobileUser.setControlInfosVersion(Integer.valueOf(builder.toString()));
	            }  
	            else if (localName.equalsIgnoreCase("mapObject")){
	            	mobileUser.setMapObject(mapObject);
	            }  
	            else if (localName.equalsIgnoreCase("mapName")){
	            	mapObject.setMapName(builder.toString());
	            }  
	            else if (localName.equalsIgnoreCase("mapLink")){
	            	mapObject.setMapLink(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("mapObject")){
	            	// No support for multiple map objects currently
	            }
	            else if (localName.equalsIgnoreCase("controlNumber")){
	            		currentControlInfo.setControlNumber(builder.toString());
	            }  
	            else if (localName.equalsIgnoreCase("code")){
	            	currentControlInfo.setControlCode(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("latitude")){
	            	currentControlInfo.setLatitude(Double.valueOf(builder.toString()));
	            }
	            else if (localName.equalsIgnoreCase("longitude")){
	            	currentControlInfo.setLongitude(Double.valueOf(builder.toString()));
	            }
	            else if (localName.equalsIgnoreCase("imageLink")){
	            	currentControlInfo.setImageLink(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("webLink")){
	            	currentControlInfo.setWebLink(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("color")){
	            	currentControlInfo.setControlColor(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("controlPoint")){
	            	currentControlInfo.setControlPoint(Integer.valueOf(builder.toString()));
	            }
	            else if (localName.equalsIgnoreCase("startDate")){
	            	currentControlInfo.setStartDate(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("endDate")){
	            	currentControlInfo.setEndDate(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("availableTo")){
	            	currentControlInfo.setAvailableTo(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("lightControlInfos")){
	            	mobileUser.setControls(controlInfos);
	            }
	            else if (localName.equalsIgnoreCase("lightControlInfo")){
	            	controlInfos.add(currentControlInfo);
	            }
	            else if (localName.equalsIgnoreCase("type")){
	            	currentNewsFlash.setType(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("header")){
	            	currentNewsFlash.setHeader(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("description")){
	            	if (currentElement.equals("NewsFlash")) {
	            		currentNewsFlash.setDescription(builder.toString());
	            	}
	            	if (currentElement.equals("ControlInfo")) {
	            		currentControlInfo.setDescription(builder.toString());
	            	}
	            }
	            else if (localName.equalsIgnoreCase("timeStamp")){
	            	// Parse the timestamp which is stored as a string on the server.

            		Date serverTimeStamp = null;
            		try {
            			serverTimeStamp = serverDateFormat.parse(builder.toString());

            			if (currentElement.equals("NewsFlash")) {
    						currentNewsFlash.setTimeStamp(serverTimeStamp);
    	            	}
    	            	if (currentElement.equals("MapObject")) {
    	            		mapObject.setTimeStamp(serverTimeStamp);
    	            	}
					} catch (ParseException e) {
						e.printStackTrace();
					}
	            }
	            else if (localName.equalsIgnoreCase("clientType")){
	            	currentControl.setClientType(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("lightNewsFlashes")){
	            	mobileUser.setNewsFlashes(newsFlashes);
	            }
	            else if (localName.equalsIgnoreCase("lightNewsFlash")){
	            	newsFlashes.add(currentNewsFlash);
	            }

	            else if (localName.equalsIgnoreCase("id")){
	            	if (currentElement.equals("MapObject")) {
	            	}
	            	if (currentElement.equals("Company")) {
	            	}
	            	if (currentElement.equals("ControlInfo")) {
	            		currentControlInfo.setProjectId(builder.toString());
	            	}
	            }
	            else if (localName.equalsIgnoreCase("companyName")){
	            	currentCompany.setCompanyName(builder.toString());
	            }
	            else if (localName.equalsIgnoreCase("company")){
	            	companies.add(currentCompany);
	            }
	            else if (localName.equalsIgnoreCase("companies")){
	            	mobileUser.setCompanies(companies);
	            }
	            builder.setLength(0); 
	        }
	    }

	    @Override
	    public void startDocument() throws SAXException {
	        super.startDocument();
	        builder = new StringBuilder();
	    }

	    @Override
	    public void startElement(String uri, String localName, String name,
	            Attributes attributes) throws SAXException {
	        super.startElement(uri, localName, name, attributes);
	        if (localName.equalsIgnoreCase("unregisteredMobileObject")){
	            this.mobileUser = new UnregisteredMobileUser();
	        }
	        else if (localName.equalsIgnoreCase("mapObject")){
	            this.mapObject = new MapObject();
	            currentElement = "MapObject";
	        }
	        else if (localName.equalsIgnoreCase("lightControlInfos")) {
	        	this.controlInfos = new ArrayList<ControlInfo>();
	        }
	        else if (localName.equalsIgnoreCase("lightControlInfo")) {
	        	this.currentControlInfo = new ControlInfo();
	            currentElement = "ControlInfo";

	        }
	        else if (localName.equalsIgnoreCase("lightNewsFlashes")) {
	        	this.newsFlashes= new ArrayList<NewsFlash>(); 
	        }
	        else if (localName.equalsIgnoreCase("companies")) {
	        	this.companies = new ArrayList<Company>(); 
	        }
	        else if (localName.equalsIgnoreCase("lightNewsFlash")) {
	        	this.currentNewsFlash= new NewsFlash(); 
	            currentElement = "NewsFlash";
	        }

	        else if (localName.equalsIgnoreCase("company")) {
	        	this.currentCompany = new Company(); 
	            currentElement = "Company";
	        }
	    }
	} // end inner class XmlHandler
	
} // end class UnregisteredUserXmlHandler
