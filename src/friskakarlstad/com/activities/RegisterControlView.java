package friskakarlstad.com.activities;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import friskakarlstad.com.R;
import friskakarlstad.com.pojos.Control;
import friskakarlstad.com.pojos.ControlInfo;
import friskakarlstad.com.pojos.RegisteredMobileUser;
import friskakarlstad.com.pojos.UnregisteredMobileUser;
import friskakarlstad.com.utilities.FileHandler;
import friskakarlstad.com.utilities.RestClient;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Class that handles validation and registration of a control.
 *
 * @author jonast42@yahoo.com
 */
public class RegisterControlView extends Activity {
	
	/**************************************************************
	 * Result codes for different methods called within the Activity.
	 **************************************************************/
	
	// Dialog id if it is found out that the control is already scanned.
	static final int DIALOG_ALREADY_SCANNED_ID = 0;

	// Dialog id if it is found out that the control is already stored on the server.
	static final int DIALOG_ALREADY_STORED_ID = 1;

	// An id for a progress dialog shown during post to server.
	static final int PROGRESS_DIALOG = 2;

	// And id for an alert dialog shown when attempting to register a control for an unregistered user.
	static final int USER_NOT_REGISTERED = 4;
	
	// Id to handle the problem if a user does not have the QR-code reader installed.
	static final int BARCODE_SCANNER_NOT_INSTALLED = 5;
	
	// Id to handle problem if control has been already registered before this registration attempt.
	static final int CONTROL_ALREADY_REGISTERED = 6;
	
	// Id to handle problem if control is not valid in some way.
	static final int INVALID_CONTROL = 7;
	
	static final int RESULT_WAS_OK = 8;
	
	// Keep this result if no contact could be made with the server.
	static final int RESULT_UNDEFINED = -1;
	
	/**************************************************************
	 * Widgets for this activity.
	 **************************************************************/
	
	// Heading for this view.
	private TextView tvResultScanHeading;
	
	// Heading for control number.
	private TextView tvControlNumberHeading;
	
	// Displays the control number.
	private TextView tvControlNumber;

	// Heading for control code
	private TextView tvControlCodeHeading;
	
	// Allows the user to fill in a control code manually.
	private EditText tvControlCode;
	
	// Shows the description for the location of the control.
	private TextView tvControlDescriptionHeading;
	
	// Shows the description for the location of the control.
	private TextView tvControlDescription;

	// A Picture of the company that sponsor the scanned control.
	private ImageView ivCompanyCommImage;
	
	// Posts the scanned control to server.
	private Button btnScan;
	
	/**************************************************************
	 * Utility attributes for this activity.
	 **************************************************************/
	
	// Communicates via the REST-API on google app-engine.
	private RestClient restClient;
	
	// The list of controls which have been registered.
	private List<Control> controlList;
	
	// The list of controlInfos for this project.
	private List<ControlInfo> controlInfoList;
	
	// Progress dialog for lengthy operations.
	private ProgressDialog requestProgressDialog;
	
	// The control id of the currently selected control.
	private String selectedControlNumber;
	
	// The time stamp when this control was scanned or typed in manually.
	private String timeStamp;

	// Flag to inidicate whether the user has the correct qr-code scanner installed.
	private boolean scannerAppInstalled;

	private SharedPreferences sharedPreferences;
	
	// The user name for the user, Used to indicate whether the user is registered or not.
	private String userName;
	
	// An object representing a registered user.
	private RegisteredMobileUser registeredUser;
	
	// This variable will hold the weblink the user launches when clickin on the advertisement
	// if any.
	private Uri webLinkUriUrl;
	
	private boolean hasBeenScanned;
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_control_view_layout);

        restClient = new RestClient();
        scannerAppInstalled = true; // Assume the user has this installed.
        
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
	    userName = sharedPreferences.getString("username", null);
	    
        // Load the control list from file for check agains entered/scanned control id and control code.
        try {
        	if (userName != null) {
        		registeredUser = (RegisteredMobileUser)FileHandler.getObjectFromFile(openFileInput(FileHandler.REGISTERED_USER_FILENAME));
        		controlList = registeredUser.getControls();
        		controlInfoList = registeredUser.getControlInfos();
        		Collections.sort(controlInfoList);
        	}
        	else {
        		UnregisteredMobileUser unregisteredUser = 
        				(UnregisteredMobileUser)FileHandler.getObjectFromFile(openFileInput(FileHandler.UNREGISTERED_USER_FILENAME));
        		controlInfoList = unregisteredUser.getControlInfos();
        	}
		} 
        catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}   
        
        tvResultScanHeading = (TextView) findViewById(R.id.tvResultScanHeading);    
        
        tvControlNumberHeading = (TextView) findViewById(R.id.tvControlNumberHeading);
        tvControlNumber = (TextView) findViewById(R.id.tvControlNumber);
    	
        tvControlCodeHeading = (TextView) findViewById(R.id.tvControlCodeHeading);
    	tvControlCode = (EditText) findViewById(R.id.tvControlCode);
    	tvControlCode.addTextChangedListener(new TextWatcher(){
			@Override
            public void afterTextChanged(Editable s) {}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
				// If the user have filled in the control manually, change the text on the button to
				// allow the user to store the control on the server.
				if (tvControlCode.getText().toString().length() == 2) {
					btnScan.setText(getString(R.string.register_control_str));
					// Indicate that this controlled has not been scanned but entered manually.
					//hasBeenScanned = false;
				}
				else {
					btnScan.setText(getString(R.string.scan_control_str));
				}
			}
    	});
    	
        if (savedInstanceState != null) {
        	tvControlCode.setText(savedInstanceState.getString("entered_code"));
        }
    	
        tvControlDescriptionHeading = (TextView) findViewById(R.id.tvControlDescriptionHeading);
        tvControlDescription = (TextView) findViewById(R.id.tvControlDescription);
        
        
        // This button will start up the scanner to scan a control.
        btnScan = (Button) findViewById(R.id.btnScan);
        btnScan.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	executeButtonClick(btnScan.getText().toString());
            }
        });
    	
        // If this view is created with an extra it means that the user selected a control
        // from the map or the control list view. In these cases, the control id will be 
        // fed in the extras to this view. Use this id to fill in the information about 
        // the control but leave the control code out. If the user selected the scan activity
        // directly from the main menu. The control id cannot be determined until after the
        // scan, then start the scan view directly.
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
	    	selectedControlNumber = extras.getString("control_number");
	    	updateControlInfoData(selectedControlNumber);
        }
        
	    // Add possible advertisement in the imageview.
    	ivCompanyCommImage = (ImageView) findViewById(R.id.ivCompanyCommImage);
    	
    	// Retrieve the selected control from the control infos list.
    	for (ControlInfo currentControlInfo : controlInfoList) {
    		
	    	if ( selectedControlNumber.equals(currentControlInfo.getControlNumber()) ) {
    			// If there is any link to the web advertisement, add a click handler
    			// that will open the link in a browser.
    			if (!currentControlInfo.getWebLink().equals("")) {

    				webLinkUriUrl = Uri.parse(currentControlInfo.getWebLink());
    				
    				ivCompanyCommImage.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							Intent launchBrowser = new Intent(Intent.ACTION_VIEW, webLinkUriUrl);
							startActivity(launchBrowser);
						}
					});
    			}
    			// And load the advertisement image.
				new AsyncLoadAdvertisement().execute(currentControlInfo.getImageLink());
			}
    	}
	} // end onCreate()
	
	
	/**
	 * Selects correct method when pressing the button. What method to call is dependent on the 
	 * text of the button which represents the state of the selected control (scan or store).
	 */
	private void executeButtonClick(String buttonText) {

		if (buttonText.equals(getString(R.string.scan_control_str))) {
			startScanView();
		}
		else {
	    	// Check that the user is registered.
	    	if (sharedPreferences.getString("username", null) != null) {
	    		
	    		// Check that the code matches.
	    		boolean controlCodeMatches = validateControlCode();
	    		
	    		if (controlCodeMatches == true) {
					new StoreToServerTask().execute();
				}
	    		else {
	    			showToast("Felaktig kontrollkod angiven. Prova igen att fylla i eller eller scanna kontrollen");
	    			tvControlCode.requestFocus();
	    		}
	    	}
			else {
	    		// If the user is not registered, they have to do it first.
	    		showDialog(USER_NOT_REGISTERED);
	    	}
		}
	} // end executeButtonClick()

	
	/**
	 * Asynch Task Class that uses worker thread to download an advertisement image
	 * asynchronously. The image link from where to load the advertisment is specified 
	 * in the controlinfo. If no link exist load the default logo for friska karlstad.
	 */
	private class AsyncLoadAdvertisement extends AsyncTask<String, Void, Drawable> {

		@Override
		protected void onPreExecute() {} // end onPreExecute()
	
	    @Override
		protected Drawable doInBackground(String... imageUrl) {

	    	Drawable advertisementDrawable = null;
		    
	    	if (!imageUrl[0].equals("")) {
		    	InputStream inputStream = null;
		    	URL imageURL = null;
				try {
					imageURL = new URL(imageUrl[0]);
					
					inputStream = (InputStream)imageURL.getContent();
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
				
				if (inputStream != null) {
			    	advertisementDrawable = Drawable.createFromStream(inputStream, "advertiser");	
				}
	    	}
			
	    	return advertisementDrawable;
	    } // end doInBackground()
	    
	    @Override
	    protected void onProgressUpdate(Void... values) {}
	    
	    @Override
	    protected void onPostExecute(Drawable image) {
	    	if (image == null) {
	    		ivCompanyCommImage.setImageResource(R.drawable.register_image_default);
	    	}
	    	else {
		    	ivCompanyCommImage.setImageDrawable(image);
	    	}
	    } // end onPostExecute()

	} // end class StoreToServerTask
	
	
	/**
	 * Starts a view to allow scanning of a control. The view will return the scan result.
	 */
	private void startScanView() {
    	
		// Use the ZXING client to scan the control
		try {
	        Intent scanIntent = new Intent("com.google.zxing.client.android.SCAN");
	        scanIntent.setPackage("com.google.zxing.client.android");
	        scanIntent.putExtra("SCAN_MODE", "QR_CODE_MODE");
	        startActivityForResult(scanIntent, 0);
		}
		catch (ActivityNotFoundException e) {
			scannerAppInstalled = false;
			showDialog(BARCODE_SCANNER_NOT_INSTALLED);
			e.printStackTrace();
		}
	} // end startScanView()
    
    
    /**
     * Check that the control code is correct before storing it on the server. This check is carried 
     * out against the local control list.
     * 
     * @return a boolean indicating that everything is ok with the selected control or not.
     */
    private boolean validateControlCode() {
    	//Always assume the worst.
    	boolean controlOk = false;
    	
    	for (ControlInfo currentControlInfo : controlInfoList) {
    		
    		if ( selectedControlNumber.equals(currentControlInfo.getControlNumber()) ) {
    			if ( currentControlInfo.getControlCode().equalsIgnoreCase((tvControlCode.getText().toString())) ) {
    				controlOk = true;
    			}
    		}
    	}
		return controlOk;
    } // end validateControlCode()

    
    /**
     * Registers a control, first on GAE and then on Stamford. The reason to store in
     * GAE is that it is easier to undo the changes to GAE than Stamford if one of them
     * fails.
     * 
     * @return a boolean that indicates if everything went ok on both servers.
     */
    private int storeControlOnServer() {
  
    	int result = RESULT_UNDEFINED;
    	
   		if (userName == null) {
   			startActivity(new Intent(this, LoginOrRegisterView.class));
   		}
   		else {
 			Control controlToRegister = new Control(selectedControlNumber, new Date(), "Android", RestClient.PROJECT_NAME);
   			
 			String password = sharedPreferences.getString("password", null);
   			
	    	// If the control registration could be stored on to the server, update the local list and the number of controls taken.
 			result = restClient.registerControl( controlToRegister, userName, password );
 			
 			// Regardless of if we could store the control on the server successfully we want
 			// to save it to the users controls because the control still counts and we
 			// want to update the number of registered controls for the user.
 			registeredUser.getControls().add(controlToRegister);
 			
 			if (result == RESULT_UNDEFINED) {
 				registeredUser.getRegisterFailedControls().add(controlToRegister);
 			}
 			
 			try {
				FileHandler.writeObjectToFile(registeredUser, openFileOutput(FileHandler.REGISTERED_USER_FILENAME, Context.MODE_PRIVATE));
			} 
    		catch (FileNotFoundException e) {
				e.printStackTrace();
			}
    		
 			// Update the number of registered control for the display.
    		int controlsTaken = sharedPreferences.getInt("controls_taken", 0);
        	SharedPreferences.Editor editor = sharedPreferences.edit();
        	editor.putInt("controls_taken", ++controlsTaken);
        	editor.commit();
   		}
   		
   		return result;
    } // end storeControlOnServers()
	
	
	/**
	 * Fills in the latest available data from the selected controlinfo into 
	 * the view and checks if the user has already registered or stored the 
	 * control code locally in this method.
	 */
	private void updateControlInfoData(String controlNumber) {
		
		ControlInfo selectedControlInfo = null;

		for (ControlInfo currentControlInfo : controlInfoList) {
			if (controlNumber.equals(currentControlInfo.getControlNumber())) {
				selectedControlInfo = currentControlInfo;
				break;
			}
		}
		
		tvControlNumber.setText(selectedControlInfo.getControlNumber());
        tvControlDescription.setText(selectedControlInfo.getDescription());
		
        if (!selectedControlInfo.getImageLink().equals("")) {
			new AsyncLoadAdvertisement().execute(selectedControlInfo.getImageLink());
    	}
        
        boolean alreadyRegisteredControl = false;
        
        if (sharedPreferences.getString("username", null) != null) {
	        
        	for (Control currentControl : controlList) {
	        	if(selectedControlNumber.equals(currentControl.getControlNumber())) {
	        		alreadyRegisteredControl = true;
	        		break;
	        	}
	        }
        }
        
		if (alreadyRegisteredControl) {
			tvControlCode.setText(selectedControlInfo.getControlCode());
			tvControlCode.setEnabled(false);
			
			btnScan.setText(getString(R.string.control_already_registered_str));
			btnScan.setEnabled(false);
			btnScan.setBackgroundColor(Color.GRAY);
			btnScan.setTextColor(Color.DKGRAY);
		}
		else {
			btnScan.setText(getString(R.string.scan_control_str));
			// Remove the code so the user has to be at the control to scan it.
			//tvControlCode.setText("");
		}
	} // updateControlInfoData()
	
	
	// TODO: This will probably need update. Check if it can be replaced with a call to updateControlInfoData()
	/**
	 * Checks the status in the local control list based on a control id (e.g. 1-100).
	 */
	private void checkControlStatusAfterScan(String scannedCode) {
		
		hasBeenScanned = true;
		ControlInfo selectedControlInfo = null;

		for (ControlInfo currentControlInfo : controlInfoList) {
			if (selectedControlNumber.equals(currentControlInfo.getControlNumber())) {
				selectedControlInfo = currentControlInfo;
				break;
			}
		}

		tvControlNumber.setText(selectedControlInfo.getControlNumber());
        tvControlDescription.setText(selectedControlInfo.getDescription());
		tvControlCode.setText(scannedCode);
		btnScan.setText(getString(R.string.register_control_str));
	} // end checkControlStatusAfterScan()

	
	/**
	 * If the user attempts to register a control without being registred, a view to allow
	 * new registration or synch with existing account at Stamford should be shown.
	 */
	private void openLoginOrRegisterUserView() {
		Intent registerUserIntent = new Intent(this, LoginOrRegisterView.class);
		startActivity(registerUserIntent);
	} // end openLoginOrRegisterUserView()
	

	@Override
	protected Dialog onCreateDialog(int id) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		
		switch(id) {
        
        case PROGRESS_DIALOG:
        	requestProgressDialog = new ProgressDialog(this);
        	requestProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        	requestProgressDialog.setMessage("Registrerar kontroll...");
            return requestProgressDialog;
            
        case RESULT_UNDEFINED:
        	builder.setMessage("Kontrollen kunde inte registreras hos Friska Karlstad pga dålig täcknng. Den kommer nu " + 
        					   "att sparas lokalt i telefonen. Programmet kommer att skicka kontrollen till Friska " +
        					   "Karlstad vid nästa omstart och du behöver inte göra någon mer åtgärd. Skulle dock problemet " +
        					   "kvarstå, kontakta Friska Karlstads support.")
        	       .setCancelable(false)
        	       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	        	   dialog.cancel();
        	           }
        	       });
        	break;
        case RESULT_WAS_OK:
        	builder.setMessage("Kontrollen registrerad på Friska Karlstad!.")
        	       .setCancelable(false)
        	       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	        	   dialog.cancel();
        	           }
        	       });
        	break;
        case CONTROL_ALREADY_REGISTERED:
        	builder.setMessage("Kontrollen har redan registrerats.")
        	       .setCancelable(false)
        	       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	        	   dialog.cancel();
        	           }
        	       });
        	break;
        case INVALID_CONTROL:
        	builder.setMessage("Ogiltig kontroll.")
        	       .setCancelable(false)
        	       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	        	   dialog.cancel();
        	           }
        	       });
        	break;
        case USER_NOT_REGISTERED:
        	builder.setMessage("Du är inte registrerad hos Friska Karlstad ännu. Tryck på OK om du vill gå till registrering.")
        	       .setCancelable(true)
        	       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	        	   dialog.cancel();
        	        	   openLoginOrRegisterUserView();
        	           }
        	       }).setNegativeButton("Avbryt", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	        	   dialog.cancel();
        	           }
        	       });
        	break;
        case BARCODE_SCANNER_NOT_INSTALLED:
        	builder.setMessage("Du saknar applikationen för scanning. Den är gratis och finns på Android Market. Vill du installera det?")
 	       .setCancelable(true)
 	       .setPositiveButton("Ja", new DialogInterface.OnClickListener() {
 	           public void onClick(DialogInterface dialog, int id) {
 	        	   dialog.cancel();			
 	        	   Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.zxing.client.android"));
 				   startActivity(marketIntent);
 				   finish();
 	           }
 	       }).setNegativeButton("Nej", new DialogInterface.OnClickListener() {
 	           public void onClick(DialogInterface dialog, int id) {
 	        	   dialog.cancel();
 	           }
 	       });
        	break;
		default:
            break;
		}
        return builder.create();
    } // end onCreateDialog()
	

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {
			
	        if (resultCode == RESULT_OK) {
	        	// Set the scan code to the current content read by the scanner.
	            String scanCode = intent.getStringExtra("SCAN_RESULT");
	        	checkControlStatusAfterScan(scanCode);
	        } 
	        else if (resultCode == RESULT_CANCELED) {
	            // If this activity was started directly from main menu. Return there on cancel.
	        	//if (getIntent().getExtras() == null) {
	        		if (scannerAppInstalled == true) {
		        		finish();
	        		}
	        	//}
	        }
	    }
	} // end onActivityResult()

	
    /**
     * Displays a toast message on screen
     * @param toastText a String that is the text to display.
     */
    private void showToast(String toastText) {
    	int duration = Toast.LENGTH_LONG;
    	Toast toast = Toast.makeText(this, toastText, duration);
    	toast.show();
    } // end showToast()
    
    
    @Override
    protected void onRestart() {
    	super.onRestart();
        // Load the control list from file for check agains entered/scanned control id and control code.
        
    	userName = sharedPreferences.getString("username", null);
    	
    	if (hasBeenScanned == false) {
	    	try {
	        	if (userName != null) {
	        		
	        	    registeredUser = 
	        	    		(RegisteredMobileUser)FileHandler.getObjectFromFile(openFileInput(FileHandler.REGISTERED_USER_FILENAME));
	        		controlList = registeredUser.getControls();
	        		controlInfoList = registeredUser.getControlInfos();
	        		Collections.sort(controlInfoList);
	        		updateControlInfoData(selectedControlNumber);
	        	}
	        	else {
	        		UnregisteredMobileUser unregisteredUser = 
	        				(UnregisteredMobileUser)FileHandler.getObjectFromFile(openFileInput(FileHandler.UNREGISTERED_USER_FILENAME));
	        		controlInfoList = unregisteredUser.getControlInfos();
	        	}
			} 
	        catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}   
    	}
    } // end onRestart()


	/**
     * Class used to contact the server asynchronously in a separate thread from the 
     * UI-thread to register a control.
     */
    private class StoreToServerTask extends AsyncTask<Void, Void, Integer> {
    	
    	@Override
		protected void onPreExecute() {
    		showDialog(PROGRESS_DIALOG);
        } // end onPreExecute()

        @Override
		protected Integer doInBackground(Void... params) {
        	return storeControlOnServer();
        	// TODO: Since storeControlOnServers now contain updates previously done in updateControlStatus. Can this be removed and the asynctask still work? Try!
        	
        } // end doInBackground()
		
        protected void onProgressUpdate() {}

        protected void onPostExecute(Integer result) {
        	dismissDialog(PROGRESS_DIALOG);
        	
			if (result == RestClient.RESULT_WAS_OK) {
 				showDialog(RESULT_WAS_OK);
	    	}
 			else if (result == RestClient.CONTROL_ALREADY_REGISTERED) {
    			showDialog(CONTROL_ALREADY_REGISTERED);
 			}
 			else if (result == RestClient.INVALID_CONTROL) {
    			showDialog(INVALID_CONTROL);
 			}
 			else if (result == RESULT_UNDEFINED) {
    			showDialog(RESULT_UNDEFINED);
 			}
			
        	// Update the control info widgets to reflect the stored control.
        	updateControlInfoData(selectedControlNumber);
        } // end onPostExecute()
        
    } // end class StoreToServerTask
	
    
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			
			if (requestProgressDialog != null && requestProgressDialog.isShowing()) {
				requestProgressDialog.dismiss();
			}
			finish();
		}
		return super.onKeyDown(keyCode, event);
	} // end onKeyDown()
	
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.register_control_options_menu, menu);
        return true;
    } // end onCreateOptionsMenu()
    
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        	case R.id.register_control_options_help:
    			Intent browserIntent = new Intent(this, InformationView.class);
    			browserIntent.putExtra("browser_url", "file:///android_asset/reg_contr_menu_help.html");
    			startActivity(browserIntent);
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
        }
    } // end onOptionsItemSelected()
	
    
	/**
	 * Save the code entered in the text field if the views destroy method is called, for example when the user
	 * rotates the screen.
	 * 
	 * @param savedInstanceState a Bundle that will be passed to this method on destroy method call.
	 */
	@Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
		savedInstanceState.putString("entered_code", tvControlCode.getText().toString());
		super.onSaveInstanceState(savedInstanceState);
	} // end onSaveInstanceState()
	
	
} // end class ScannerView