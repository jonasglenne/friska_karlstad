package friskakarlstad.com.activities;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import friskakarlstad.com.R;
import friskakarlstad.com.pojos.Company;
import friskakarlstad.com.pojos.Control;
import friskakarlstad.com.pojos.MapObject;
import friskakarlstad.com.pojos.RegisteredMobileUser;
import friskakarlstad.com.pojos.UnregisteredMobileUser;
import friskakarlstad.com.utilities.FileHandler;
import friskakarlstad.com.utilities.RestClient;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class RegisterUserView extends Activity {
	
	/**************************************************************
	 * Result codes for different methods called within the Activity.
	 **************************************************************/
	
	// Identifier for the dialog shown if user already exists.
	static final int USER_ALREADY_REGISTERED = 1;
	
	// Identifier for the dialog shown if user did not accept PUL.
	static final int PUL_NOT_ACCEPTED = 2;
	
	// Identifier for the dialog shown to ask if the user already have an account
	// at Friska Karlstad. In this case it is possible to synchronize with this accound
	// rather than register again.
	static final int USER_HAS_ACCOUNT_QUERY = 3;
	
	static final int MANDATORY_FIELD_NOT_FILLED_IN = 4;
	
	static final int USER_REGISTRATION_FAILED = 5;
	
	static final int EMAIL_EXISTS_ALREADY = 6;
	
	static final int PASSWORD_TOO_SHORT = 7;
	
	static final int INCORRECT_AGE_FORMAT = 8;
	
	static final int AN_ACCOUNT_ALREADY_EXISTS = 9;
	
	/**************************************************************
	 * Widgets for this activity.
	 **************************************************************/
	
	// Heading for the user registration view.
	private TextView tvRegisterUserHeading;
	
	// Informs the user that some of the fields are required to be filled in
	// to be able to register.
	private TextView tvFieldsRequiredInfo;
	
	// Heading for the real name input EditText.
	private TextView tvRegisterFirstNameHeading;

	// Enter the real name here.
	private EditText etRegisterFirstName;

	// Heading for the real name input EditText.
	private TextView tvRegisterLastNameHeading;

	// Enter the real name here.
	private EditText etRegisterLastName;
	
	// Heading for the address input EditText.
	private TextView tvRegisterAddressHeading;

	// Enter the real name here.
	private EditText etRegisterAddress;

	// Heading for the address input EditText.
	private TextView tvRegisterPostalCodeHeading;

	// Enter the real name here.
	private EditText etRegisterPostalCode;

	// Heading for the address input EditText.
	private TextView tvRegisterPostalAddressHeading;

	// Enter the real name here.
	private EditText etRegisterPostalAddress;

	// Heading for the phone number input EditText.
	private TextView tvRegisterEmailAddress;

	// Enter the real name here.
	private EditText etEmailAddress;
	
	// Heading for the phone number input EditText.
	private TextView tvRegisterPhoneHeading;

	// Enter the real name here.
	private EditText etRegisterPhone;

	// Heading for the phone number input EditText.
	private TextView tvRegisterMobilePhoneHeading;

	// Enter the real name here.
	private EditText etRegisterMobilePhone;
	
	// Heading for the user name input EditText.
	private TextView tvRegisterUserNameHeading;

	// Enter the real name here.
	private EditText etRegisterUserName;
	
	// Heading for the password input EditText.
	private TextView tvRegisterPasswordHeading;

	// Enter the real name here.
	private EditText etRegisterPassword;
	
	// Heading for the password input EditText.
	private TextView tvRegisterBirthHeading;

	// Enter the real name here.
	private EditText etRegisterBirth;

	// Heading for the spinner where the user selects which user type he or she is.
	private TextView tvUserType;
	
	// Lets the user select which type of account this user should have.
	private Spinner spUserType;
	
	// Checkbox to determine whether user want newsletter or not.
	private CheckBox cbWantsNewsLetter;

	// Checkbox to determine whether user wants to accept storing personal informaiton according to PUL.
	private CheckBox cbPulAcceptance;

	// Checkbox to determine whether user wants to publish its results.
	private CheckBox cbPublish;
	
	// Registers the user
	private Button registerButton;
	
	//Clears the edit text fields.
	private Button clearFieldsButton;
	
	// Cancels the registration
	private Button cancelButton;
	
	/**************************************************************
	 * Utility attributes for this activity.
	 **************************************************************/
	
	// Communicates via the REST-API on google app-engine.
	private RestClient restClient;

	// ArrayAdapter used for the user type spinner.
	private ArrayAdapter<CharSequence> spinnerAdapter;

	// List of companies and organisations the user can select from.
	private ArrayList<Company> companyList;

	// Reference to shared preferences for this application.
	private SharedPreferences sharedPreferences;
	
	// A boolean that indicates that the user has answered yes to the question in the dialog
	// if it is ok to overwrite existing account on login.
	private boolean isOverWriteOk;

	private ProgressDialog progressDialog;
	
	// The project for an unregistered user. We need this object to retrieve the map object for the new user.
	UnregisteredMobileUser unregisteredUser;
	//The project for a registered user we need this object if the user already has an account on the phone.
	// Then we fetch control infos from this object. If it is not present, we fetch control infos from the unregistered
	// user object.
	RegisteredMobileUser registeredUser;
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_user_view_layout);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        restClient = new RestClient();
        
    	// Load the project from file to get the project name and the company list.
		try {
			// Check whether the user already has an account on the phone or not. Get the companies from either object but the correct one.
			if ( sharedPreferences.getString("username", null) != null ) {
				registeredUser = (RegisteredMobileUser)FileHandler.getObjectFromFile(openFileInput(FileHandler.REGISTERED_USER_FILENAME));
				companyList = registeredUser.getCompanies();
			}
			unregisteredUser = (UnregisteredMobileUser)FileHandler.getObjectFromFile(openFileInput(FileHandler.UNREGISTERED_USER_FILENAME));
			companyList = unregisteredUser.getCompanies();
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
        
        tvRegisterUserHeading = (TextView) findViewById(R.id.tvRegisterUserHeading);
    	
        tvFieldsRequiredInfo = (TextView) findViewById(R.id.tvFieldsRequiredInfo);
        tvRegisterFirstNameHeading = (TextView) findViewById(R.id.tvRegisterFirstNameHeading);
    	etRegisterFirstName = (EditText) findViewById(R.id.etRegisterFirstName);
    	tvRegisterLastNameHeading = (TextView) findViewById(R.id.tvRegisterLastNameHeading);
    	etRegisterLastName = (EditText) findViewById(R.id.etRegisterLastName);
    	tvRegisterAddressHeading = (TextView) findViewById(R.id.tvRegisterAddressHeading);
    	etRegisterAddress = (EditText) findViewById(R.id.etRegisterAddress);
    	tvRegisterPostalCodeHeading = (TextView) findViewById(R.id.tvRegisterPostalCodeHeading);
    	etRegisterPostalCode = (EditText) findViewById(R.id.etRegisterPostalCode);
    	tvRegisterPostalAddressHeading = (TextView) findViewById(R.id.tvRegisterPostalAddressHeading);
    	etRegisterPostalAddress = (EditText) findViewById(R.id.etRegisterPostalAddress);

    	tvRegisterEmailAddress = (TextView) findViewById(R.id.tvRegisterEmailAddressHeading);
    	etEmailAddress = (EditText) findViewById(R.id.etRegisterEmailAddress);
    	
    	tvRegisterPhoneHeading = (TextView) findViewById(R.id.tvRegisterPhoneHeading);
    	etRegisterPhone = (EditText) findViewById(R.id.etRegisterPhone);
    	tvRegisterMobilePhoneHeading = (TextView) findViewById(R.id.tvRegisterMobilePhoneHeading);
    	etRegisterMobilePhone = (EditText) findViewById(R.id.etRegisterMobilePhone);
    	
    	tvRegisterUserNameHeading = (TextView) findViewById(R.id.tvRegisterUserNameHeading);
    	etRegisterUserName = (EditText) findViewById(R.id.etRegisterUserName);
    	tvRegisterPasswordHeading = (TextView) findViewById(R.id.tvRegisterPasswordHeading);
    	etRegisterPassword = (EditText) findViewById(R.id.etRegisterPassword);

    	tvRegisterBirthHeading = (TextView) findViewById(R.id.tvRegisterBirthHeading);
    	etRegisterBirth = (EditText) findViewById(R.id.etRegisterBirth);
    	
    	tvUserType = (TextView) findViewById(R.id.tvUserType);
    	
    	spUserType = (Spinner) findViewById(R.id.spUserType);
    	spinnerAdapter = new ArrayAdapter<CharSequence>(this, android.R.layout.simple_spinner_item);
    	spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spUserType.setAdapter(spinnerAdapter);
        
    	// Add selectable user categories to the list.
    	if (companyList != null) {
	    	for (Company currentCompany : companyList) {
	    		spinnerAdapter.add(currentCompany.getCompanyName());
	    	}
	    	spUserType.setSelection(0);
	    }
    	
    	
    	cbWantsNewsLetter = (CheckBox) findViewById(R.id.cbWantsNewsLetter);
    	cbPulAcceptance = (CheckBox) findViewById(R.id.cbPulAcceptance);
    	cbPublish = (CheckBox) findViewById(R.id.cbPublishUserResults);
    	cbPublish.setChecked(true);
    	
    	registerButton = (Button) findViewById(R.id.btnRegister);
    	registerButton.setOnClickListener(new OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		new RegisterUserAsynchTask().execute();
        	} // end onClick()
        });

    	clearFieldsButton = (Button) findViewById(R.id.btnClearFields);
    	clearFieldsButton.setOnClickListener(new OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		clearFields();
        	} // end onClick()
        });

    	cancelButton = (Button) findViewById(R.id.btnCancelRegistration);
    	cancelButton.setOnClickListener(new OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		cancelRegistration();
        	} // end onClick()
        });
    	
        if (savedInstanceState != null) {
        	isOverWriteOk =  savedInstanceState.getBoolean("user_accepted_overwrite");
            
            etRegisterFirstName.setText(savedInstanceState.getString("registerview_firstname"));
            etRegisterLastName.setText(savedInstanceState.getString("registerview_lastname"));
            etRegisterAddress.setText(savedInstanceState.getString("registerview_streeaddress"));
            etRegisterPostalCode.setText(savedInstanceState.getString("registerview_postalcode"));
            etRegisterPostalAddress.setText(savedInstanceState.getString("registerview_postaladdress"));
            etEmailAddress.setText(savedInstanceState.getString("registerview_email"));
            etRegisterPhone.setText(savedInstanceState.getString("registerview_homephone"));
            etRegisterMobilePhone.setText(savedInstanceState.getString("registerview_mobphone"));
            etRegisterUserName.setText(savedInstanceState.getString("registerview_username"));
            etRegisterPassword.setText(savedInstanceState.getString("registerview_password"));
            etRegisterBirth.setText(savedInstanceState.getString("registerview_birth"));
            spUserType.setSelection(savedInstanceState.getInt("registerview_usertype_choice") - 1);
            cbWantsNewsLetter.setChecked(savedInstanceState.getBoolean("news_letter"));
            cbPulAcceptance.setChecked(savedInstanceState.getBoolean("pul_acceptance"));
            cbPublish.setChecked(savedInstanceState.getBoolean("publish_result_ok"));
       }
    	
    	checkForExistingAccount();
        
	} // end onCreate()

	
	/**
	 * Asynch Task to create a user on the server.
	 * 
	 * @author jonast42@yahoo.com
	 */
	private class RegisterUserAsynchTask extends AsyncTask<Void, Void, Integer> {   	
		
		@Override
		protected void onPreExecute() {
	    	lockScreenRotation();
			progressDialog = new ProgressDialog(RegisterUserView.this);
			progressDialog.setTitle("Friska Karlstad");
			progressDialog.setMessage("Registrering genomförs");
			progressDialog.show();
		} // end onPreExecute()
		
		@Override
		protected Integer doInBackground(Void... params) {
			int result = 5;
			try {
				result = registerUser();
			}
			catch (Exception e ) {
				progressDialog.dismiss();
			}
			return result;
		}
		
	    protected void onPostExecute(Integer result) {
	    	progressDialog.cancel();
	    	if (result == 0) {
				Intent data = new Intent();
				data.putExtra("new_register_result", true);
				setResult(RESULT_OK, data);
				finish();
	    	}
	    	else {
				showDialog(result);
	    	}
			unlockScreenRotation();
	    } // end onPostExecute()
	} // end class RegisterUserAsynchTask
	
	
	/**
	 * Checks if an account already exists on the phone (indicated by an attempt to retrieve
	 * a string value for key username in the shared preferences return non null.
	 * In this case warn the user that login will overwrite existing information locally 
	 * in the phone and replace with the account stored at the server.
	 */
	private void checkForExistingAccount() {

		if ( (sharedPreferences.getString("username", null) != null) && isOverWriteOk == false) {
			showDialog(AN_ACCOUNT_ALREADY_EXISTS);
		}
	} // end checkForExistingAccount()

	
	/**
	 * Registers a user using the information supplied by the user at the GAE server.
	 * 
	 * @return a boolean that indicates whether the registration at GAE was successful
	 */
	private int registerUser() throws Exception {
		
		int registrationResult = 0;

		// Check that the user has filled in the fields correctly through the checks below. If not, assign a fault code
		// to the result code of this method.
		if (etRegisterFirstName.getText().toString().equals("") || etRegisterLastName.getText().toString().equals("") ||
			etRegisterAddress.getText().toString().equals("") || etRegisterPostalCode.getText().toString().equals("") ||
			etRegisterUserName.getText().toString().equals("") || etRegisterPassword.getText().toString().equals("") ||
			etRegisterBirth.getText().toString().equals("")) {
			registrationResult = MANDATORY_FIELD_NOT_FILLED_IN;
		}
		else if (etRegisterPassword.getText().length() < 4) {
			registrationResult = PASSWORD_TOO_SHORT;
		}
		else if (etRegisterBirth.getText().toString().length() != 4){
			// TODO Add textwatcher checking that the input is a number in oncreate
			registrationResult = INCORRECT_AGE_FORMAT;
		}
		else if (!cbPulAcceptance.isChecked()) {
			registrationResult = PUL_NOT_ACCEPTED;
		}
		else {
			StringBuilder registerUserXMLbody = new StringBuilder();
			
			registerUserXMLbody.append("<user>");
			registerUserXMLbody.append("<firstName>" + etRegisterFirstName.getText().toString() + "</firstName>");
			registerUserXMLbody.append("<lastName>" + etRegisterLastName.getText().toString() + "</lastName>");
			registerUserXMLbody.append("<address>" + etRegisterAddress.getText().toString()+ "</address>");
			registerUserXMLbody.append("<postNr>" + etRegisterPostalCode.getText().toString()+ "</postNr>");
			registerUserXMLbody.append("<postAddress>" + etRegisterPostalAddress.getText().toString()+ "</postAddress>");
			registerUserXMLbody.append("<role>NormalUser</role>");
			registerUserXMLbody.append("<phoneNumber>" + etRegisterPhone.getText().toString()+ "</phoneNumber>");
			registerUserXMLbody.append("<mobilePhoneNumber>" + etRegisterMobilePhone.getText().toString()+ "</mobilePhoneNumber>");
			registerUserXMLbody.append("<userName>" + etRegisterUserName.getText().toString()+ "</userName>");
			registerUserXMLbody.append("<password>" + etRegisterPassword.getText().toString()+ "</password>");
			registerUserXMLbody.append("<age>" + etRegisterBirth.getText().toString() + "</age>");
			registerUserXMLbody.append("<email>" + etEmailAddress.getText().toString()+ "</email>");
			registerUserXMLbody.append("<wantsNewsLetter>" + cbWantsNewsLetter.isChecked() + "</wantsNewsLetter>");
			registerUserXMLbody.append("<wantsToBePublished>" + cbPublish.isChecked() + "</wantsToBePublished>");
			registerUserXMLbody.append("<sessionId></sessionId>");
			registerUserXMLbody.append("<projectName>" + RestClient.PROJECT_NAME + "</projectName>");
			registerUserXMLbody.append("<companyName> " + spinnerAdapter.getItem(spUserType.getSelectedItemPosition()) + " </companyName>");
			registerUserXMLbody.append("</user>");

			registrationResult = restClient.registerUser(registerUserXMLbody.toString());
			
			if (registrationResult == 0) {
				saveCreatedUserToFile();
			}
			else if (registrationResult == RestClient.USER_ALREADY_EXIST) {
				registrationResult = USER_ALREADY_REGISTERED;
 			}
			else {
				registrationResult = USER_REGISTRATION_FAILED;
			}
		}
		return registrationResult;
	} // end registerUser()
	
	
	/**
	 * Saves the newly created locally in the phone to be able to start use the account immediately.
	 */
    private void saveCreatedUserToFile() {
    	
    	RegisteredMobileUser newUser = new RegisteredMobileUser();
    	
    	newUser.setFirstName(etRegisterFirstName.getText().toString());
    	newUser.setLastName(etRegisterLastName.getText().toString());
    	newUser.setAddress(etRegisterAddress.getText().toString());
    	newUser.setPostNr(etRegisterPostalCode.getText().toString());
    	newUser.setPostAddress(etRegisterPostalAddress.getText().toString());
    	newUser.setRole("NormalUser");
    	newUser.setPhoneNumber(etRegisterPhone.getText().toString());
    	newUser.setMobilePhoneNumber(etRegisterMobilePhone.getText().toString());
    	newUser.setUserName(etRegisterUserName.getText().toString());
    	newUser.setPassword(etRegisterPassword.getText().toString());
    	newUser.setAge(etRegisterBirth.getText().toString());
    	newUser.setEmail(etEmailAddress.getText().toString());
    	newUser.setWantsNewsLetter(cbWantsNewsLetter.isChecked());
    	newUser.setWantsToBePublished(cbPublish.isChecked());
    	newUser.setProjectName(RestClient.PROJECT_NAME);
    	newUser.setCompanies(companyList);
    	if (registeredUser != null) {
        	newUser.setControlInfos(registeredUser.getControlInfos());
    	}
    	else {
        	newUser.setControlInfos(unregisteredUser.getControlInfos());
    	}
    	newUser.setControls(new ArrayList<Control>());

    	MapObject newMapObject = unregisteredUser.getMapObject();
    	newUser.setMapObject(newMapObject);
    	
    	SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
	      	
	    prefsEditor.putString("username", etRegisterUserName.getText().toString());
	    prefsEditor.putString("password", etRegisterPassword.getText().toString());
		prefsEditor.putInt("controls_taken", 0);
	    // Find a better way to save the company selection
	    prefsEditor.putInt("selected_company", spUserType.getSelectedItemPosition());
		prefsEditor.commit();
		
		try {
			FileHandler.writeObjectToFile(newUser, openFileOutput(FileHandler.REGISTERED_USER_FILENAME, Context.MODE_PRIVATE));
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    } // saveCreatedUserToFile()
    
	
	// Code to show dialog windows. The different dialogs are created using an int constant
	// to identify which one to open. This function is called through showDialog(int) call
	// in code.
    @Override
    protected Dialog onCreateDialog(int id) {

    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	
    	switch (id) {
   			case AN_ACCOUNT_ALREADY_EXISTS:
   				builder.setMessage("Observera att ett konto redan används på telefonen med användarnamn: " 
   									+ sharedPreferences.getString("username", null) + ". Om du registrerara ett nytt " + 
   									"konto på den här telefonen kommer det nuvarande att ersättas med det nya " +
   									"kontot i mobilen.")
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			        	   isOverWriteOk = true;
			        	   dialog.dismiss();
			           }
			       })
			       .setNegativeButton("Avbryt", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	        	   dialog.cancel();
			        	   finish();
        	           }
        	       });
   				break;
			case INCORRECT_AGE_FORMAT:
				builder.setMessage("Ange år som NNNN (t.ex. 1976).")
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				           public void onClick(DialogInterface dialog, int id) {
				        	  dialog.dismiss();
				        	  etRegisterBirth.requestFocus();
				           }
				       });
				break;  
			case PASSWORD_TOO_SHORT:
	    		builder.setMessage("Lösenordet måste vara minst 4 tecken långt.")
	    		.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			        	  dialog.dismiss();
			        	  etRegisterPassword.requestFocus();
			           }
			       });
	    		break;
			case EMAIL_EXISTS_ALREADY:
				builder.setMessage("E-postadressen som angavs används redan. Vänligen välj en annan.")
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				           public void onClick(DialogInterface dialog, int id) {
				        	   dialog.dismiss();
				        	   etEmailAddress.requestFocus();
				           }
				       });
				break;  	
    		case USER_ALREADY_REGISTERED:
    			builder.setMessage("Användarnamnet finns redan. Om du redan är registrerad på Friska " +
    							   "Karlstad med detta namn, välj \"Logga in\" ifall du vill använda " + 
    							   "det kontot!")
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int id) {
    			        	   dialog.dismiss();
    			           }
    			       });
    			break;
       		case PUL_NOT_ACCEPTED:
    			builder.setMessage("Du måste acceptera PUL!")
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int id) {
    			        	   dialog.dismiss();
    			           }
    			       });
    			break;
       		case MANDATORY_FIELD_NOT_FILLED_IN:
    			builder.setMessage("Obligatoriskt fält tomt! Fält märkta med '*' måste fyllas i.")
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int id) {
    			        	   dialog.dismiss();
    			           }
    			       });
    			break;
       		case USER_REGISTRATION_FAILED:
    			builder.setMessage("Registreringen misslyckades! Var vänlig försök igen senare eller " + 
    							   "kontakta Friska Karlstad.")
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int id) {
    			        	   dialog.dismiss();
    			           }
    			       });

    			break;
    		default:
        }
        return builder.create();
    } // end onCreateDialog()
	
    
	/**
	 * Clears all EditText widgets.
	 */
	private void clearFields() {
		etRegisterFirstName.setText("");
		etRegisterLastName.setText("");
		etRegisterAddress.setText("");
		etRegisterPostalCode.setText("");
		etRegisterPostalAddress.setText("");
		etEmailAddress.setText("");
		etRegisterPhone.setText("");
		etRegisterMobilePhone.setText("");
		etRegisterUserName.setText("");
		etRegisterPassword.setText("");
		etRegisterBirth.setText("");
		etRegisterFirstName.requestFocus();
	} // end clearFields()

    
	/**
	 * Cancels the registration of the user by returning to the previous view.
	 */
	private void cancelRegistration() {
		finish();
	} // end cancelRegistration()

	/**
	 * Save all text in the text field if the views destroy method is called, for example when the user
	 * rotates the screen.
	 * 
	 * @param savedInstanceState a Bundle that will be passed to this method on destroy method call.
	 */
	@Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
    	savedInstanceState.putString("registerview_firstname", etRegisterFirstName.getText().toString());
    	savedInstanceState.putString("registerview_lastname", etRegisterLastName.getText().toString());
    	savedInstanceState.putString("registerview_streeaddress", etRegisterAddress.getText().toString());
    	savedInstanceState.putString("registerview_postalcode", etRegisterPostalCode.getText().toString());
    	savedInstanceState.putString("registerview_postaladdress", etRegisterPostalAddress.getText().toString());
    	savedInstanceState.putString("registerview_email", etEmailAddress.getText().toString());
    	savedInstanceState.putString("registerview_homephone", etRegisterPhone.getText().toString());
    	savedInstanceState.putString("registerview_mobphone", etRegisterMobilePhone.getText().toString());
    	savedInstanceState.putString("registerview_username", etRegisterUserName.getText().toString());
    	savedInstanceState.putString("registerview_password", etRegisterPassword.getText().toString());
    	savedInstanceState.putString("registerview_birth", etRegisterBirth.getText().toString());
    	savedInstanceState.putInt("registerview_usertype_choice", spUserType.getSelectedItemPosition());
    	savedInstanceState.putBoolean("registerview_newsletter", cbWantsNewsLetter.isChecked());
    	savedInstanceState.putBoolean("pul_acceptance", cbPulAcceptance.isChecked());
    	savedInstanceState.putBoolean("publish_result_ok", cbPublish.isChecked());
    	savedInstanceState.putBoolean("user_accepted_overwrite", isOverWriteOk);
    	removeDialog(AN_ACCOUNT_ALREADY_EXISTS); // We do not want the dialog to reappear on screen orientation change.
    	super.onSaveInstanceState(savedInstanceState);
   	} // end onSaveInstanceState()
    
	
    /**
     * Locks any screen orientation changes. After the user registration have been submitted we do not want
     * the screen to rotate since it will kill this activity and restart it.
     */
    private void lockScreenRotation() {
      // Stop the screen orientation changing during an event
        switch (this.getResources().getConfiguration().orientation) {
	      case Configuration.ORIENTATION_PORTRAIT:
	        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	        break;
	      case Configuration.ORIENTATION_LANDSCAPE:
	        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	        break;
        }
    } // end lockScreenRotation()
    
    
    /**
     * Unlocks the screen orientation change ability.
     */
    private void unlockScreenRotation() {
    	this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    } // end unlockScreenRotation()
	
} // end class RegisterUserView