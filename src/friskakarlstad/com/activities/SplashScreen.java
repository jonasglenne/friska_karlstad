package friskakarlstad.com.activities;

import friskakarlstad.com.R;
import friskakarlstad.com.R.layout;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;

/**
 * Displays a splash screen showing the project logo for a specified amount of time.
 * After the specified amount of time, the main menu will be loaded.
 * 
 * @author jonast42@yahoo.com
 */
public class SplashScreen extends Activity {

	private static final int STOPSPLASH = 0;
	
	// The time the splash screen will be shown.
    private static final long SPLASHTIME = 3000;
    private ImageView splash;

    // Handles stopping of splash screen and loading of main menu view.
    private Handler splashHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
		        switch (msg.what) {
		
		        case STOPSPLASH:
		        	SplashScreen.this.startActivity(new Intent(SplashScreen.this, MainMenuView.class));
		        	SplashScreen.this.finish();
		        	break;
		        }
		        super.handleMessage(msg);
		}
    };
        
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        
        //splash = (ImageView) findViewById(R.drawable.splash_pic);
        
        Message msg = new Message();
        msg.what = STOPSPLASH;
        splashHandler.sendMessageDelayed(msg, SPLASHTIME);
    } // end onCreate()
    
} // end class SplashScreen


