package friskakarlstad.com.activities;

import java.io.FileNotFoundException;
import friskakarlstad.com.R;
import friskakarlstad.com.pojos.RegisteredMobileUser;
import friskakarlstad.com.utilities.FileHandler;
import friskakarlstad.com.utilities.RestClient;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterApplicationView extends Activity {
	
	// Id for an Alert showing that the unlock of the application failed.
	public static final int UNLOCK_FAILED_ALERT = 1;
	
	// Id for an Alert informing the user that the unlock of the application was successful.
	public static final int UNLOCK_SUCCEED_ALERT = 2;
	
	// id for an Alert informing the user that the network connection could not be established 
	// to the server.
	public static final int NETWORK_CONNECTION_FAILED_ALERT = -1;
	
	// Tag for progress dialog indicating download from Stamford.
	public static final int PROGRESS_DIALOG = 4;
	
	// Id for an Alert informing the user that there is no registered account in the application.
	public static final int NO_REGISTERED_USER = 5;
	
	// Id for an Alert informing the user that the code they tried to unlock the application with
	// has already been used.
	public static final int UNLOCK_CODE_ALREADY_USED = 6;
	
	// Heading for the login user view.
	private TextView tvUnlockHeading;
	
	// Text describing the alternatives to login to an existing account or
	// make a new registration at server.
	private TextView tvCodeHeading;

	// EditText where the user enters the username for the existing account.
	private EditText etCodeEnterField;
	
	// Text shown if the user has already registered the code.
	private TextView tvAlreadyUnlocked;

	// Button for executing the login for an existing account.
	private Button btnSendCode;
	
	// Communicates via the REST-API on google app-engine.
	private RestClient restClient;
	
	// Show that download of controls from Stamford and upload to GAE are in progress.
	private ProgressDialog unlockProgress;
	
	// Holds the username for the currently installed account.
	private String userName;
	
	private SharedPreferences sharedPreferences;
	
	// Dialog that shows that the unlock operation is in progress.
	private ProgressDialog downloadProgress;
	
	// An object containing the accound (user info, controls taken and controlinfos) for 
	// a registered user.
	private RegisteredMobileUser registeredUser;
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.unlock_application_view);
        
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		restClient = new RestClient();
        userName = sharedPreferences.getString("username", null);
        
		if (userName != null) {
			try {
				registeredUser = (RegisteredMobileUser)FileHandler.getObjectFromFile(openFileInput(FileHandler.REGISTERED_USER_FILENAME));
			
				Boolean alreadyUnlocked = registeredUser.getCodeHasBeenVerified();
				
				if (alreadyUnlocked != null) {
					if (alreadyUnlocked == true) {
						tvAlreadyUnlocked = (TextView) findViewById(R.id.tvAlreadyUnlocked);
						tvAlreadyUnlocked.setText("Du har redan skickat in din kod och låst upp applikationen. " +
												  "Kan du fortfarande inte se betalkontrollerna kontakta Friska Karlstad");
					}
				}
			} 
			catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
		}
		else {
			showDialog(NO_REGISTERED_USER);
		}
        
        tvUnlockHeading = (TextView) findViewById(R.id.tvUnlockHeading);
        tvCodeHeading = (TextView) findViewById(R.id.tvCodeHeading);

        etCodeEnterField = (EditText) findViewById(R.id.etCodeEnterField);
        if (savedInstanceState != null) {
        	etCodeEnterField.setText(savedInstanceState.getString("saved_code"));
        }
        
        btnSendCode = (Button) findViewById(R.id.btnSendCode);
        btnSendCode.setOnClickListener(new OnClickListener() {
        	@Override
        	public void onClick(View v) {
            	if ( !etCodeEnterField.getText().toString().equals("") ) {
            		new SendUnlockToServer().execute();
        		}
        		else {
        			showToast("Kodfält tomt! Ange koden du fått med Friska Karlstad-kortet.");
        		}
        	} // end onClick()
        });
	} // end onCreate()
	
	
    /**
     * Asynch class used to contact the server to unlock the application
     */
    private class SendUnlockToServer extends AsyncTask<Void, Void, Integer> {
    	
    	@Override
		protected void onPreExecute() {
    		// Prevent screen rotation (which will kill the activity) during the download of 
    		// account from the server to avoid interrupted download.
    		lockScreenRotation();
    		showProgressDialog();
        } // end onPreExecute()

        @Override
		protected Integer doInBackground(Void... params) {
        	return unlockApplication();
        } // end doInBackground()
		
        protected void onProgressUpdate() {}

        protected void onPostExecute(Integer result) {
    		downloadProgress.dismiss();
    		
    		if (result == RestClient.UNDEFINED_RESPONSE) {
    			showDialog(NETWORK_CONNECTION_FAILED_ALERT);
    		}
    		else if (result == RestClient.INVALID_UNLOCK_CODE) {
    			showDialog(UNLOCK_FAILED_ALERT);
    		}
    		else if (result == RestClient.UNLOCK_CODE_ALREADY_USED) {
    			showDialog(UNLOCK_CODE_ALREADY_USED);
    		}
    		else if (result == RestClient.RESULT_WAS_OK) {
    			showDialog(UNLOCK_SUCCEED_ALERT);
    			writeUnlockedToUser();
    		}
    		else if (result == NO_REGISTERED_USER) {
    			showDialog(NO_REGISTERED_USER);
    		}

    		// Restore the posibility to rotate the screen after the download of the account has finished.
    		unlockScreenRotation();
        } // end onPostExecute()
        
    } // end class SendUnlockToServer
    
    
    /**
     * On successful lock, the user will be indicated to have unlocked the application and
     * be able to see all the controls published. The information is saved to file.
     */
    private void writeUnlockedToUser() {
    	registeredUser.setCodeHasBeenVerified(true);
    	
    	try {
   			FileHandler.writeObjectToFile(registeredUser, openFileOutput(FileHandler.REGISTERED_USER_FILENAME, Context.MODE_PRIVATE));
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    } // end writeUnlockedToUser()
	
    
    /**
     * Sends the code to the server to unlock the application.
     * 
     * @return an int that is the result of the unlock attempt.
     */
    private int unlockApplication() {
    	
    	int result = NETWORK_CONNECTION_FAILED_ALERT;
    	String codeToSend = etCodeEnterField.getText().toString();
    	String password = sharedPreferences.getString("password", null);
    	result = restClient.unlockApplication(codeToSend, userName, password);
		return result;
    } // end unlockApplication()
    
    
    /**
     * Displays a toast message on screen
     * @param toastText a String that is the text to display.
     */
    private void showToast(String toastText) {
    	int duration = Toast.LENGTH_SHORT;
    	Toast toast = Toast.makeText(this, toastText, duration);
    	toast.show();
    } // end showToast()
	
	
    /**
     * Show a progress dialog during the download of account from Stamford.
     */
	private void showProgressDialog() {
		// Start the progress dialog for download of controls from Stamford.
		downloadProgress = ProgressDialog.show(this, "Låser upp applikation", "Vänligen vänta...", true);
	} // end showProgressDialog()

	
	/**
	 * Code to show dialog windows. The different dialogs are created using an int constant
	 * to identify which one to open. This function is called through showDialog(int) call
	 * in code.
	 */
    @Override
    protected Dialog onCreateDialog(int id) {
        
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	
    	switch (id) {
    		case NO_REGISTERED_USER:
    			builder.setMessage("Ingen Användare registrerad ännu. Registrera dig eller logga " + 
    							   "in på ditt Friska Karlstad-konto och försök igen.")
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int id) {
    			        	   removeDialog(NO_REGISTERED_USER);
    			        	   finish();
    			           }
    			       });
    			break;
       		case UNLOCK_FAILED_ALERT:
    			builder.setMessage("Kunde inte hitta koden. Kontrollera siffrorna och försök igen.")
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int id) {
    			        	   removeDialog(UNLOCK_FAILED_ALERT);
    			           }
    			       });
    			break;
       		case UNLOCK_SUCCEED_ALERT:
    			builder.setMessage("Du har nu låst upp applikationen!")
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int id) {
    			        	   removeDialog(UNLOCK_SUCCEED_ALERT);
    			        	   setResult(RESULT_OK);
    			        	   finish();
    			           }
    			       });
    			break;
       		case NETWORK_CONNECTION_FAILED_ALERT:
    			builder.setMessage("Kunde inte kontakta servern.")
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int id) {
    			        	   removeDialog(NETWORK_CONNECTION_FAILED_ALERT);
    			           }
    			       });
    			break;
       		case UNLOCK_CODE_ALREADY_USED:
    			builder.setMessage("Koden har redan använts.")
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int id) {
    			        	   removeDialog(UNLOCK_CODE_ALREADY_USED);
    			           }
    			       });
    			break;
    		default:
                break;
        }
    	
        return builder.create();
    } // end onCreateDialog()
    
    
    protected void onSaveInstanceState(Bundle savedInstanceState) {
    	savedInstanceState.putString("saved_code", etCodeEnterField.getText().toString());
    	super.onSaveInstanceState(savedInstanceState);
   	} // end onSaveInstanceState()
    
    
    /**
     * Locks any screen orientation changes. 
     */
    private void lockScreenRotation() {
      // Stop the screen orientation changing during an event
        switch (this.getResources().getConfiguration().orientation) {
	      case Configuration.ORIENTATION_PORTRAIT:
	        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	        break;
	      case Configuration.ORIENTATION_LANDSCAPE:
	        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	        break;
        }
    } // end lockScreenRotation()
    
    
    /**
     * Unlocks the screen orientation change ability. 
     */
    private void unlockScreenRotation() {
    	this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    } // end unlockScreenRotation()
    
} // end class LoginUserView