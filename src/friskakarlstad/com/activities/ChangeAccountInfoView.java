package friskakarlstad.com.activities;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import friskakarlstad.com.R;
import friskakarlstad.com.pojos.Company;
import friskakarlstad.com.pojos.RegisteredMobileUser;
import friskakarlstad.com.utilities.FileHandler;
import friskakarlstad.com.utilities.RestClient;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class ChangeAccountInfoView extends Activity {
	
	/**************************************************************
	 * Result codes for different methods called within the Activity.
	 **************************************************************/
	
	// Result from the update is OK.
	static final int UPDATE_IS_OK = 0;
	
	// Identifier for the dialog shown if user already exists.
	static final int USER_ALREADY_REGISTERED = 1;
	
	//Identifier for the dialog to show when no new password has been given when user 
	// try to change password.
	static final int NEW_PASSWORD_TOO_SHORT = 3;
	
	//Identifier for the dialog to show when the old password was incorrect when a user 
	// tried to change password.
	static final int OLD_PASSWORD_INCORRECT = 4;

	// Identifier for a dialog that shows progressing of update to user account.
	static final int UPDATING_ACCOUNT_ALERT = 6;
	
	// Identifier for a dialog that informs user the update of the account info has failed.
	static final int FAILED_TO_UPDATE_ACCOUNT = 7;
	
	// Identifier for a result that will inform the user that the age field needs a specific format NNNN.
	static final int INCORRECT_AGE_FORMAT = 8;
	
	// The server has replied that the user does not exist
	static final int USER_DOES_NOT_EXIST = 9;
	
	/**************************************************************
	 * Widgets for this activity.
	 **************************************************************/
	
	// Heading for the user registration view.
	private TextView tvRegisterUserHeading;
	
	// Informs the user that some of the fields are required to be filled in
	// to be able to register.
	private TextView tvFieldsRequiredInfo;
	
	// Heading for the real name input EditText.
	private TextView tvRegisterFirstNameHeading;

	// Enter the real name here.
	private EditText etUpdateRegisterFirstName;

	// Heading for the real name input EditText.
	private TextView tvRegisterLastNameHeading;

	// Enter the real name here.
	private EditText etUpdateRegisterLastName;
	
	// Heading for the address input EditText.
	private TextView tvRegisterAddressHeading;

	// Enter the real name here.
	private EditText etUpdateRegisterAddress;

	// Heading for the address input EditText.
	private TextView tvRegisterPostalCodeHeading;

	// Enter the real name here.
	private EditText etUpdateRegisterPostalCode;

	// Heading for the address input EditText.
	private TextView tvRegisterPostalAddressHeading;

	// Enter the real name here.
	private EditText etUpdateRegisterPostalAddress;

	// Heading for the phone number input EditText.
	private TextView tvRegisterEmailAddress;

	// Enter the real name here.
	private EditText etUpdateEmailAddress;
	
	// Heading for the phone number input EditText.
	private TextView tvRegisterPhoneHeading;

	// Enter the real name here.
	private EditText etUpdateRegisterPhone;

	// Heading for the phone number input EditText.
	private TextView tvRegisterMobilePhoneHeading;

	// Enter the real name here.
	private EditText etUpdateRegisterMobilePhone;
	
	// Heading for the user name input EditText.
	private TextView tvRegisterUserNameHeading;

	// Enter the real name here.
	private EditText etUpdateRegisterUserName;
	
	// Heading for the password input EditText.
	private TextView tvOldPasswordHeading;

	// Enter the real name here.
	private EditText etOldPassword;
	
	private TextView tvRegisterUpdatePasswordHeading;
	
	private EditText etUpdateRegisterPassword;
	
	// Heading for the password input EditText.
	private TextView tvRegisterBirthHeading;

	// Enter the real name here.
	private EditText etUpdateRegisterBirth;

	// Registers the user
	private Button accountUpdateButton;
	
	// Cancels the registration
	private Button cancelButton;
	
	// Lets the user select which type of account this user should have.
	private Spinner spUserType;

	private ArrayAdapter spUserTypeArrayAdapter;

	// Checkbox to determine whether user want newsletter or not.
	private CheckBox cbWantsNewsLetter;
	
	// Checkbox to determine whether user wants to accept storing personal informaiton according to PUL.
	private CheckBox cbPulAcceptance;

	// Checkbox to determine whether user wants to publish its results.
	private CheckBox cbPublish;

	// String that represents the existing e-mail address. Used for comparison to the new one if the user
	// updates the e-mail address.
	private String oldEMailAddress;

	// Heading for the spinner where the user selects which user type he or she is.
	private TextView tvUserType;
	
	private ProgressDialog progressDialog;

	/**************************************************************
	 * Utility attributes for this activity.
	 **************************************************************/
	
	// Communicates via the REST-API on google app-engine.
	private RestClient restClient;
	
	// List of companies and organisations the user can select from.
	private ArrayList<Company> companyList;
	
	// The registered users account.
	private RegisteredMobileUser registeredUser;
	
	private SharedPreferences sharedPreferences;
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_user_view_layout);

        restClient = new RestClient();
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		
    	// Load the user account from file to get the user information to fill into the fields.
		try {
			registeredUser = (RegisteredMobileUser)FileHandler.getObjectFromFile(openFileInput(FileHandler.REGISTERED_USER_FILENAME));
			companyList = registeredUser.getCompanies();
		}
		
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
        tvRegisterUserHeading = (TextView) findViewById(R.id.tvRegisterUserHeading);
    	
        tvFieldsRequiredInfo = (TextView) findViewById(R.id.tvFieldsRequiredInfo);
        
        tvRegisterFirstNameHeading = (TextView) findViewById(R.id.tvRegisterFirstNameHeading);
    	etUpdateRegisterFirstName = (EditText) findViewById(R.id.etUpdateRegisterFirstName);
    	etUpdateRegisterFirstName.setText(registeredUser.getFirstName());
    	
    	tvRegisterLastNameHeading = (TextView) findViewById(R.id.tvRegisterLastNameHeading);
    	etUpdateRegisterLastName = (EditText) findViewById(R.id.etUpdateRegisterLastName);
    	etUpdateRegisterLastName.setText(registeredUser.getLastName());
    	
    	tvRegisterAddressHeading = (TextView) findViewById(R.id.tvRegisterAddressHeading);
    	etUpdateRegisterAddress = (EditText) findViewById(R.id.etUpdateRegisterAddress);
    	etUpdateRegisterAddress.setText(registeredUser.getAddress());
    	
    	tvRegisterPostalCodeHeading = (TextView) findViewById(R.id.tvRegisterPostalCodeHeading);
    	etUpdateRegisterPostalCode = (EditText) findViewById(R.id.etUpdateRegisterPostalCode);
    	etUpdateRegisterPostalCode.setText(registeredUser.getPostNr());
    	
    	tvRegisterPostalAddressHeading = (TextView) findViewById(R.id.tvRegisterPostalAddressHeading);
    	etUpdateRegisterPostalAddress = (EditText) findViewById(R.id.etUpdateRegisterPostalAddress);
    	etUpdateRegisterPostalAddress.setText(registeredUser.getPostAddress());
    	
    	tvRegisterEmailAddress = (TextView) findViewById(R.id.tvRegisterEmailAddressHeading);
    	oldEMailAddress = registeredUser.getEmail();
    	etUpdateEmailAddress = (EditText) findViewById(R.id.etUpdateEmailAddress);
    	etUpdateEmailAddress.setText(registeredUser.getEmail());
    	
    	tvRegisterPhoneHeading = (TextView) findViewById(R.id.tvRegisterPhoneHeading);
    	etUpdateRegisterPhone = (EditText) findViewById(R.id.etUpdateRegisterPhone);
    	etUpdateRegisterPhone.setText(registeredUser.getPhoneNumber());
    	
    	tvRegisterMobilePhoneHeading = (TextView) findViewById(R.id.tvRegisterMobilePhoneHeading);
    	etUpdateRegisterMobilePhone = (EditText) findViewById(R.id.etUpdateRegisterMobilePhone);
    	etUpdateRegisterMobilePhone.setText(registeredUser.getMobilePhoneNumber());
    	
    	tvRegisterUserNameHeading = (TextView) findViewById(R.id.tvRegisterUserNameHeading);

    	etUpdateRegisterUserName = (EditText) findViewById(R.id.etUpdateRegisterUserName);
    	etUpdateRegisterUserName.setText(sharedPreferences.getString("username", null));
    	// It shall not be possible to change username.
    	etUpdateRegisterUserName.setEnabled(false);
    	
    	tvOldPasswordHeading = (TextView) findViewById(R.id.tvOldPasswordHeading);
    	etOldPassword = (EditText) findViewById(R.id.etOldPassword);
    	tvRegisterUpdatePasswordHeading = (TextView) findViewById(R.id.tvRegisterUpdatePasswordHeading);
    	etUpdateRegisterPassword = (EditText) findViewById(R.id.etUpdateRegisterPassword);
    	
    	tvRegisterBirthHeading = (TextView) findViewById(R.id.tvRegisterBirthHeading);
    	etUpdateRegisterBirth = (EditText) findViewById(R.id.etRegisterBirth);
    	etUpdateRegisterBirth.setText(registeredUser.getAge());
    	
    	tvUserType = (TextView) findViewById(R.id.tvUserType);
    	
    	spUserType = (Spinner) findViewById(R.id.spUserType);
    	spUserTypeArrayAdapter = new ArrayAdapter<CharSequence>(this, android.R.layout.simple_spinner_item);
    	
    	// Add selectable user categories to the list.
    	for (Company currentCompany : companyList) {
    		spUserTypeArrayAdapter.add(currentCompany.getCompanyName());
    	}
    	int savedSelectedCompany = sharedPreferences.getInt("selected_company", 1);
    	
    	spUserTypeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spUserType.setAdapter(spUserTypeArrayAdapter);
        spUserType.setSelection(savedSelectedCompany);
        
    	cbWantsNewsLetter = (CheckBox) findViewById(R.id.cbWantsNewsLetter);
   		cbWantsNewsLetter.setChecked(registeredUser.getWantsNewsLetter());

    	cbPublish = (CheckBox) findViewById(R.id.cbPublishUserResults);
    	cbPublish.setChecked(registeredUser.getWantsToBePublished());
   		
    	accountUpdateButton = (Button) findViewById(R.id.accountUpdateButton);
    	accountUpdateButton.setOnClickListener(new OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		new UpdateAccountInfoAsyncTask().execute();
        	} // end onClick()
        });

    	cancelButton = (Button) findViewById(R.id.btnCancelRegistration);
    	cancelButton.setOnClickListener(new OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		cancelAccountUpdate();
        	} // end onClick()
        });
	} // end onCreate()


	/**
	 * Performs a check of that the user entered values are correct.
	 * 
	 * @return an int indicating if the values are ok or if there is some problem
	 */
	private int checkEnteredValues() {
		
		int result = UPDATE_IS_OK;
		
		// Check possible updates to password. If the old password is filled in and matches the stored one the user
		// wants to update the password.
		if (!etUpdateRegisterPassword.getText().toString().equals("")) {

			// Check that the old password is correct
			if (etOldPassword.getText().toString().equals(sharedPreferences.getString("password", null))) { 
				
				// Check that a new password has been given.
				if (etUpdateRegisterPassword.getText().toString().length() < 4 &&
					etUpdateRegisterPassword.getText().toString().length() >= 0) {
					result = NEW_PASSWORD_TOO_SHORT;
				}
			}
			else {
				result = OLD_PASSWORD_INCORRECT;
			}
		}
		
		if (etUpdateRegisterBirth.getText().toString().length() != 4){
			// TODO Add textwatcher checking that the input is a number in oncreate
			//showDialog(INCORRECT_AGE_FORMAT);
			result = INCORRECT_AGE_FORMAT;
		}
		
		return result;
	} // end enteredValuesOk()
	
	
	/**
	 * Sends the updated user account information supplied in the EditText fields to the server.
	 */
	private int updateUserAccount() {
		
		int result = checkEnteredValues();
		
		if (result == UPDATE_IS_OK) {
			StringBuilder xmlBuilder = new StringBuilder(); 
			
			xmlBuilder.append("<user>");
			xmlBuilder.append("<firstName>" + etUpdateRegisterFirstName.getText().toString() + "</firstName>");
			xmlBuilder.append("<lastName>" + etUpdateRegisterLastName.getText().toString() + "</lastName>");
			xmlBuilder.append("<address>" + etUpdateRegisterAddress.getText().toString() + "</address>");
			xmlBuilder.append("<postNr>" + etUpdateRegisterPostalCode.getText().toString() + "</postNr>");
			xmlBuilder.append("<postAddress>" + etUpdateRegisterPostalAddress.getText().toString() + "</postAddress>");
			xmlBuilder.append("<role>NormalUser</role>");
			xmlBuilder.append("<phoneNumber>" + etUpdateRegisterPhone.getText().toString() + "</phoneNumber>");
			xmlBuilder.append("<mobilePhoneNumber>" + etUpdateRegisterMobilePhone.getText().toString() + "</mobilePhoneNumber>");
			xmlBuilder.append("<userName>" + sharedPreferences.getString("username", null) + "</userName>");
			xmlBuilder.append("<password>" + etUpdateRegisterPassword.getText().toString() + "</password>");
			xmlBuilder.append("<age>" + etUpdateRegisterBirth.getText().toString() + "</age>");
			xmlBuilder.append("<email>" + etUpdateEmailAddress.getText().toString() + "</email>");
			xmlBuilder.append("<wantsNewsLetter>" + cbWantsNewsLetter.isChecked() + "</wantsNewsLetter>");
			xmlBuilder.append("<wantsToBePublished>" + cbPublish.isChecked() + "</wantsToBePublished>");
			xmlBuilder.append("<sessionId></sessionId>");
			xmlBuilder.append("<projectName>" + registeredUser.getProjectName() + "</projectName>");
			xmlBuilder.append("<companyName>" + spUserTypeArrayAdapter.getItem(spUserType.getSelectedItemPosition()) + "</companyName>");
			xmlBuilder.append("</user>");
				
			result = restClient.updateUser(xmlBuilder.toString()); 
				
			if (result == UPDATE_IS_OK) {
				saveUpdatedUserToFile();
			}
			else {
				result = USER_DOES_NOT_EXIST;
			}
		}
		
		return result;
	} // end registerUser()

	
	/**
	 * Saves the newly updated user locally in the phone to be able to start use the updated account 
	 * immediately.
	 */
    private void saveUpdatedUserToFile() {
    	
    	SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
	    
    	registeredUser.setFirstName(etUpdateRegisterFirstName.getText().toString());
    	registeredUser.setLastName(etUpdateRegisterLastName.getText().toString());
    	registeredUser.setAddress(etUpdateRegisterAddress.getText().toString());
    	registeredUser.setPostNr(etUpdateRegisterPostalCode.getText().toString());
    	registeredUser.setPostAddress(etUpdateRegisterPostalAddress.getText().toString());
    	registeredUser.setRole("NormalUser");
    	registeredUser.setPhoneNumber(etUpdateRegisterPhone.getText().toString());
    	registeredUser.setMobilePhoneNumber(etUpdateRegisterMobilePhone.getText().toString());
    	
    	if (!etUpdateRegisterPassword.getText().toString().equals("")) {
    		registeredUser.setPassword(etUpdateRegisterPassword.getText().toString());
    	    prefsEditor.putString("password", etUpdateRegisterPassword.getText().toString());
    	}
    	registeredUser.setAge(etUpdateRegisterBirth.getText().toString());
    	registeredUser.setEmail(etUpdateEmailAddress.getText().toString());
    	registeredUser.setWantsNewsLetter(cbWantsNewsLetter.isChecked());
    	registeredUser.setWantsToBePublished(cbPublish.isChecked());
    	
	    // Find a better way to save the company selection
	    prefsEditor.putInt("selected_company", spUserType.getSelectedItemPosition());
		prefsEditor.commit();
		
		try {
			FileHandler.writeObjectToFile(registeredUser, openFileOutput(FileHandler.REGISTERED_USER_FILENAME, Context.MODE_PRIVATE));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    } // end saveCreatedUserToFile()

    
    /**
     * Class used to contact the server asynchronously in a separate thread from the UI-thread 
     * to download an existing account for a Stamford user.
     */
    private class UpdateAccountInfoAsyncTask extends AsyncTask<Void, Integer, Integer> {

    	@Override
		protected void onPreExecute() {
    		progressDialog = new ProgressDialog(ChangeAccountInfoView.this);
    		progressDialog.setTitle("Friska Karlstad");
    		progressDialog.setMessage("Uppdaterar kontoinformation");
    		progressDialog.show();
    	} // end onPreExecute()
    	
		@Override
		protected Integer doInBackground(Void... params) {
			return updateUserAccount();
		}
    	
        protected void onPostExecute(Integer result) {
        	
        	progressDialog.cancel();
        	
        	if (result == NEW_PASSWORD_TOO_SHORT) {
				etUpdateRegisterPassword.requestFocus();
				showDialog(NEW_PASSWORD_TOO_SHORT);
        	}
        	else if (result == OLD_PASSWORD_INCORRECT) {
				etOldPassword.requestFocus();
        		showDialog(OLD_PASSWORD_INCORRECT);
        	}
        	else if (result == 0) {
				Intent data = new Intent();
				data.putExtra("update_account_result", true);
				setResult(RESULT_OK, data);
				finish();
        	}
        	else {
        		showDialog(FAILED_TO_UPDATE_ACCOUNT);
        	}
        } // end onPostExecute()
    } // end class UpdateAccountInfoAsyncTask()


	/**
	 * Code to show dialog windows. The different dialogs are created using an int constant
	 * to identify which one to open. This function is called through showDialog(int) call
	 * in code.
	 */
    @Override
    protected Dialog onCreateDialog(int id) {
        
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	
    	switch (id) {
       		case NEW_PASSWORD_TOO_SHORT:
    			builder.setMessage("Nytt lösenord måste vara minst 4 tecken!")
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int id) {
    			        	   removeDialog(NEW_PASSWORD_TOO_SHORT);
    			           }
    			       });
    			break;
       		case OLD_PASSWORD_INCORRECT:
    			builder.setMessage("Gammalt lösenord inkorrekt eller saknas!")
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int id) {
    			        	   removeDialog(OLD_PASSWORD_INCORRECT);
    			           }
    			       });
    			break;
       		case FAILED_TO_UPDATE_ACCOUNT:
    			builder.setMessage("Misslyckades uppdatera kontoinformation!")
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int id) {
    			        	   removeDialog(FAILED_TO_UPDATE_ACCOUNT);
    			           }
    			       });
    			break;
       	 case INCORRECT_AGE_FORMAT:
 			builder.setMessage("Ange år som NNNN (t.ex. 1976).")
 			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
 			           public void onClick(DialogInterface dialog, int id) {
 			        	  removeDialog(INCORRECT_AGE_FORMAT);
 			        	  etUpdateRegisterBirth.requestFocus();
 			           }
 			       });
 			break; 
    		default:
    			break;
        }
    	
        return builder.create();
    } // end onCreateDialog()


	/**
	 * Cancels the registration of the user by returning to the previous view.
	 */
	private void cancelAccountUpdate() {
		finish();
	} // end cancelRegistration()

} // end class ChangeAccountInfoView