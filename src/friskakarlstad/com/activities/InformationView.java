package friskakarlstad.com.activities;

import friskakarlstad.com.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.TextView;
import android.webkit.WebView;

/**
 * Class to display information about Friska Karlstad
 * 
 * @author jonast42@yahoo.com
 */
public class InformationView extends Activity {
    
	// Heading for the about menu.
	private TextView tvAboutFriskaKalrstadHeading;
	
	// Declare webview
	WebView browser;

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.information_view_layout);
	
		tvAboutFriskaKalrstadHeading = (TextView) findViewById(R.id.tvAboutFriskaKalrstadHeading);
		
		browser=(WebView)findViewById(R.id.webkit);
	
		Bundle extras = getIntent().getExtras();
	    if (extras != null) {
	    	String browserUrl = extras.getString("browser_url");
	    	browser.loadUrl(browserUrl);
	    }
	} // end onCreate()
	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		// Go back one step in history if possible rather than closing
		// the view.
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (browser.canGoBack()) {
				browser.goBack();
				return false;
			}
		}
		return super.onKeyDown(keyCode, event);
	} // end onKeyDown()

} // end class InformationView
