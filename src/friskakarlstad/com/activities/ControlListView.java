package friskakarlstad.com.activities;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import friskakarlstad.com.R;
import friskakarlstad.com.pojos.Control;
import friskakarlstad.com.pojos.ControlInfo;
import friskakarlstad.com.pojos.RegisteredMobileUser;
import friskakarlstad.com.pojos.UnregisteredMobileUser;
import friskakarlstad.com.utilities.FileHandler;
import friskakarlstad.com.utilities.RestClient;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

/**
 * Presents the controls to the user in listform. The list indicates
 * which controls have been registered, which needs synch and so on.
 * 
 * @author jonast42@yahoo.com
 */
public class ControlListView extends ListActivity {
    
	// Tag for Log printouts.
	public static final String CLASSNAME = "ControlListView";
	
	/**************************************************************
	 * Result codes for different methods called within the Activity.
	 **************************************************************/
	
	// Activity request code to be used for setting the configuration for this view;
	public static int CHANGE_SETTINGS_REQ = 3;
	
	// Dialog ID for a progress dialog.
	static final int PROGRESS_DIALOG_ID = 1;
	static final int USER_NOT_REGISTERED = 2;
	
	// Id to handle problem if control has been already registered before this registration attempt.
	static final int CONTROL_ALREADY_REGISTERED = 4;
	
	// Id to handle problem if control is not valid in some way.
	static final int INVALID_CONTROL = 5;
	
	static final int RESULT_WAS_OK = 6;
	
	// An id for an Alert dialog shown when it was not possible to store the control to the server.
	static final int STORAGE_NOT_POSSIBLE = 7;
	
	// Keep this result if no contact could be made with the server.
	static final int RESULT_UNDEFINED = -1;
	
	// Id to handle the event that user selected to try to upload unregistered controls to server but
	// there were no unregistered controls to send.
	static final int NOTHING_TO_UPDATE = 8;
	
	/**************************************************************
	 * Widgets for this activity.
	 **************************************************************/
	
	// Heading for this view.
	private TextView tvControlListViewHeading;

	/**************************************************************
	 * Utility attributes for this activity.
	 **************************************************************/
	
	// A list of the controls for Friska Karlstad.
	private ArrayList<ControlInfo> controlInfosList;
	
	// A list with the controls that is used for the list adapter.
	private ArrayList<ControlInfo> lvControlInfoList;
	
	// A custom adapter for the listview that accepts a list of Control objects.
	private ControlInfoAdapter theControlInfoListAdapter;
	
	// TODO Move this to the preference file.
	// Setting which indicates whether user have selected to show already stored controls or not.
	private boolean hideRegisteredControls;
	
	// Progress dialog to show during lengthy operations.
	private ProgressDialog requestProgressDialog;

	// Shared preferences for this application.
	private SharedPreferences sharedPreferences;
	
	// Loaded from the preference attribute "user_name". If this is null after onCreate 
	// it means the user is not registered.
	private String userName;
	
	// An object containing the accound (user info, controls taken and controlinfos) for 
	// a registered user.
	private RegisteredMobileUser registeredUser;
	
	// An object containing project information for an unregistered user.
	private UnregisteredMobileUser unregisteredUser;
	
	private ArrayList<Control> controlList;
	private ArrayList<Control> failedStoreControlList;
	
	private HashMap<Integer, Control> registeredControlHashMap;
	private HashMap<Integer, Control> failedRegisterControlHashMap;
	
	@SuppressWarnings("unchecked")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.control_list_listview_layout);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		
		// Check if there is a retained stateful object from a change in screen orientation. If not, load the list
		// from file.
		final Object retainedControlList = getLastNonConfigurationInstance();

			// Read the control list from local file.
			try {
				
		        userName = sharedPreferences.getString("username", null);
				
				if (userName != null) {
					registeredUser = (RegisteredMobileUser)FileHandler.getObjectFromFile(openFileInput(FileHandler.REGISTERED_USER_FILENAME));

					controlInfosList = registeredUser.getControlInfos();
					
					// If the user has not unlocked the phone. Remove all controls that are not
					// of color green.
					if (registeredUser.getCodeHasBeenVerified() != null) {
						if (registeredUser.getCodeHasBeenVerified() != true) {
							controlInfosList = filterLockedListContent(controlInfosList);
						}
					}

					// Place the controls in a HashMap for key access based on controlnumber.
					controlList = registeredUser.getControls();
					registeredControlHashMap = new HashMap<Integer, Control>();
		        	
		        	for (Control currentControl : controlList) {
		        		registeredControlHashMap.put(Integer.valueOf(currentControl.getControlNumber()), currentControl);
		        	}
		        	
		        	// Retrieve the controls that are only stored locally in the phone and not on the server.
					failedStoreControlList = registeredUser.getRegisterFailedControls();
					failedRegisterControlHashMap = new HashMap<Integer, Control>();
		        	
		        	for (Control currentControl : failedStoreControlList) {
		        		failedRegisterControlHashMap.put(Integer.valueOf(currentControl.getControlNumber()), currentControl);
		        	}
				}
				else {
					unregisteredUser = (UnregisteredMobileUser)FileHandler.getObjectFromFile(openFileInput(FileHandler.UNREGISTERED_USER_FILENAME));
					controlInfosList = unregisteredUser.getControlInfos();
					controlInfosList = filterLockedListContent(controlInfosList);
				}
				
				
			}
			catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
		
		// Find out if the user wants to hide already registered controls or not.
        hideRegisteredControls = sharedPreferences.getBoolean("hide_registered_controls_in_list", false);
        
        tvControlListViewHeading = (TextView) findViewById(R.id.tvControlListViewHeading);
        
        lvControlInfoList = new ArrayList<ControlInfo>();
		theControlInfoListAdapter = new ControlInfoAdapter(this, R.layout.control_list_listrow_layout, lvControlInfoList);
        updateListControlAdapter(controlInfosList);
        setListAdapter(theControlInfoListAdapter);
        // Add a click listener for the control items in the list.
        getListView().setOnItemClickListener(new OnItemClickListener() {
        	@Override
        	public void onItemClick(AdapterView<?> a, View v, int position, long id) {
        		openRegisterControlView(lvControlInfoList.get(position).getControlNumber());
        	}
        });
    } // end onCreate()

	
	/**
	 * Filters out paid controls if the user has not paid and entered the registration code.
	 * 
	 * @param unfilteredControlInfoList a List of all ControlInfos for the project.
	 * @return a List of ControlInfo's that has been filtered
	 */
	private ArrayList<ControlInfo> filterLockedListContent(ArrayList<ControlInfo> unfilteredControlInfoList) {
		
		ArrayList<ControlInfo> filteredList = new ArrayList<ControlInfo>();
		
		for (ControlInfo currentControlInfo : unfilteredControlInfoList) {
			if (!currentControlInfo.getAvailableTo().equalsIgnoreCase("PaidPrivate")) {
				
				filteredList.add(currentControlInfo);
			}
		}
		
		return filteredList;
		
	} // end filterUnlockedListContent()

	
	/**
	 * Populates the listview adapter with elements from a list of controls on create of this view. 
	 * If the user has selected to hide already stored controls, these are not included in the list.
	 * 
	 * @param controlInfoList a List<Control> object is used as parameter
	 */
	private void updateListControlAdapter(final List<ControlInfo> controlInfoList) {
		
		theControlInfoListAdapter.clear();
		
		// Make a copy of the list from the user which will be filtered for not showing registered
		// controls if preferenced
		List<ControlInfo> updateControlInfoList = new ArrayList<ControlInfo>();

		if (controlInfoList != null) {

			for (ControlInfo currentControl : controlInfoList) {
				updateControlInfoList.add(currentControl);
			}
			Collections.sort(updateControlInfoList);
	
			
				
				// If the user has selected to hide controls that are registered already remove
				// them from the list before displaying the list.
				if (hideRegisteredControls == true && userName != null) {
				
					for (Control currentControl : controlList) {
						
						for (int index = 0; index < updateControlInfoList.size(); index++) {
							
							if (updateControlInfoList.get(index).getControlNumber().equals(currentControl.getControlNumber())) {
								updateControlInfoList.remove(index);
								break;
							}
						}
					}
				}
				
				// Add the filtered list to the listadapter for display
				for (ControlInfo currentControlInfo: updateControlInfoList) {
					theControlInfoListAdapter.add(currentControlInfo);
				}
			}
		
		theControlInfoListAdapter.notifyDataSetChanged();
	} // end updateListControlAdapter()
	
	
	
	/**
	 * Updates the listview adapter with the current state of the controls in the list. If the 
	 * user has selected to hide already stored controls, these are not included in the list.
	 */
	private void updateListContent() {
    	
    	// Read the controlinfo list from local user object.
		if (userName != null) {
			
			try {
				registeredUser = (RegisteredMobileUser)FileHandler.getObjectFromFile(openFileInput(FileHandler.REGISTERED_USER_FILENAME));
				controlList = registeredUser.getControls();
			} 
			catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			registeredControlHashMap.clear();
	    	
	    	for (Control currentControl : controlList) {
	    		registeredControlHashMap.put(Integer.valueOf(currentControl.getControlNumber()), currentControl);
	    	}
	    	
        	// Retrieve the controls that are only stored locally in the phone and not on the server.
			failedStoreControlList = registeredUser.getRegisterFailedControls();
			failedRegisterControlHashMap.clear();
        	
        	for (Control currentControl : failedStoreControlList) {
        		failedRegisterControlHashMap.put(Integer.valueOf(currentControl.getControlNumber()), currentControl);
        	}
		}
		
		updateListControlAdapter(controlInfosList);
	} // end updateListControlAdapter()
	
	
	/**
	 * Opens the view which allows the user to register controls.
	 * 
	 * @param controlNumber a String that is the control number for the selected control.
	 */
	private void openRegisterControlView(String controlNumber) {
		Intent registerControlIntent = new Intent(this, RegisterControlView.class);
		registerControlIntent.putExtra("control_number", controlNumber);
		startActivity(registerControlIntent);
	} // end startScannerForSelectedItem

	
    /**
     * Register the controls in the control list which have been scanned but not registered
     * at the servers yet.
     * 
     * @return a boolean indicating false if the registration was unsuccessful and true if the registration was successful.
     */
    private int registerFailedControlsOnServer() {

    	int result = RESULT_UNDEFINED;

    	RestClient restClient = new RestClient();
    	
		// Load the lates information from file to examine if there are any controls to send.
		try {
			registeredUser = 
					(RegisteredMobileUser) FileHandler.getObjectFromFile(openFileInput(FileHandler.REGISTERED_USER_FILENAME));
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    	
		if (registeredUser != null) {
			if (registeredUser.getRegisterFailedControls().size() > 0) {
				result = restClient.registerControls(registeredUser.getRegisterFailedControls(), userName, 
						 sharedPreferences.getString("password", null));
			}
			else {
				result = NOTHING_TO_UPDATE;
			}
		}
		
		return result;
    } // end registerFailedControlsOnServer()

	
    /**
     * Private Asynch Task class used to register controls to the server that the application
     * has failed to send to the server earlier.
     */
    private class RegisterFailedControlsTask extends AsyncTask<Void, Void, Integer> {
    	
    	@Override
		protected void onPreExecute() {
    		showDialog(PROGRESS_DIALOG_ID);
        } // end onPreExecute()

        @Override
		protected Integer doInBackground(Void... params) {
        	return registerFailedControlsOnServer();
        } // end doInBackground()

        @Override
        protected void onPostExecute(Integer result) {
        	super.onPostExecute(result);
        	requestProgressDialog.dismiss();
        	
        	if (result == RestClient.RESULT_WAS_OK) {

	        	// Since everything was ok, clear the lists of failed registrations before writing the
        		// registered user object to file.
				failedStoreControlList.clear();
				failedRegisterControlHashMap.clear();
				registeredUser.setRegisterFailedControls(failedStoreControlList);
				
				// TODO: Should I not also add the controls from the failed register controls?
				try {
					FileHandler.writeObjectToFile(registeredUser, 
							openFileOutput(FileHandler.REGISTERED_USER_FILENAME, Context.MODE_PRIVATE));
				} 
				catch (FileNotFoundException e) {
					e.printStackTrace();
				}
	    	}
			else if (result == RestClient.CONTROL_ALREADY_REGISTERED) {
				showDialog(CONTROL_ALREADY_REGISTERED);
			}
			else if (result == RestClient.INVALID_CONTROL) {
				showDialog(INVALID_CONTROL);
			}
			else if (result == RESULT_UNDEFINED) {
				showDialog(STORAGE_NOT_POSSIBLE);
			}
    		else if (result == NOTHING_TO_UPDATE) {
    			showDialog(NOTHING_TO_UPDATE);
    		}
			else {
			}

			updateListContent();
        } // end onPostExecute()
        
    } // end class StoreToServerTask

	
	/**
	 * Custom list Adapter for the listview that accepts Control objects and display their
	 * data in the list.
	 */
	private class ControlInfoAdapter extends ArrayAdapter<ControlInfo> {

        private ArrayList<ControlInfo> items;
        
        public ControlInfoAdapter(Context context, int textViewResourceId, ArrayList<ControlInfo> items) {
                super(context, textViewResourceId, items);
                this.items = items;
        } // end constructor for ControlAdapter
        
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    v = vi.inflate(R.layout.control_list_listrow_layout, null);
                }
                
                ControlInfo currentControlInfo = items.get(position);
                
                if (currentControlInfo != null) {
                	
            		ImageView ivStatusImage = (ImageView) v.findViewById(R.id.ivStatusImage);
                    TextView tvListRowControlNumber = (TextView) v.findViewById(R.id.tvListRowControlNumber);
                    TextView tvListRowControlDescription = (TextView) v.findViewById(R.id.tvListRowControlDescription);
                    
                    if (tvListRowControlNumber != null) {
                    	tvListRowControlNumber.setText(currentControlInfo.getControlNumber());
            		}
            		
                    if (userName != null) {
                    	
	                    if(ivStatusImage != null) {
	
	                    	Integer controlNumber = Integer.valueOf(currentControlInfo.getControlNumber());
	                    	
	        				if (currentControlInfo.getControlColor().equals("Grön")) {
	        					if (failedRegisterControlHashMap.containsKey(controlNumber)) {
	                				ivStatusImage.setImageResource(R.drawable.control_need_synch_green);
	        					}
	        					else if (registeredControlHashMap.containsKey(controlNumber)) {
	        						ivStatusImage.setImageResource(R.drawable.registered_control_green);
	        					}
	        					else {
	                				ivStatusImage.setImageResource(R.drawable.needs_scan_control_green);
	        					}
	        				}
	                    	
	        				else if (currentControlInfo.getControlColor().equals("Blå")) {
	        					
	        					if (failedRegisterControlHashMap.containsKey(controlNumber)) {
	                				ivStatusImage.setImageResource(R.drawable.control_need_synch_blue);
	        					}
	        					else if (registeredControlHashMap.containsKey(controlNumber)) {
	                				ivStatusImage.setImageResource(R.drawable.registered_control_blue);
	        					}
	        					else {
	                				ivStatusImage.setImageResource(R.drawable.needs_scan_control_blue);
	        					}
	        				}
	
	            			else if (currentControlInfo.getControlColor().equals("Röd")) {
	        					if (failedRegisterControlHashMap.containsKey(controlNumber)) {
	                				ivStatusImage.setImageResource(R.drawable.control_need_synch_red);
	        					}
	        					else if (registeredControlHashMap.containsKey(controlNumber)) {
	        						ivStatusImage.setImageResource(R.drawable.registered_control_red);
	        					}
	        					else {
	                				ivStatusImage.setImageResource(R.drawable.needs_scan_control_red);
	        					}	
	            			}
	        				else if (currentControlInfo.getControlColor().equals("Svart")) {
	        					if (failedRegisterControlHashMap.containsKey(controlNumber)) {
	                				ivStatusImage.setImageResource(R.drawable.control_need_synch_black);
	        					}
	        					else if (registeredControlHashMap.containsKey(controlNumber)) {
	        						ivStatusImage.setImageResource(R.drawable.registered_control_black);
	        					}
	        					else {
	                				ivStatusImage.setImageResource(R.drawable.needs_scan_control_black);
	        					}	
	        				}
	                    }
                    }
                    else {
                    	if (currentControlInfo.getControlColor().equals("Grön")) {
            				ivStatusImage.setImageResource(R.drawable.needs_scan_control_green);
                    	}
                    	else if (currentControlInfo.getControlColor().equals("Blå")) {
            				ivStatusImage.setImageResource(R.drawable.needs_scan_control_blue);
                    	}
                    	else if (currentControlInfo.getControlColor().equals("Röd")) {
            				ivStatusImage.setImageResource(R.drawable.needs_scan_control_red);
                    	}
                    	else {
            				ivStatusImage.setImageResource(R.drawable.needs_scan_control_black);
                    	}
                    }
                    
            		if(tvListRowControlDescription != null){
            			tvListRowControlDescription.setText(currentControlInfo.getDescription());
            		}
                }
                return v;
        }
	} // end class OrderAdapter
	
	
	/**
	 * If the user attempts to register a control without being registred, a view to allow
	 * new registration or synch with existing account at Stamford should be shown.
	 */
	private void openLoginOrRegisterUserView() {
		startActivity(new Intent (this, LoginOrRegisterView.class));
	} // end openLoginOrRegisterUserView()
	
	
	@Override
	protected Dialog onCreateDialog(int id) {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		
		switch(id) {
	        case PROGRESS_DIALOG_ID:
	        	requestProgressDialog = new ProgressDialog(this);
	        	requestProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	        	requestProgressDialog.setMessage("Skickar lokalt sparade kontroll(er) till Friska Karlstad...");
	            return requestProgressDialog;
	        case USER_NOT_REGISTERED:
	        	builder = new AlertDialog.Builder(this);
	        	builder.setMessage("Du är inte registrerad hos Friska Karlstad Ännu. Tryck på \"Ok\" om du vill gå till registrering.")
	        	       .setCancelable(true)
	        	       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	        	           public void onClick(DialogInterface dialog, int id) {
	        	        	   dialog.cancel();
	        	        	   openLoginOrRegisterUserView();
	        	           }
	        	       }).setNegativeButton("Avbryt", new DialogInterface.OnClickListener() {
	        	           public void onClick(DialogInterface dialog, int id) {
	        	        	   dialog.cancel();
	        	           }
	        	       });
	        	break;
	        case STORAGE_NOT_POSSIBLE:
	        	builder.setMessage("Kontrollen/Kontrollerna kunde inte skickas till Friska Karlstad men är sparade lokalt.")
	        	       .setCancelable(false)
	        	       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	        	           public void onClick(DialogInterface dialog, int id) {
	        	        	   dialog.cancel();
	        	           }
	        	       });
	        	break;
	        case CONTROL_ALREADY_REGISTERED:
	        	builder.setMessage("Kontrollen/Kontrollerna har redan registrerats hos Friska Karlstad.")
	        	       .setCancelable(false)
	        	       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	        	           public void onClick(DialogInterface dialog, int id) {
	        	        	   dialog.cancel();
	        	           }
	        	       });
	        	break;
	        case INVALID_CONTROL:
	        	builder.setMessage("Ogiltig kontroll/kontroller.")
	        	       .setCancelable(false)
	        	       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	        	           public void onClick(DialogInterface dialog, int id) {
	        	        	   dialog.cancel();
	        	           }
	        	       });
	        	break;
	        case NOTHING_TO_UPDATE:
	        	builder.setMessage("Det fanns inga kontroller som behövde skickas till Friska Karlstad.")
	        	       .setCancelable(false)
	        	       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	        	           public void onClick(DialogInterface dialog, int id) {
	        	        	   dialog.cancel();
	        	           }
	        	       });
	        	break;
	        case RESULT_WAS_OK:
	        	builder.setMessage("Kontroll/Kontroller sparade hos Friska Karlstad.")
	        	       .setCancelable(false)
	        	       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	        	           public void onClick(DialogInterface dialog, int id) {
	        	        	   dialog.cancel();
	        	           }
	        	       });
	        	break;
	        default:
	        	break;
        }

        return builder.create();
    } // end onCreateDialog()

	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.control_list_options_menu, menu);
        return true;
    } // end onCreateOptionsMenu()

	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	
        switch (item.getItemId()) {
        	
        	// Open the settings view
        	case R.id.options_settings:
        		startActivityForResult(new Intent(this, EditPreferences.class), CHANGE_SETTINGS_REQ);
        		return true;
        	
        	// Open the help page (in browser)
        	case R.id.control_list_options_help:
    			Intent browserIntent = new Intent(this, InformationView.class);
    			browserIntent.putExtra("browser_url", "file:///android_asset/control_list_menu_help.html");
    			startActivity(browserIntent);
	            return true;
	            
	        // Synchronizes unregistered controls with server
        	case R.id.synch_with_server:
        		new RegisterFailedControlsTask().execute();
	            return true;
	            
	        default:
	            return super.onOptionsItemSelected(item);
        }
        
    } // end onOptionsItemSelected()
    
    
    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		if (requestCode == CHANGE_SETTINGS_REQ) {
	        hideRegisteredControls = sharedPreferences.getBoolean("hide_registered_controls_in_list", false);
			updateListContent();
	    }
	} // end onActivityResult()
    
    
    @Override
    protected void onRestart() {
    	super.onRestart();
    	updateListContent();
    }
    
    
    /**
     * Displays a toast message on screen
     * @param toastText a String that is the text to display.
     */
    private void showToast(String toastText) {
    	int duration = Toast.LENGTH_SHORT;
    	Toast toast = Toast.makeText(this, toastText, duration);
    	toast.show();
    } // end showToast()
    
} // end class ControlListView
