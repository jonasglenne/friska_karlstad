package friskakarlstad.com.activities;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import friskakarlstad.com.R;
import friskakarlstad.com.utilities.RestClient;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

public class StatisticsView extends Activity {
	
	/**************************************************************
	 * Result codes for different methods called within the Activity.
	 **************************************************************/
	
	public final static int PROGRESS_DIALOG_ID = 0;
	
	/**************************************************************
	 * Widgets for this activity.
	 **************************************************************/
	
	// Heading for this view.
	private TextView tvTop100Title;

	// Webview to display top list of users.
	private WebView statsWebView;
	
	// Button to fetch latest update.
	private Button btnPreviousHunded;
	
	private Button btnNextHundred;
	
	/**************************************************************
	 * Utility attributes for this activity.
	 **************************************************************/
			
	// Progress dialog to show during lengthy operations.
	private ProgressDialog progressDialog;

	// Shared preferences for this application.
	private SharedPreferences sharedPreferences;
	
	// Loaded from the preference attribute "user_name". If this is null after onCreate 
	// it means the user is not registered.
	private String userName;
	
	// Http client that handles communication with remote servers.
	private RestClient restClient;
	
	private int offset;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistics_listview_layout);
		
        restClient = new RestClient();
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

		// Check if there is a retained stateful object from a change in screen orientation. If not, load the list
		// from file.
		final Object retainedOffsetValue = getLastNonConfigurationInstance();
		
		if (retainedOffsetValue != null) {
	    	offset = (Integer) retainedOffsetValue;
	    }
		else {
	        offset = 0;
		}
		
		// Find out if the user wants to hide already registered controls or not.
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        
        tvTop100Title = (TextView) findViewById(R.id.tvTop100Title);
        
        statsWebView = (WebView) findViewById(R.id.resultsWebView);
        statsWebView.setPadding(0, 0, 0, 0);
        statsWebView.setHorizontalScrollBarEnabled(false);
        btnPreviousHunded = (Button) findViewById(R.id.btnPreviousHundred);
        btnPreviousHunded.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	offset -= 100;
            	new DownloadStatisticsAsyncTask().execute();
            }
        });
        btnPreviousHunded.setText("1-100");
        btnPreviousHunded.setEnabled(false);
        
        btnNextHundred = (Button) findViewById(R.id.btnNextHundred);
        btnNextHundred.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	offset += 100; 
            	new DownloadStatisticsAsyncTask().execute();
            }
        });
        btnNextHundred.setText("101-200");
        
        new DownloadStatisticsAsyncTask().execute();
		
    } // end onCreate()
	
	
	/**
	 * Updates the statistics view with fresh statistics from the server.
	 */
	private void updateUserStatistics(String topListHTMLString) {
		
		statsWebView.loadData(topListHTMLString, "text/html", null);
		
		if (offset == 0) {
	        btnPreviousHunded.setEnabled(false);
		}
		else {
			btnPreviousHunded.setEnabled(true);
		}
		
		// Update button text. Offset starts at 0 so add + 1 to get position
		btnPreviousHunded.setText(String.valueOf(offset + 1) + "-" + String.valueOf(offset + 100));
		btnNextHundred.setEnabled(true);
		btnNextHundred.setText(String.valueOf(offset + 101) + "-" + String.valueOf(offset + 200));

	} // end updateUserStatisticsListView()
	
	
    /**
     * Asynch Task class used to get statistics from Friska Karlstad 
     */
    private class DownloadStatisticsAsyncTask extends AsyncTask<Void, Void, String> {
    	
    	@Override
		protected void onPreExecute() {
    		btnPreviousHunded.setEnabled(false);
    		btnNextHundred.setEnabled(false);
    		showDialog(PROGRESS_DIALOG_ID);
    	} // end onPreExecute()
    	
        @Override
		protected String doInBackground(Void... params) {
        	return restClient.getTopList(offset);
        } // end doInBackground()
        
        protected void onProgressUpdate() {}
        
        protected void onPostExecute(String topListHTMLString) {
        	dismissDialog(PROGRESS_DIALOG_ID);
        	updateUserStatistics(topListHTMLString);        
        }
    } // end class SynchWithServerAsyncTask
    
	
    /**
     * Displays a progress dialog for the user.
     * 
     * @param title a String that is the title of the progress dialog.
     * @param description a String that is the descriptive text for the dialog.
     */
	private void showProgressDialog(String title, String description){

		// Start the progress dialog for download of controls from Stamford.
		progressDialog = ProgressDialog.show(StatisticsView.this, title, description, 
												true, true, new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				progressDialog.dismiss();
			}
		});
	} // end showProgressDialog()
	
	
	@Override
	protected Dialog onCreateDialog(int id) {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		
		switch(id) {
	        case PROGRESS_DIALOG_ID:
	        	progressDialog = new ProgressDialog(this);
	        	progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	        	progressDialog.setMessage("Hämtar statistik från Friska Karlstad...");
	            return progressDialog;
	        default:
	        	break;
        }

        return builder.create();
    } // end onCreateDialog()
	
	
	// Android function to retain a stateful object if the activity is destroyed. This
	// stateful object can be retrieved if the activity restarts.
	@Override
	public Object onRetainNonConfigurationInstance() {
	    final int retainedOffset = offset;
	    return retainedOffset;
	} // end onRetainNonConfigurationInstance()
    
	
	@Override
	public void onBackPressed() {
		if (progressDialog != null) {
			dismissDialog(PROGRESS_DIALOG_ID);
		}
	    finish();
	}
	
} // end class StatisticsView
