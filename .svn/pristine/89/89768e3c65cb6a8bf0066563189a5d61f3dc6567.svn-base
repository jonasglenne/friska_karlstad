package jonas.com.friskakarlstad.utilities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import jonas.com.friskakarlstad.activities.MainMenuView;


import android.os.Environment;
import android.util.Log;

public class FileHandler {

	private static final String CLASSNAME = "ControlFileHandler";
	
	public static final String UNREGISTERED_USER_FILENAME = "unregistered_user.obj";
	public static final String REGISTERED_USER_FILENAME = "registered_user.obj";
	
	/**
	 * Takes an FileInputStream and reads it into an object that is returned to the calling 
	 * class.
	 * 
	 * @param inStream a FileInputStream that is the received data from a server.
	 * @return an Object that is an object retrieved from file.
	 */
	@SuppressWarnings("unchecked")
	public static synchronized Object getObjectFromFile(FileInputStream inStream) {

		ObjectInputStream objIn = null;
		Object readFromFileObject = null;
		// Try to open the locally stored file. If it is the first time the program is started
		// this will fail since the file has not been created yet. In that case simply save all
		// controls that have been downloaded to the file.
		try {
			objIn = new ObjectInputStream(inStream);
			
			if (objIn != null) {
				readFromFileObject = objIn.readObject();
			}
			
		}
		catch (IOException e) {
			Log.e(MainMenuView.CLASSNAME, "Something went wrong when reading the object from file.");
			e.printStackTrace();
		} 
		catch (ClassNotFoundException e) {
			Log.e(MainMenuView.CLASSNAME, "Could not find the object file.");
			e.printStackTrace();
		}
		finally {
			if (objIn != null) {
				try {
					objIn.close();
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return readFromFileObject;
	} // end getObjectFromFile()

	
	/**
	 * Takes an FileOutputStream and writes to an ObjectOutputStream which is saved in the internal storage
	 * of the application.
	 * 
	 * @param theObjectToWrite an ArrayList<Control> object which shall be written to internal storage.
	 * @param outStream an FileOutputStream used to write the control list to internal storage.
	 */
	public static synchronized void writeObjectToFile(Object theObjectToWrite, FileOutputStream outStream) {
		Log.i(CLASSNAME, "Writing the local control list to file.");
		
		ObjectOutputStream objOut = null;
		
		try {
			objOut = new ObjectOutputStream(outStream);

			// If the local list is null, it means that it is the first time the list has been downloaded
			// from the server. In that case write the complete list from the server to internal storage.
			// Otherwise, save the update local control list.
			if (theObjectToWrite != null) {
				objOut.writeObject(theObjectToWrite);
			}
		}
		catch (IOException e) {
			Log.e(MainMenuView.CLASSNAME, "Something went wrong when writing object to file.");
			e.printStackTrace();
		}
		finally {
			if (objOut != null) {
				try {
					objOut.close();
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	} // end writeObjectToFile()
	
	/**
	 * Checks if everything if the external media is writeable
	 */
	public static String checkExternalStorageAvailability() {
		
		String sdCardState = "";
		
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_UNMOUNTED)) {
			Log.w(CLASSNAME, "Memory card not mounted");
			sdCardState = Environment.MEDIA_UNMOUNTED;
		}
		else if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED_READ_ONLY)) {
			Log.w(CLASSNAME, "Memory card not writeable");
			sdCardState = Environment.MEDIA_MOUNTED_READ_ONLY;
	    }
		else if (Environment.getExternalStorageState().equals(Environment.MEDIA_SHARED)) {
			Log.w(CLASSNAME, "Memory card shared via USB");
			sdCardState = Environment.MEDIA_SHARED;
	    } 
		else {
	    	Log.w(CLASSNAME, "No problems accessing SD card");
			sdCardState = Environment.MEDIA_MOUNTED;
	    }
		return sdCardState;
	} // end checkExternalStorageAvailability()
	
} // end class ControlFileHandler
