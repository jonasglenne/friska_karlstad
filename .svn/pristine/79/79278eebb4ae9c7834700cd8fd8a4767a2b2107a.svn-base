package jonas.com.friskakarlstad.pojos;

import java.util.ArrayList;
import java.util.Collection;




/**
 * This class contains information about Friska Karlstad and is used by an unregistered
 * user. After the user is registered, this object is not used anymore.
 * 
 * @author jge
 */

public class MobileUnregisteredUser {

	//The name of the project.
    private String projectName;

    // A text description of the project.
    private String projectDescription;

    // Flag to indicate if there has been any changes to the control list.
    private Integer controlInfosVersion;

    // Contains information about the map.
	private MapObject mapObject;
	
	// A list of controls and information about them.
	private ArrayList<ControlInfo> controlInfos;

	// A list of news for this project.
	private ArrayList<NewsFlash> newsFlashes;

	/**
	 * Constructor without arguments. Creates a clean mobileunregistereduser.
	 */
    public MobileUnregisteredUser() {
    } // end constructor MobileUnregisteredUser()

    /***********************************************
     * Getters and setters for class MobileUnregistered User.
     *************************************************/

    
    public MapObject getMapObject() {
		return mapObject;
	}

	public void setMapObject(MapObject mapObject) {
		this.mapObject = mapObject;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectDescription() {
		return projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	public Integer getControlInfosVersion() {
		return controlInfosVersion;
	}

	public void setControlInfosVersion(Integer controlInfosVersion) {
		this.controlInfosVersion = controlInfosVersion;
	}

	public Collection<ControlInfo> getControlInfos() {
		return controlInfos;
	}

	public void setControlInfos(ArrayList<ControlInfo> newControlInfos) {
		controlInfos = newControlInfos;
	}

	public Collection<NewsFlash> getNewsFlashes() {
		return newsFlashes;
	}

	public void setNewsFlashes(ArrayList<NewsFlash> newNewsFlashes) {
		newsFlashes = newNewsFlashes;
	}
} // end class MobileUnregisteredUser