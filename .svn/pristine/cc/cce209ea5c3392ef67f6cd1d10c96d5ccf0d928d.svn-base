package friskakarlstad.com.activities;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import friskakarlstad.com.R;
import friskakarlstad.com.pojos.Control;
import friskakarlstad.com.pojos.ControlInfo;
import friskakarlstad.com.pojos.RegisteredMobileUser;
import friskakarlstad.com.pojos.UnregisteredMobileUser;
import friskakarlstad.com.utilities.CustomMyLocationOverlay;
import friskakarlstad.com.utilities.FileHandler;
import friskakarlstad.com.utilities.ItemizedMapMarkerOverlay;
import friskakarlstad.com.utilities.MapItemsOverlay;
import friskakarlstad.com.utilities.MeasureStickOverlay;
import friskakarlstad.com.utilities.RestClient;

import org.apache.http.HttpEntity;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ZoomControls;

/**
 * Displays a map to the user with mapoverlays and controls plotted on the map.
 * 
 * @author jonast42@yahoo.com
 */
public class MapNavigatorView extends MapActivity {
	
	/**************************************************************
	 * Result codes for different methods called within the Activity.
	 **************************************************************/
	
	// Activity request codes to be used in onActivityResult;
	public static int CHANGE_SETTINGS_REQ = 1;
	public static int CHANGE_GPS_SETTING_REQ = 2;
	
	// Dialog id's to be used for dialogs;
	public static final int MAP_TILES_ALERT = 3;
	public static final int MEM_CARD_NOT_MOUNTED_ALERT = 4;
	public static final int MEM_CARD_READ_ONLY_ALERT = 5;
	public static final int GPS_DISABLED_ALERT = 6;
	public static final int MAP_TILES_DOWNLOAD_FAILED = 7;
	public static final int MAP_TILES_SUCCESS = 8;

	/**************************************************************
	 * Widgets for this activity.
	 **************************************************************/
	
	// The mapview used to select a location for the end or start of a travel.
	private MapView theMapView;

	// An overlay for the map tiles.
	private MapItemsOverlay mapOverlay;

	// Contains all the overlay items for the control markers.
	private ItemizedMapMarkerOverlay theMarkerOverlay;

	// An overlay to display a measure stick to the user.
	private MeasureStickOverlay measureStick;
	
	// An overlay to show current user position.
	private MyLocationOverlay myLocationOverlay;

	// Progress dialog for download of map tiles.
	private ProgressDialog progressDialog;
	
	/**************************************************************
	 * Utility attributes for this activity.
	 **************************************************************/
	
	// Communicates via the REST-API on google app-engine.
	private RestClient restClient;

	// Controller for the map.
	private MapController mapController;

	// The location manager for this view.
	private LocationManager locationManager;
	
	// The locationlistener for this view.
	private LocationListener locationListener;
	
	// A list containing all marker and map tile overlays.
	private List<Overlay> overlays;
	
	// A list containing all the overlays for marker items.
	private List<ControlInfo> controlInfoList;
	private List<Control> controlList;
	
	// Holds the current zoom level on the map.
	private int currentZoomLevel;
		
	// Indicates whether the users position is shown on the map or not.
	private boolean showOwnPositionOnMap;

	// Indicates whether the compass is shown on the map or not.
	private boolean showCompassOnMap;

	// Indicates whether to show already stored controls or not.
	private boolean hideRegisteredControlsInMap;
	
	// Determines whether the measure stick should be shown or not.
	private boolean showMeasureScaleOnMap;

	// TODO Try to change to the new way of showing zoom controls. This may be outdated in the future.
	// The zoom controls for this map view
	private ZoomControls theZoomControls;
	
	// Determines the minimum zoom level (from google maps available i.e. 1-17)
	private final int MIN_ZOOM_LEVEL = 11;

	// Determines the maximum zoom level (from google maps available i.e. 1-17)
	private final int MAX_ZOOM_LEVEL = 17;

	// Boolean indicating whether the initial overlays are still being assembled. 
	// To avoid crashes, no changes may be done to the overlay through zoom, pan, etc.
	private boolean stillLoadingUp;
	
	// The shared preferences for this application.
	private SharedPreferences settings;
	
	// An array list that holds ArrayLists of Map Tile overlays. Each overlay represents the map
	// tile from zoom level 11 - 16 (17-20 will use level 16 with zoom).
	private ArrayList<ArrayList<Overlay>> theMapTileOverlayList;
	
	private String userName;
	
	// Contains the registered users account if any is registered, otherwise null
	RegisteredMobileUser registeredUser;
	
	// Contains the unregistered users information if no account has been created yet.
	UnregisteredMobileUser unregisteredUser;
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_navigator_view_layout);

        restClient = new RestClient();
        settings = PreferenceManager.getDefaultSharedPreferences(this);
	    userName = settings.getString("username", null);
	    
		// This boolean helps prevent changes to the map before the initial loading of the map tiles.
		// Changes before this point has shown to be prone to lead to a crash.
		stillLoadingUp = true;
		
        theMapView = (MapView)findViewById(R.id.MapNavigatorMapView);

		theMapView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction()==MotionEvent.ACTION_UP && stillLoadingUp != true) {
					changeMapTilesLevel();
				}
	            return false;
	        }
	    });
		
	    theZoomControls = (ZoomControls) findViewById(R.id.zoomcontrols);
	    theZoomControls.setOnZoomInClickListener(new View.OnClickListener() {
    		@Override
            public void onClick(View v) {
				if (stillLoadingUp != true && MapItemsOverlay.numberOfLoaded < 60) {
					System.gc();
        			mapController.zoomIn();
        			changeMapTilesLevel();
				}
				else {
					System.gc();
					recycleBitMaps();
				}
            }
        });
	    theZoomControls.setOnZoomOutClickListener(new View.OnClickListener() {
    		@Override
            public void onClick(View v) {

        		if (stillLoadingUp != true && MapItemsOverlay.numberOfLoaded < 60) {
					System.gc();
        			mapController.zoomOut();
        			changeMapTilesLevel();
    			}
				else {
					System.gc();
					recycleBitMaps();
				}
            }
        });
	    
        mapController = theMapView.getController();
        currentZoomLevel = Integer.valueOf(settings.getString("initial_map_zoom_level", "14"));
	    mapController.setZoom(currentZoomLevel);
	    
    	// Prepare to receive location updates.
	    locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		locationListener = new LocationListener() {
			
		    public void onLocationChanged(Location location) {
		    	GeoPoint updatedGeoPoint = new GeoPoint( (int)(location.getLatitude() * 1e6),
		    											(int)(location.getLongitude() * 1e6));
		    	// TODO Consider removing this or make it configurable.
		    	if (settings.getBoolean("move_to_position", false)) {
	    			mapController.animateTo(updatedGeoPoint);
		    	}
		    }

		    public void onStatusChanged(String provider, int status, Bundle extras){}
		    public void onProviderEnabled(String provider){}
		    public void onProviderDisabled(String provider){}
		};
		
	    // Check if GPS is activated. If not, offer to the user to switch it on.
	    if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
	    	startLocationListener(LocationManager.GPS_PROVIDER);
	    }
	    else {
	    	startLocationListener(LocationManager.NETWORK_PROVIDER);
    		showDialog(GPS_DISABLED_ALERT);
	    }
		
		// Read the control list from local file.
		try {
			if (userName != null) {
				registeredUser = 
						(RegisteredMobileUser)FileHandler.getObjectFromFile(openFileInput(FileHandler.REGISTERED_USER_FILENAME));
				controlList = registeredUser.getControls();
				Collections.sort(controlList);
				controlInfoList = registeredUser.getControlInfos();
				Collections.sort(controlInfoList);
				
				// If the user has not unlocked the phone. Remove all controls that are not
				// of color green.
				if (registeredUser.getCodeHasBeenVerified() != null) {
					if (registeredUser.getCodeHasBeenVerified() != true) {
						controlInfoList = filterLockedListContent(controlInfoList);
					}
				}
			}
			else {
				controlList = new ArrayList<Control>();
				unregisteredUser = 
						(UnregisteredMobileUser)FileHandler.getObjectFromFile(openFileInput(FileHandler.UNREGISTERED_USER_FILENAME));
				controlInfoList = unregisteredUser.getControlInfos();
				controlInfoList = filterLockedListContent(controlInfoList);
			}
		}
		catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		
		// Populate the map overlays asynchronously.
		overlays = theMapView.getOverlays();
		new PopulateInitialOverlays().execute();
		
	} // end onCreate()

	
	/**
	 * Filters out paid controls if the user has not paid and entered the registration code.
	 * 
	 * @param unfilteredControlInfoList a List of all ControlInfos for the project.
	 * @return a List of ControlInfo's that has been filtered
	 */
	private List<ControlInfo> filterLockedListContent(List<ControlInfo> unfilteredControlInfoList) {
		
		List<ControlInfo> filteredList = new ArrayList<ControlInfo>();
		
		for (ControlInfo currentControlInfo : unfilteredControlInfoList) {
			if (!currentControlInfo.getAvailableTo().equalsIgnoreCase("PaidPrivate")) {
				
				filteredList.add(currentControlInfo);
			}
		}
		
		return filteredList;
		
	} // end filterUnlockedListContent()
	
	
	// TODO: Seems superfluous no calls to this method...
	/**
	 * Updates the overlays
	 * @author jge
	 *
	 */
	private class UpdateMapOverlaysTask extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected Boolean doInBackground(Void... params) {
			stillLoadingUp = true;
			changeMapTilesLevel();
    		return true;
    	} // end doInBackground()

        protected void onProgressUpdate(Void v) {}

        @Override
		protected void onPostExecute(Boolean result) {
		    overlays.clear();
 		    
		    overlays.addAll(theMapTileOverlayList.get(currentZoomLevel -11));
	    	updatePrefrenceMapping();
		    stillLoadingUp = false;
        } // end onPostExecute()
	}
	
	
	/**
	 * Handles change of map tiles if the user have zoomed in or out.
	 */
	private void changeMapTilesLevel() {
		int oldZoomLevel = currentZoomLevel;
		currentZoomLevel = theMapView.getZoomLevel();

		stillLoadingUp = true;
		
		if ((oldZoomLevel != currentZoomLevel && oldZoomLevel != 17) &&
			(currentZoomLevel < MAX_ZOOM_LEVEL && currentZoomLevel > MIN_ZOOM_LEVEL)
			) {
			recycleBitMaps();
			overlays.clear();
 		    // TODO Fix crash when no map available.
		    if (theMapTileOverlayList.size() > 0) {
		    	overlays.addAll(theMapTileOverlayList.get(currentZoomLevel -11));
		    }
	    	updatePrefrenceMapping();
		}
		if (showMeasureScaleOnMap == true) {
			measureStick.changeZoomLevel(currentZoomLevel);
		}
		stillLoadingUp = false;
	} // end changeMapTilesLevel()
	
	
	/**
	 * Recycles the current bitmaps in the Overlay List.
	 */
	private void recycleBitMaps() {
			
		for (int index = 0; index < overlays.size(); index++) {
			if (overlays.get(index) instanceof MapItemsOverlay) {
				((MapItemsOverlay)overlays.get(index)).recycleBitmap();
			}
		}
		System.gc();
	} // end recycleBitMaps()
	
	
	/**
	 * Starts the location listener with the given provider
	 * 
	 * @param provider a String that represents the provider to select.
	 */
	private void startLocationListener(String provider) {
		locationManager.removeUpdates(locationListener);
		locationManager.requestLocationUpdates(provider, 5000, 10, locationListener);
		
		Location lastKnownLocation = locationManager.getLastKnownLocation(provider);
		
	    if (lastKnownLocation != null) {
	    	GeoPoint theLastKnownGeoPoint = new GeoPoint((int)(lastKnownLocation.getLatitude() * 1e6), 
	    											  	 (int)(lastKnownLocation.getLongitude() * 1e6));
	    	mapController.animateTo(theLastKnownGeoPoint);
	    }
	} // end startLocationListener()

	
	/**
	 * Updates the settings of the compass, mylocation overlay, measurestick and whether to show 
	 * registered controls or not in the map. 
	 */
	private void updatePrefrenceMapping() {
        hideRegisteredControlsInMap = settings.getBoolean("hide_registered_controls_in_map", false);
        showCompassOnMap = settings.getBoolean("show_compass_on_map", false);
        showMeasureScaleOnMap = settings.getBoolean("show_measure_scale_on_map", false);
        showOwnPositionOnMap = settings.getBoolean("show_my_position_on_map", false);
        
		toggleMyPosition();
		toggleCompass();
		toggleShowRegisteredControls();
		toggleMeasureStick();
		
    	theMapView.invalidate();
	} // end updatePrefrenceMapping()
	
	
	/**
	 * Populates the markers that is the controls in the overlay for this map view.
	 * 
	 * @return a boolean indicating whether the populating of markerOverlays went well or not.
	 */
	private void populateMarkerOverlays() {
		
		theMarkerOverlay = new ItemizedMapMarkerOverlay(getResources().getDrawable(R.drawable.pink_marker), this);
		
		// Arrange the controlinfos in a hashmap to be able to retrieve them based on the key which is 
		// the control number.
		HashMap<Integer, ControlInfo> mappedControlInfos  = new HashMap<Integer, ControlInfo>();

		for (ControlInfo currentControlInfo : controlInfoList) {
			mappedControlInfos.put(Integer.valueOf(currentControlInfo.getControlNumber()), currentControlInfo);
		}
		
		// Prepare a grey ring with a green check mark in it to indicate already registered control.
		Drawable grey_marker = getResources().getDrawable(R.drawable.grey_marker);
		grey_marker.setBounds(0, 0, grey_marker.getIntrinsicWidth(), grey_marker.getIntrinsicHeight());

		if (controlList != null && controlInfoList != null) {
			
			// Check which controls have been registered and if they are to be shown create OverlayItems for them
			for (Control currentControl : controlList) {

				int controlNumber = Integer.parseInt(currentControl.getControlNumber());
				
				if (hideRegisteredControlsInMap == false) {
					// Convert all latitude and longitude combinations to GeoPoints to be able to add
					// them to the constructor of the OverlayItem.
					GeoPoint markerPoint = new GeoPoint( (int)(mappedControlInfos.get(controlNumber).getLatitude() * 1e6), 
													(int)(mappedControlInfos.get(controlNumber).getLongitude() * 1e6));
	
					OverlayItem overlayitem = new OverlayItem(markerPoint, currentControl.getControlNumber(), null);
					overlayitem.setMarker(theMarkerOverlay.centerOverlay(grey_marker));
					theMarkerOverlay.addOverlay(overlayitem);
				}
				
				// Remove the corresponding controlinfo since we only want to have pink markers on those controls
				// that have not been registered yet.
				mappedControlInfos.remove(controlNumber);
			}
			
			// For the rest, create OverlayItems with the standard marker.
			for (ControlInfo currentControlInfo : mappedControlInfos.values()) {
					
					// Convert all latitude and longitude combinations to GeoPoints to be able to add
					// them to the constructor of the OverlayItem.
					GeoPoint markerPoint = new GeoPoint( (int)(currentControlInfo.getLatitude() * 1e6), 
													(int)(currentControlInfo.getLongitude() * 1e6));
					OverlayItem overlayitem = new OverlayItem(markerPoint, currentControlInfo.getControlNumber(), currentControlInfo.getControlNumber());
	
					theMarkerOverlay.addOverlay(overlayitem);
				}
			}
			
			theMarkerOverlay.populateItemizedOverlay();
		} // end populateItemizedOverlays()
	

	/**
	 * Creates a list of lists of Overlays which holds the file name for the map tiles to overlay at each
	 * zoom level. The list for each level is created by iterating through the folder and file structure
	 * on the SD-card.
	 * 
	 * The files are ordered by folder name which represents the x-index for the tile (e.g. 1100, 1101)
	 * and the filename is the y-index (e.g 1406, 1407)
	 */
	private void populateTheMapTilesOverlayList() {
		
		theMapTileOverlayList = new ArrayList<ArrayList<Overlay>>();
		
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			
			// Retrieve the path to the SD-Card.
		    File externalStorageDir = Environment.getExternalStorageDirectory();
			
			// TODO Get all the zoom levels from the folder structure instead. More dynamic way.
		    // For each level 11-17 read in the map tiles in a separate ArrayList
			for (int index = MIN_ZOOM_LEVEL; index < MAX_ZOOM_LEVEL; index++) {				
				
				ArrayList<Overlay> currentLevelArray = new ArrayList<Overlay>();
				
			    // Get the base directory for the map tiles at the current zoom level.
			    File mapTileFolders = new File(externalStorageDir, "/data/friska_karlstad/Tiles/" + index);
			    
			    String[] folderNames = mapTileFolders.list();
			    
			    if (folderNames != null) {
			    	java.util.Arrays.sort(folderNames);
				    
			    	// Loop through the folders in the current zoom level folder (e.g. /mnt/sdcard/data/friska_karlstad/12/)
				    for ( int folderIndex = 0; folderIndex < folderNames.length; folderIndex++) {
				    	
				    	// Get the x-index for the tile from the folder name
						int xTileIndex = Integer.valueOf(folderNames[folderIndex]);
		
						// Get the files from this folder
			    		File currentFolder = new File(mapTileFolders, folderNames[folderIndex]);
					    String[] folderContent = currentFolder.list();
					    java.util.Arrays.sort(folderContent);
					    
					    for (int fileIndex = folderContent.length -1; fileIndex >= 0; fileIndex--) {

					    	// Get the y-index for the tile from the file name.
					    	int yTileIndex = Integer.valueOf(folderContent[fileIndex].substring(0, (folderContent[fileIndex].length() - 4)) );
					    			
					    	String fullPathAndFileName = currentFolder + "/" + folderContent[fileIndex];
					    	currentLevelArray.add(new MapItemsOverlay(this, fullPathAndFileName, xTileIndex, yTileIndex, index));
					    	yTileIndex++;
					    }
					    xTileIndex++;
				    }
			    }
				// Add a new ArrayList of Overlays for this level.
				theMapTileOverlayList.add(currentLevelArray);
			}
		}
	    stillLoadingUp = false;
	} // end populateTheMapTilesOverlayList()

	
//	/**
//	 * Populates the Overlay containing all the map tiles. This is called once att start up and
//	 * again each time the zoom-level changes.
//	 * 
//	 * This method assumes the map tiles are stored on the external medium in a structure that
//	 * is made up of /zoomlevel/tile_x_column/tile_yindex.png
//	 * 
//	 * @return a boolean indicating whether it was successful to populate the map tiles or not.
//	 */
//	private List<Overlay> populateMapTilesOverlays() {
//		List<Overlay> theMapTileOverlays = new ArrayList<Overlay>();
//		
//		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
//	    	
//			int xTileIndex = xTopTileIndex[currentZoomLevel];
//			
//			// Retrieve the path to the SD-Card.
//		    File externalStorageDir = Environment.getExternalStorageDirectory();
//
//		    // Get the base directory for the map tiles at the current zoom level.
//		    File mapTileFolders = new File(externalStorageDir, "/data/friska_karlstad/" + currentZoomLevel);
//		    
//		    String[] folderNames = mapTileFolders.list();
//		    
//		    if (folderNames != null) {
//		    	java.util.Arrays.sort(folderNames);
//			    
//		    	// Loop through the folders in the current zoom level folder (e.g. /mnt/sdcard/data/friska_karlstad/12/)
//			    for ( int folderIndex = 0; folderIndex < folderNames.length; folderIndex++) {
//			    	
//			    	// Reset the y-tilindex for each new x-tile index "column".
//			    	int yTileIndex = yTopTileIndex[currentZoomLevel];
//	
//		    		File currentFolder = new File(mapTileFolders, folderNames[folderIndex]);
//				    String[] folderContent = currentFolder.list();
//				    java.util.Arrays.sort(folderContent);
//				    
//				    // Loop through each file in the current folder and add it to the overlays.
//				    for (int fileIndex = folderContent.length -1; fileIndex >= 0; fileIndex--) {
//				    	String fullPathAndFileName = currentFolder + "/" + folderContent[fileIndex];
//			    		theMapTileOverlays.add(new MapItemsOverlay(this, fullPathAndFileName, xTileIndex, yTileIndex, currentZoomLevel));
//				    	yTileIndex++;
//				    }
//				    xTileIndex++;
//			    }
//		    }
//		    
//			// Re-add the itemized overlay containing the map markers.
//			// If user has selected to show own position. Add this.
//			if (showOwnPositionOnMap == true || showCompassOnMap == true) {
//				theMapTileOverlays.add(myLocationOverlay);
//			}
//			
//			if (currentZoomLevel > 13) {
//		    	theMapTileOverlays.add(theMarkerOverlay);
//		    }
//			
//		    // Add the measure stick overlay if that option is selected.
//		    if (showMeasureScaleOnMap == true) {
//		    	theMapTileOverlays.add(measureStick);
//		    }
//		}
//
//		return theMapTileOverlays;
//	} // end populateOverlays()
	
	
    /**
     * Sub classed AsyncTask that populates the overlay (both map tiles and control markers
     */
    private class PopulateInitialOverlays extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected Boolean doInBackground(Void... params) {
    		populateMarkerOverlays();
    		populateTheMapTilesOverlayList();
    		return true;
    	} // end doInBackground()

        protected void onProgressUpdate(Void v) {}

        @Override
		protected void onPostExecute(Boolean result) {
		    overlays.clear();
 		    // TODO Fix crash when no map available.
		    if (theMapTileOverlayList.size() > 0) {
		    	overlays.addAll(theMapTileOverlayList.get(currentZoomLevel -11));
		    }
	    	updatePrefrenceMapping();
		    stillLoadingUp = false;
        } // end onPostExecute()
        
    } // end class PopulateInitialOverlays
    
    
	/**
	 * Switches showing measure stick on and off. The measure stick overlay show length in 
	 * m/km to the user.
	 */
	private void toggleMeasureStick() {
		if (showMeasureScaleOnMap == true) {
			measureStick = new MeasureStickOverlay(this, 25, 50, currentZoomLevel);
			overlays.add(measureStick);
		}
		else {
			if (measureStick != null) {
				overlays.remove(measureStick);
				measureStick = null;
			}
		}
	} // end createMeasureStick()
	
	
	/**
	 * Switches showing user location on and off.
	 */
	private void toggleMyPosition() {
		if (showOwnPositionOnMap == true) {
			
			if (myLocationOverlay == null) {
				myLocationOverlay = new CustomMyLocationOverlay(this, theMapView);
			}
			myLocationOverlay.enableMyLocation();
			int location = overlays.size() > 1 ? overlays.size() - 2 : 0;
			overlays.add(location, myLocationOverlay);
		}
		else {
			if (myLocationOverlay != null) {
				myLocationOverlay.disableMyLocation();
			}
		}
	} // end toggleMyPosition()
	
	
	/**
	 * Toggles the showing of compass in the mapview on and off.
	 */
	private void toggleCompass() {
		if (showCompassOnMap == true) {
			
			if (myLocationOverlay == null) {
				myLocationOverlay = new MyLocationOverlay(this, theMapView);
			}
			myLocationOverlay.enableCompass();
			overlays.add(myLocationOverlay);
		}
		else {
			if (myLocationOverlay != null) {
				myLocationOverlay.disableCompass();
			}
		}
	} // end toggleCompass()
	
	
	/**
	 * Toggles showing already stored controls or turn them off.
	 */
	private void toggleShowRegisteredControls() {
		overlays.remove(theMarkerOverlay);
		populateMarkerOverlays();
		overlays.add(theMarkerOverlay);
	} // end toggleShowStoredControls()
    
	
	/**
	* Code to show dialog windows. The different dialogs are created using an int constant
	* to identify) which one to open. This function is called through showDialog(int) call
	* in code.
	*/ 
    @Override
    protected Dialog onCreateDialog(int id) {
        
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);

    	switch (id) {
			case GPS_DISABLED_ALERT:
		    	builder.setMessage("GPS är inte påslaget. För mer precis positionsbestämning, slå på den.")  
		    	.setCancelable(false)  
		    	.setPositiveButton("Slå på GPS", new DialogInterface.OnClickListener(){  
		    		public void onClick(DialogInterface dialog, int id){  
		    			showGpsOptions();
		    		}  
		    	});  
		    	builder.setNegativeButton("Nej tack",  
		    	new DialogInterface.OnClickListener(){  
		    		public void onClick(DialogInterface dialog, int id){
		    			removeDialog(GPS_DISABLED_ALERT);
		    		}  
		    	});  
		    	break;
			case MEM_CARD_READ_ONLY_ALERT:
		    	builder.setMessage("Inga skrivrättigheter på minneskort.")
		    	.setCancelable(false)  
		    	.setPositiveButton("Ok", new DialogInterface.OnClickListener(){  
		    		public void onClick(DialogInterface dialog, int id){
		    			removeDialog(MEM_CARD_READ_ONLY_ALERT);
		    		}  
		    	});  
		    	break;
			case MEM_CARD_NOT_MOUNTED_ALERT:
		    	builder.setMessage("Inget minneskort tillgängligt")
		    	.setCancelable(false)  
		    	.setPositiveButton("Ok", new DialogInterface.OnClickListener(){  
		    		public void onClick(DialogInterface dialog, int id){
		    			removeDialog(MEM_CARD_NOT_MOUNTED_ALERT);
		    		}  
		    	});  
		    	break;
			case MAP_TILES_ALERT:
		    	File mapTilesBaseDiractory = new File(Environment.getExternalStorageDirectory() + RestClient.DATA_STORAGE_BASE);
			    
		    	String okText = null;
		    	if (mapTilesBaseDiractory.exists()) {
		    		okText = "Kartan verkar redan vara nedladdad? Fortsätta ändå?";
		    	}
		    	else {
		    		okText = "Vill du ladda ner kartan (ca 15Mb)?";
		    	}
		    	
		    	builder.setMessage(okText)  
		    	.setCancelable(false)  
		    	.setPositiveButton("Ja", new DialogInterface.OnClickListener(){  
		    		public void onClick(DialogInterface dialog, int id){
		    	    	new DownloadMapTilesAsyncTask().execute();
		    		}  
		    	});  
		    	builder.setNegativeButton("Nej",  
		    	new DialogInterface.OnClickListener(){  
		    		public void onClick(DialogInterface dialog, int id){
		    			removeDialog(MAP_TILES_ALERT);
		    		}  
		    	});  
		    	break;
			case MAP_TILES_DOWNLOAD_FAILED:
		    	builder.setMessage("Misslyckades ladda ner kartan")
		    	.setCancelable(false)  
		    	.setPositiveButton("Ok", new DialogInterface.OnClickListener(){  
		    		public void onClick(DialogInterface dialog, int id){
		    			removeDialog(MAP_TILES_DOWNLOAD_FAILED);
		    		}  
		    	});
		    	break;
			case MAP_TILES_SUCCESS:
		    	builder.setMessage("Nedladdning av kartbitar klar.")
		    	.setCancelable(false)  
		    	.setPositiveButton("Ok", new DialogInterface.OnClickListener(){  
		    		public void onClick(DialogInterface dialog, int id){
		    			removeDialog(MAP_TILES_SUCCESS);
		    		}  
		    	});
		    	break;
		    default:
		    	break;
    	}
	    return builder.create();
    } // end onCreateDialog()
	
  
    /**
     * Opens the GPS-preference view.
     */
    private void showGpsOptions() {  
    	startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), CHANGE_GPS_SETTING_REQ);
    } // end showGpsOptions()
    
    
    /**
     * Class used to contact the server asynchronously in a separate thread from the UI-thread 
     * to download an existing account for a Stamford user.
     */
    private class DownloadMapTilesAsyncTask extends AsyncTask<Void, Integer, String> {
    	
    	protected ProgressDialog progressDialog;
    	
    	@Override
		protected void onPreExecute() {
    		lockScreenRotation();
    		progressDialog = new ProgressDialog(MapNavigatorView.this);
    		progressDialog.setTitle("Laddar ner karta");
    		progressDialog.setMessage("Laddar ner kartbitar från servern (ca 15Mb)");
    		progressDialog.show();
    	} // end onPreExecute()
        
        @Override
		protected String doInBackground(Void... params) {
        	
        	String result = "failed";
        	String sdCardState = FileHandler.checkExternalStorageAvailability();
        	
        	if (sdCardState.equals(Environment.MEDIA_UNMOUNTED) || sdCardState.equals(Environment.MEDIA_MOUNTED_READ_ONLY)) {
        		result = sdCardState;
        	}
        	else {
        		
				HttpEntity theZipFile = null;
				// Show the registered user name if the user has been registered.
				if (userName != null) {
					theZipFile = restClient.downloadMapTiles(registeredUser.getMapObject().getMapLink());
				}
				else {
					theZipFile = restClient.downloadMapTiles(unregisteredUser.getMapObject().getMapLink());
				}
	            
	        	if (theZipFile != null) {
		        	// Create the base directory where we want to write the map tiles to.
				    File mapTilesBaseDiractory = new File(Environment.getExternalStorageDirectory() + RestClient.DATA_STORAGE_BASE);
				    mapTilesBaseDiractory.mkdirs();
				    
				    try {
				    	int BUF_SIZE = 1024;
				    	ZipEntry entry = null;
				    	ZipInputStream zin = new ZipInputStream(theZipFile.getContent());
				    	BufferedInputStream bufIn = new BufferedInputStream(zin, BUF_SIZE);
				    	BufferedOutputStream outBuf = null;
				    	int fileCounter = 0;
				    	
				    	// Read through all the entries in the zip file and unzip to their respective directory
				    	while ((entry = zin.getNextEntry()) != null) {
				    		
						    publishProgress(++fileCounter);
					    	
						    File newFile = new File (mapTilesBaseDiractory.getPath() + "/" + entry.getName() + "/");
					    	
					    	//If it is a directory, create it else if it is a file, write that file.
					    	if (!newFile.getName().endsWith("png")) {
					    		newFile.mkdir();
					    	}
					    	else {
					    		outBuf = new BufferedOutputStream(new FileOutputStream(newFile), BUF_SIZE);
						    	int count;
					            byte data[] = new byte[BUF_SIZE];
					            
					            // write the files to the disk
					            while ((count = bufIn.read(data, 0, BUF_SIZE)) != -1) {
					            	outBuf.write(data, 0, count);
					            }
					            outBuf.flush();
					            outBuf.close();
					    	}
				    	}
					    zin.close();
					    
			    		// Save the number of map tiles downloaded to SD-card. This will be used as an indicator for that all
					    // necessary map tiles have been downloaded.
			    		SharedPreferences.Editor editor = settings.edit();
					    editor.putInt("nr_of_map_tiles", fileCounter);
					    editor.commit();
					    
					    if (fileCounter != 0) {
						    result = "ok";
					    }
				    }
					catch (IOException e1) {
						e1.printStackTrace();
					}
	        	}
	        }
			return result;
        } // end doInBackground()
		
        protected void onProgressUpdate(Integer... fileCount) {
        	progressDialog.setTitle("Packar upp filer och sparar");
        	progressDialog.setMessage("Sparar: " + fileCount[0] + " av 1141 kartbitar.");
        }

        protected void onPostExecute(String result) {
        	if (progressDialog.isShowing()) {
            	progressDialog.cancel();
        	}
        	if (result.equals(Environment.MEDIA_UNMOUNTED)) {
        		showDialog(MEM_CARD_NOT_MOUNTED_ALERT);
        	}
        	else if (result.equals(Environment.MEDIA_MOUNTED_READ_ONLY)) {
        		showDialog(MEM_CARD_READ_ONLY_ALERT);
        	}
        	else if (result.equals("ok")) {
        		showDialog(MAP_TILES_SUCCESS);
        	    // Populate the map overlays asynchronously when downloading is finished.
        		new PopulateInitialOverlays().execute();
        	}
        	else {
        		showDialog(MAP_TILES_DOWNLOAD_FAILED);
        	}
        	unlockScreenRotation();
        } // end onPostExecute()
        
    } // end class DownloadControlListAsyncTask

    

    /**
     * Locks any screen orientation changes
     */
    private void lockScreenRotation() {
      // Stop the screen orientation changing during an event
        switch (this.getResources().getConfiguration().orientation) {
	      case Configuration.ORIENTATION_PORTRAIT:
	        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	        break;
	      case Configuration.ORIENTATION_LANDSCAPE:
	        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	        break;
        }
    } // end lockScreenRotation()
    
    /**
     * Unlocks the screen orientation change ability
     */
    private void unlockScreenRotation() {
    	this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    } // end unlockScreenRotation()
    
    
	@Override
	protected boolean isRouteDisplayed() {
		return false;
	} // end isRouteDisplayed()
	
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.map_options_menu, menu);
        return true;
    } // end onCreateOptionsMenu()
    
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        	case R.id.download_map_tiles_menu_option:
        		showDialog(MAP_TILES_ALERT);
            return true;
        	case R.id.options_settings:
        		startActivityForResult(new Intent(this, EditPreferences.class), CHANGE_SETTINGS_REQ);
            return true;
        	case R.id.map_options_help:
    			Intent browserIntent = new Intent(this, InformationView.class);
    			browserIntent.putExtra("browser_url", "file:///android_asset/map_menu_help.html");
    			startActivity(browserIntent);
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
        }
    } // end onOptionsItemSelected()

    
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		if (requestCode == CHANGE_SETTINGS_REQ) {
			updatePrefrenceMapping();
	    }
		if (requestCode == CHANGE_GPS_SETTING_REQ) {
		    // Check if the GPS was activated in the preferences.
		    if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
		    	startLocationListener(LocationManager.GPS_PROVIDER);
		    }
		}
	} // end onActivityResult()

	  
	@Override
	protected void onDestroy() {
		super.onDestroy();
		recycleBitMaps();
		System.gc();
		locationManager.removeUpdates(locationListener);
	} // end onDestroy()

	
	@Override
	protected void onPause() {
		super.onPause();
		recycleBitMaps();
		System.gc();
	} // end onDestroy()
    
} // end class MapNavigatorView