package jonas.com.friskakarlstad.utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.BitmapFactory.Options;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class MapItemsOverlay extends Overlay {

	public static int numberOfLoaded;
	public static int numberOfReady;
	
	// Holds the custom tile image
	Bitmap bmp;
	
	// The path to where the image is stored.
    private String imagePath;
    
    // The x-index in the google maps tile grid.
	private int xTileIndex;
	
	// The y-index in the google maps tile grid.
    private int yTileIndex;
    
    // The zoom level when this custom map tile is shown
    private int zoomLevel;
    
    // The latitude for the top-left corner of this tile.
    private double topLatitude;
    
    // The bottom right corner of this tile.
    private double bottomLatitude;
    
    // The longitude for the top-left corner of this tile.
    private double topLongitude;
    
    // The longitude of the bottom-right corner of this map tile.
    private double bottomLongitude;
    
    // The Location for the top-left corner of this map tile.
    private GeoPoint tileWorldPositionTop;

    // The Location for the bottom-right corner of this map tile.
    private GeoPoint tileWorldPositionBottom;

    // The options for the bmp file in the custom overlay.
    private BitmapFactory.Options aPtions;
	
    /**
	 * Constructor for class MapItemsOverlay.
	 * @param defaultMarker a Drawable that is the marker to point out where the user is
	 * @param context a Context which is the context in which this class is operating.
	 */
	public MapItemsOverlay(Context context, String theImagePath, int theXTileIndex, int theYTileIndex, int theZoomLevel) {
		imagePath = theImagePath;
		
		xTileIndex = theXTileIndex;
		yTileIndex = convertTMStoGoogleMapTileYIndex(theYTileIndex, theZoomLevel);
		
		zoomLevel = theZoomLevel;
		
		bmp = null;
		
		aPtions = new Options();
		// Important! Delays painting this tile if there is memory shortage. 
		// Removing this will cause the application to crash at high loads.
		aPtions.inPurgeable = true;
		aPtions.inDither=true;  
		aPtions.inInputShareable=true;
		
		// Get this tile's latitude top position from the tile x-index.
	    double n = Math.PI - (2.0 * Math.PI * yTileIndex) / Math.pow(2.0, zoomLevel);
	    topLatitude = Math.toDegrees(Math.atan(Math.sinh(n)));
	    
	    // Get this tile's latitude bottom position from the tile x-index.
	    n = Math.PI - 2.0 * (Math.PI * (yTileIndex + 1)) / Math.pow(2.0, zoomLevel);
	    bottomLatitude = Math.toDegrees(Math.atan(Math.sinh(n)));
	    
	    // Get this tile's top longitude position from the tile y-index
	    topLongitude = xTileIndex / Math.pow(2.0, zoomLevel) * 360.0 - 180.0;
	    // Get this tile's bottom longitude position from the tile y-index
	    bottomLongitude = (xTileIndex + 1) / Math.pow(2.0, zoomLevel) * 360.0 - 180.0;
	    
	    // Create a geopoint to feed to the projection function inside the draw method.
		tileWorldPositionTop = new GeoPoint((int)(topLatitude * 1000000), (int)(topLongitude * 1000000) );
		tileWorldPositionBottom = new GeoPoint((int)(bottomLatitude * 1000000), (int)(bottomLongitude * 1000000) );
		
	} // end MapItemsOverlay()

	
	/**
	 * TMS has another scheme for naming the y-tileindex than Google Maps. This function converts 
	 * the index from TMS to Google scheme.
	 * 
	 * @param tmsYIndex an int that is the tile index in the y-axis in TMS
	 * @param theZoomLevel an int that is the current zoomlevel where this map tile belong to.
	 * @return an int that is the tile number in Google format
	 */
	public int convertTMStoGoogleMapTileYIndex (int tmsYIndex, int theZoomLevel) {
		//Converts TMS tile coordinates to Google Tile coordinates"
		// coordinate origin is moved from bottom-left to top-left corner of the extent
		Double convertedYTileIndex = (Math.pow(2.0, theZoomLevel) -1) - tmsYIndex;
		return convertedYTileIndex.intValue();
	} // end convertTMStoGoogleMapTileYIndex()
	
	
	public String getPath() {
		return imagePath;
	}

	
	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {
		// Update the point where this tile's, NW corner is located in pixels offset from the current screen projection.
		Point aPoint = mapView.getProjection().toPixels(tileWorldPositionTop, null);
		Point aPoint2 = mapView.getProjection().toPixels(tileWorldPositionBottom, null);

		int xMinLevel;
		int yMinLevel;
		
		if (mapView.getZoomLevel() < 17) {
			xMinLevel = zoomLevel < 15 ? -128: -256;
			yMinLevel = zoomLevel < 15 ? -128: -256;
		}
		else {
			xMinLevel = -1024;
			yMinLevel = -1024;
		}
		
		if (aPoint.x > xMinLevel &&  aPoint.x < mapView.getWidth() && aPoint.y < mapView.getHeight() && aPoint.y > yMinLevel) {
	
			if (bmp == null && numberOfLoaded < 70) {
				bmp = BitmapFactory.decodeFile(imagePath, aPtions);
				numberOfLoaded++;
			}

			if (numberOfLoaded < 70) {
				Rect dst = new Rect( aPoint.x, aPoint.y,aPoint2.x,aPoint2.y );
				canvas.drawBitmap(bmp, null, dst, null);
				super.draw(canvas, mapView, false);
			}
		}
		else {

			if (bmp != null) {
				numberOfLoaded--;
				bmp.recycle();
				bmp = null;
			}
		}
		
	} // end draw()

	
	/**
	 * Recycles the Bitmap in this overlay if it is not needed anymore.
	 */
	public void recycleBitmap() {
		if (bmp != null) {
			bmp.recycle();
			bmp = null;
			numberOfLoaded--;
		}
	} // end recycleBitmap()
	
} // end class MapItemsOverlay
