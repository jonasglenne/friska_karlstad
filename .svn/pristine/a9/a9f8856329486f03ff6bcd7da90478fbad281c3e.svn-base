package jonas.com.friskakarlstad.activities;

import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import jonas.com.friskakarlstad.R;
import jonas.com.friskakarlstad.pojos.NewsFlash;
import jonas.com.friskakarlstad.pojos.RegisteredMobileUser;
import jonas.com.friskakarlstad.pojos.UnregisteredMobileUser;
import jonas.com.friskakarlstad.utilities.FileHandler;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class NewsFlashView extends ListActivity {
	
	/**************************************************************
	 * Result codes for different methods called within the Activity.
	 **************************************************************/
	
	// Keep this result if no contact could be made with the server.
	static final int RESULT_UNDEFINED = -1;
	
	// Id for dialog informing user that it is not registered.
	static final int USER_NOT_REGISTERED = 1;
	
	// Id to handle problem if control has been already registered before this registration attempt.
	static final int CONTROL_ALREADY_REGISTERED = 2;
	
	// Id to handle problem if control is not valid in some way.
	static final int INVALID_CONTROL = 3;
	
	// Id for dialog to inform user the unlock was successful.
	static final int RESULT_WAS_OK = 4;
	
	// An id for an Alert dialog shown when it was not possible to store the control to the server.
	static final int STORAGE_NOT_POSSIBLE = 5;
	
	// Id to handle the event that user selected to try to upload unregistered controls to server but
	// there were no unregistered controls to send.
	static final int NOTHING_TO_UPDATE = 6;
	
	/**************************************************************
	 * Widgets for this activity.
	 **************************************************************/
	
	// Heading for this view.
	private TextView tvNewsListViewHeading;

	/**************************************************************
	 * Utility attributes for this activity.
	 **************************************************************/
	
	// A list with the newsflashes that is used for the list adapter.
	private ArrayList<NewsFlash> lvNewsList;
	
	// A custom adapter for the listview that accepts a list of Control objects.
	private NewsFlashListAdapter theNewsListAdapter;
	
	// Progress dialog to show during lengthy operations.
	private ProgressDialog requestProgressDialog;

	// Shared preferences for this application.
	private SharedPreferences sharedPreferences;
	
	// Loaded from the preference attribute "user_name". If this is null after onCreate 
	// it means the user is not registered.
	private String userName;
	
	// An object containing the accound (user info, controls taken and controlinfos) for 
	// a registered user.
	private RegisteredMobileUser registeredUser;
	
	// An object containing project information for an unregistered user.
	private UnregisteredMobileUser unregisteredUser;
	
	private ArrayList<NewsFlash> newsList;
	
	@SuppressWarnings("unchecked")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_list_listview_layout);
		        
		// Check if there is a retained stateful object from a change in screen orientation. If not, load the list
		// from file.
		final Object retainedNewsList = getLastNonConfigurationInstance();
		
		if (retainedNewsList == null) {
	    	
			// Read the control list from local file.
			try {
				sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
				userName = sharedPreferences.getString("username", null);
				
				if (userName != null) {
					registeredUser = (RegisteredMobileUser)FileHandler.getObjectFromFile(openFileInput(FileHandler.REGISTERED_USER_FILENAME));
					newsList = registeredUser.getNewsFlashes();
				}
				else {
					unregisteredUser = (UnregisteredMobileUser)FileHandler.getObjectFromFile(openFileInput(FileHandler.UNREGISTERED_USER_FILENAME));
					newsList = unregisteredUser.getNewsFlashes();
				}
			}
			catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
	    }
	    else {
	    	newsList = (ArrayList<NewsFlash>) retainedNewsList;
	    }
		
		// Find out if the user wants to hide already registered controls or not.
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        
        tvNewsListViewHeading = (TextView) findViewById(R.id.tvNewsListViewHeading);
        
        lvNewsList = new ArrayList<NewsFlash>();
		theNewsListAdapter = new NewsFlashListAdapter(this, R.layout.news_listview_listrow_layout, lvNewsList);
        initialLoadNewsListAdapter(newsList);
        setListAdapter(theNewsListAdapter);
    } // end onCreate()
	

	/**
	 * Populates the listview adapter with elements from a list of controls on create of this view. 
	 * If the user has selected to hide already stored controls, these are not included in the list.
	 * 
	 * @param aNewsList a List<Control> object is used as parameter
	 */
	private void initialLoadNewsListAdapter(List<NewsFlash> aNewsList) {
		
		theNewsListAdapter.clear();
		
		if (aNewsList != null) {
		
			Collections.sort(newsList);
			Collections.reverse(newsList);
			
			for (NewsFlash currentNewsFlash: aNewsList) {
				theNewsListAdapter.add(currentNewsFlash);
			}
		}
		
		theNewsListAdapter.notifyDataSetChanged();
	} // end initialLoadListControlAdapter()

	
	/**
	 * Custom list Adapter for the listview that accepts Control objects and display their
	 * data in the list.
	 */
	private class NewsFlashListAdapter extends ArrayAdapter<NewsFlash> {

        private ArrayList<NewsFlash> items;
        private SimpleDateFormat displaydateFormat = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm");
        
        public NewsFlashListAdapter(Context context, int textViewResourceId, ArrayList<NewsFlash> items) {
            super(context, textViewResourceId, items);
            this.items = items;
        } // end constructor for ControlAdapter
        
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.news_listview_listrow_layout, null);
            }
            
            NewsFlash currentNewsFlash = items.get(position);
            
            if (currentNewsFlash != null) {
            	
            	TextView tvNewsListRowTitle = (TextView) v.findViewById(R.id.tvNewsListRowTitle);
                TextView tvNewsListRowDate = (TextView) v.findViewById(R.id.tvNewsListRowDate);
                TextView tvNewsListRowBody = (TextView) v.findViewById(R.id.tvNewsListRowBody);
                
                if (tvNewsListRowTitle != null) {
                	tvNewsListRowTitle.setText(currentNewsFlash.getHeader());
        		}
                if (tvNewsListRowDate != null) {
                	
                	tvNewsListRowDate.setText(displaydateFormat.format(currentNewsFlash.getTimeStamp()));
        		}
                if (tvNewsListRowBody != null) {
                	tvNewsListRowBody.setText(currentNewsFlash.getDescription());
        		}

            }
            return v;
        }
	} // end class NewsFlashListAdapter
    
    
	// Android function to retain a stateful object if the activity is destroyed. This
	// stateful object can be retrieved if the activity restarts.
	@Override
	public Object onRetainNonConfigurationInstance() {
	    final List<NewsFlash> retainedNewsList = newsList;
	    return retainedNewsList;
	} // end onRetainNonConfigurationInstance()
    
} // end class NewsFlashView
