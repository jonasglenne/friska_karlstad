package jonas.com.friskakarlstad;

import java.io.Serializable;

public class Control implements Serializable, Comparable<Control>{

	// The id for this control.
	private String controlId;
	
	// The code writen on the control post.
	private String codeName;
	
	// The description of the location for this post.
	private String description;

	// The latitude position of this control
	private double latitude;
	
	// The longitude position of this control
	private double longitude;
	
	// Indicates whether this control has been scanned by user or not.
	private boolean isScanned;
	
	// Indicates whether this control has been successfully stored on server
	// as scanned by user.
	private boolean isStoredOnServer;
	
	// A string that represents the time point the control was scanned.
	private String timeStamp;
	
	// A string that is an string representing an url to an advertisers homepage.
	private String imageLink;

	// A string that is a link to an advertisers home page.
	private String webLink;
	
	// A string that represents the difficulty level for the control (blue, green, red and black).
	private String controlColor;

	// Constructor for class Control.
	public Control() {
	} // end constructor Control()

	// Overload constructor for class Control.
	public Control(String controlId, String codeName, String timeStamp) {
		this.controlId = controlId;
		this.codeName = codeName;
		this.timeStamp = timeStamp;
	} // end constructor Control()
	
	
	public String getImageLink() {
		return imageLink;
	}

	public void setImageLink(String imageLink) {
		this.imageLink = imageLink;
	}

	public String getWebLink() {
		return webLink;
	}

	public void setWebLink(String webLink) {
		this.webLink = webLink;
	}

	public String getControlColor() {
		return controlColor;
	}

	public void setControlColor(String controlColor) {
		this.controlColor = controlColor;
	}

	public String getControlId() {
		return controlId;
	}

	public void setControlId(String controlId) {
		this.controlId = controlId;
	}

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public boolean isScanned() {
		return isScanned;
	}

	public void setScanned(boolean isScanned) {
		this.isScanned = isScanned;
	}

	public boolean isRegisteredOnServer() {
		return isStoredOnServer;
	}

	public void setStoredOnServer(boolean isStoredOnServer) {
		this.isStoredOnServer = isStoredOnServer;
	}

	
	@Override
	public String toString() {
		return "Control [codeName=" + codeName + ", controlColor="
				+ controlColor + ", controlId=" + controlId + ", description="
				+ description + ", imageLink=" + imageLink + ", isScanned="
				+ isScanned + ", isStoredOnServer=" + isStoredOnServer
				+ ", latitude=" + latitude + ", longitude=" + longitude
				+ ", timeStamp=" + timeStamp + ", webLink=" + webLink + "]";
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	@Override
	public int compareTo(Control another) {
		
        if (Integer.valueOf(this.controlId) == Integer.valueOf(((Control) another).controlId)) {
            return 0;
        }
        else if (Integer.valueOf(this.controlId) > Integer.valueOf(((Control) another).controlId)) {
            return 1;
        }
        else {
            return -1;
        }
	}
	
} // end class Control
