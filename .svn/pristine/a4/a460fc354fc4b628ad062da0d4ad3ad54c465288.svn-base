package jonas.com.friskakarlstad.utilities;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import jonas.com.friskakarlstad.pojos.Control;

import org.apache.http.Header;
import org.apache.http.HeaderIterator;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeaderIterator;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;


import android.R;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.Log;

public class RestClient {
	
	public static final int UNDEFINED_RESPONSE = -1;
	public static final int RESULT_WAS_OK = 0;
	public static final int USER_ALREADY_EXIST = 1;
	public static final int CONTROL_ALREADY_REGISTERED = 2;
	public static final int INVALID_CONTROL = 3;
	public static final int UNLOCK_CODE_ALREADY_USED = 4;
	public static final int INVALID_UNLOCK_CODE = 5;
	
	// The classname used as a tag for Log printouts.
	public static final String CLASSNAME = "RestClient";

	// http://privat.bahnhof.se/wb739361/GAETiles.zip
	// The location for the map tiles.
	final static String MAP_TILES_HTTP_LOCATION = "https://docs.google.com/uc?id=0BytBBI0ugXU-MDE2ODAzZDktMmM2OS00MjliLTllNGYtYmE2YWRiNmIwZTVm&export=download&authkey=CLXt3MkE&hl=sv";

	// The base directory for the map tiles.
	public final static String DATA_STORAGE_BASE = "/data/friska_karlstad/";

	public final static String PROJECT_NAME = "friska_karlstad_test";
	
	// The GAE host.
	private HttpHost httpHost;

	// The http client for this REST client class.
	DefaultHttpClient httpClient;	
	
	/**
	 * Constructor for class RestClient
	 */
	public RestClient() {
		httpHost = new HttpHost("checkpointserver.appspot.com", 443, "https");
	} // end constructor RestClient()

	
	/**
	 * Executes a POST request and returns the respone
	 * 
	 * @param postURI a String that is the URI for the request
	 * @param xmlBody a String that is the XML body for the request
	 * @return a HttpResponse containing the results from the request
	 */
	private HttpResponse executePostRequest(String postURI, String xmlBody) {

		printMessageToSend(xmlBody.toString());

		httpClient = new DefaultHttpClient();

		HttpPost httpPost = new HttpPost(postURI);
    	httpPost.addHeader("Accept", "text/html");
    	httpPost.addHeader("Content-Type", "application/xml");
    	HttpResponse response = null;
    	
    	try {
    		StringEntity entity = new StringEntity(xmlBody, "UTF-8");
	    	entity.setContentType("application/xml");
	    	httpPost.setEntity(entity);
	    	
	    	response = httpClient.execute(httpHost, httpPost);
    	}
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}

    	return response;
	} // end executePostRequest()
	
	
	/**
	 * Registers a single control to the server.
	 * 
	 * @param controlToSave the Control that is to be saved.
	 * @return a String indicating success (true) or failure (false) in registering the control
	 */
	public int registerControl(Control controlToSave, String username, String password) {

		int result = UNDEFINED_RESPONSE;
		StringBuilder xmlBody = new StringBuilder();
		StringBuilder responseBuilder = new StringBuilder();

		xmlBody.append("<control>");
		xmlBody.append("<controlNumber>" + controlToSave.getControlNumber() + "</controlNumber>");
		xmlBody.append("<clientType>" + controlToSave.getClientType() + "</clientType>");
		xmlBody.append("<timeStamp>" + controlToSave.getTimeStamp() + "</timeStamp>");
		xmlBody.append("<userName>" + username +  "</userName>");
		xmlBody.append("<password>" + password + "</password>");
		xmlBody.append("<sessionId></sessionId>");
		xmlBody.append("<projectName>" + PROJECT_NAME + "</projectName>");
		xmlBody.append("</control>");
		
    	try {
	    	HttpResponse response = executePostRequest("/addcontrol", xmlBody.toString());
	    	BufferedReader bufferedReader = 
	    			new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	    	
	    	String line;
	    	while ( (line = bufferedReader.readLine()) != null) {
	    		responseBuilder.append(line);
	    	}
	    	
	    	// Convert the xml string to a document and extract the return message.
	    	String xmlResponse = GeneralXMLFunctions.extractXmlMessage(responseBuilder.toString());
	    	
	    	if (xmlResponse.equals("Control already registered")) {
	    		result = CONTROL_ALREADY_REGISTERED;
	    	}
	    	else if (xmlResponse.equals("Invalid control")) {
	    		result = INVALID_CONTROL;
	    	}
	    	else if (xmlResponse.equals("success")) {
	    		result = RESULT_WAS_OK;
	    	}
	    	else {
	    		Log.i(CLASSNAME, "Unknown response code from server or could not even ");
	    	}
    	}
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	httpClient.getConnectionManager().shutdown();
    	
    	return result;
	} // end registerControl
	
	
	public int unlockApplication(String code, String userName, String password) {
		Log.i(CLASSNAME, "Sending unlock code to the server");
		
		int result = UNDEFINED_RESPONSE;
		StringBuilder responseBuilder = new StringBuilder();
		StringBuilder xmlBody = new StringBuilder();
		
		xmlBody.append("<passcode>");
		xmlBody.append("<code>" + code + "</code>");
		xmlBody.append("<userName>" + userName + "</userName>");
		xmlBody.append("<password>" + password + "</password>");
		xmlBody.append("<sessionId></sessionId>");
		xmlBody.append("<projectName>" + PROJECT_NAME + "</projectName>");
		xmlBody.append("</passcode>");
		
		HttpResponse response = null;
		
    	try {
	    	response = executePostRequest("/uploadcode", xmlBody.toString());
	    	
	    	BufferedReader bufferedReader = 
	    			new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	    	String line;
	    	while ( (line = bufferedReader.readLine()) != null) {
	    		responseBuilder.append(line);
	    	}

	    	// Convert the xml string to a document and extract the return message.
	    	String xmlResponse = GeneralXMLFunctions.extractXmlMessage(responseBuilder.toString());
	    	
	    	if (xmlResponse.equals("Ogiltig kod")) {
	    		result = INVALID_UNLOCK_CODE;
	    	}
	    	else if (xmlResponse.equals("Koden redan registrerad")) {
	    		result = UNLOCK_CODE_ALREADY_USED;
	    	}
	    	else if (xmlResponse.equals("success")) {
	    		result = RESULT_WAS_OK;
	    	}
	    	else {
	    		Log.i(CLASSNAME, "Unknown response code from server or could not even...");
	    	}
    	}
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	httpClient.getConnectionManager().shutdown();
		
		return result;
	} // end unlockApplication()
	
	
	public String getTopOneHundredStatistics(String userName, String password) {
		Log.i(CLASSNAME, "Retrieving top 100 statistics for this project.");
		
		httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost("/gettop100");
    	httpPost.addHeader("Accept", "text/html");
    	httpPost.addHeader("Content-Type", "application/xml");
    	HttpResponse response = null;
    	String serverResponse = null;
    	StringBuilder xmlBody = new StringBuilder();
    	
		xmlBody.append("<credentials>");
		xmlBody.append("<userName>" + userName + "</userName>");
		xmlBody.append("<password>" + password + "</password>");
		xmlBody.append("<sessionId></sessionId>");
		xmlBody.append("<projectId></projectId>");
		xmlBody.append("<argument1>" + PROJECT_NAME + "</argument1>");
		xmlBody.append("<argument2></argument2>");
		xmlBody.append("</credentials>");
				
    	try {
    		StringEntity entity = new StringEntity(xmlBody.toString(), "UTF-8");
	    	entity.setContentType("application/xml");
	    	httpPost.setEntity(entity);
	    	
	    	response = httpClient.execute(httpHost, httpPost);
			//Log.i(CLASSNAME, "Statusline: " + response.getStatusLine());
			//Log.i(CLASSNAME, "Reasonphrase: " + response.getStatusLine().getReasonPhrase());
	    	serverResponse = EntityUtils.toString(response.getEntity());
	    }
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	
		return serverResponse;
	} // end unlockApplication()
	
	
	/**
	 * Registers multiple controls to the server with a control objects as argument.
	 * 
	 * @param theControlsToRegister an ArrayList of Controls that is the controls to register.
	 * @param registeredUser a RegisteredUser that is the user to register controls for.
	 * @return a boolean that indicates the result of the registration
	 */
	public int registerControls(ArrayList<Control> theControlsToRegister, String username, 
			                       String password) {
		Log.i(CLASSNAME, "Register controls to the server");

		int result = UNDEFINED_RESPONSE;
		StringBuilder responseBuilder = new StringBuilder();
		StringBuilder xmlBody = new StringBuilder();

		xmlBody.append("<controls>");
		for (Control currentControl : theControlsToRegister) {
			xmlBody.append("<control>");
			xmlBody.append("<controlNumber>" + currentControl.getControlNumber() + "</controlNumber>");
			xmlBody.append("<clientType>" + currentControl.getClientType() + "</clientType>");
			xmlBody.append("<timeStamp>" + currentControl.getTimeStamp() + "</timeStamp>");
			xmlBody.append("<projectName>" + PROJECT_NAME + "</projectName>");
			xmlBody.append("</control>");
		}
		xmlBody.append("<userName>" + username +  "</userName>");
		xmlBody.append("<password>" + password + "</password>");
		xmlBody.append("<sessionId></sessionId>");
		xmlBody.append("<projectName>" + PROJECT_NAME + "</projectName>");
		xmlBody.append("</controls>");
    	
		HttpResponse response = null;
		
    	try {
	    	response = executePostRequest("/addcontrols", xmlBody.toString());
	    	
	    	BufferedReader bufferedReader = 
	    			new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	    	String line;
	    	while ( (line = bufferedReader.readLine()) != null) {
	    		responseBuilder.append(line);
	    	}

	    	// Convert the xml string to a document and extract the return message.
	    	String xmlResponse = GeneralXMLFunctions.extractXmlMessage(responseBuilder.toString());
	    	
//	    	if (xmlResponse.equals("Control already registered")) {
//	    		result = CONTROL_ALREADY_REGISTERED;
//	    	}
	    	if (xmlResponse.equals("Invalid control")) {
	    		result = INVALID_CONTROL;
	    	}
	    	else if (xmlResponse.equals("success")) {
	    		result = RESULT_WAS_OK;
	    	}
	    	else {
	    		Log.i(CLASSNAME, "Unknown response code from server or could not even ");
	    	}
    	}
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	httpClient.getConnectionManager().shutdown();
    	
    	return result;
	} // end registerControls()	
	
	
	public int updateUser(String xmlMessage) {
		Log.i(CLASSNAME, "updateUser(): Updating user at the server.");
		
		int result = UNDEFINED_RESPONSE;
		StringBuilder responseBuilder = new StringBuilder();
		
		Log.i(CLASSNAME, xmlMessage);
		BufferedReader bufferedReader = null;
		
    	try {
	    	HttpResponse response = executePostRequest("/updateuser", xmlMessage);

	    	//String serverResponse = EntityUtils.toString(response.getEntity());
	    	bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	    	String line;
	    	while ( (line = bufferedReader.readLine()) != null) {
	    		responseBuilder.append(line);
	    	}
	    	// TODO: Check what the server response can be and update this part accordingly to indicate success or failure.
	    	//String xmln = "<error><code>1</code><message>User already exists</message></error>";
	    	String xmlResponse = GeneralXMLFunctions.extractXmlMessage(responseBuilder.toString());
	    	
    	 	if (xmlResponse.equals("success")) {
    	 		result = RESULT_WAS_OK;
    	 	}
    	 	else if (xmlResponse.equals("User does not exist")) {
	    		result = USER_ALREADY_EXIST;
	    	}
    	}
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	finally {
    		if (bufferedReader != null) {
	    	    try {
	    			bufferedReader.close();
	            } 
	    	    catch (IOException e) {
	                e.printStackTrace();
	            }
    		}
    	}
    	
    	httpClient.getConnectionManager().shutdown();

    	return result;
	} // end updateUser()
	
	
	/**
	 * Registers a user at the server.
	 * 
	 * @param xmlMessage a String that is XML-body for the entity to send to the server
	 * 
	 * @return an int that is the result of the registration of the user
	 */
	public int registerUser(String xmlMessage) {
		Log.i(CLASSNAME, "registerUser(): Registering user at the server.");
		
		int result = UNDEFINED_RESPONSE;
		StringBuilder responseBuilder = new StringBuilder();
		
		Log.i(CLASSNAME, xmlMessage);
		BufferedReader bufferedReader = null;
		
    	try {
	    	HttpResponse response = executePostRequest("/adduser", xmlMessage);

	    	//String serverResponse = EntityUtils.toString(response.getEntity());
	    	bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	    	String line;
	    	while ( (line = bufferedReader.readLine()) != null) {
	    		responseBuilder.append(line);
	    	}
	    	// TODO: Check what the server response can be and update this part accordingly to indicate success or failure.
	    	//String xmln = "<error><code>1</code><message>User already exists</message></error>";
	    	String xmlResponse = GeneralXMLFunctions.extractXmlMessage(responseBuilder.toString());
	    	
    	 	if (xmlResponse.equals("success")) {
    	 		result = RESULT_WAS_OK;
    	 	}
    	 	else if (xmlResponse.equals("User already exists")) {
	    		result = USER_ALREADY_EXIST;
	    	}
    	}
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	finally {
    		if (bufferedReader != null) {
	    	    try {
	    			bufferedReader.close();
	            } 
	    	    catch (IOException e) {
	                e.printStackTrace();
	            }
    		}
    	}
    	
    	httpClient.getConnectionManager().shutdown();

    	return result;
	} // end sendXMLToServer()
	
	
	/**
	 * Downloads the map tiles zip file from the web.
	 * 
	 * @param mapURL a String that is the address to the map file location.
	 * 
	 * @return a HttpEntity containing the stream to the zip file.
	 */
	public HttpEntity downloadMapTiles(String mapURL) {
		
		HttpResponse response = null;
	    HttpEntity zipfileEntity = null;

		httpClient = new DefaultHttpClient();

	    // Get the map tiles zip-file from the server.
	    HttpGet httpGet = new HttpGet(mapURL);
	    try {
			response = httpClient.execute(httpGet);
			zipfileEntity = response.getEntity();
		} 
	    catch (ClientProtocolException e) {
			e.printStackTrace();
		} 
	    catch (IOException e) {
			e.printStackTrace();
		}

		return zipfileEntity;
	} // end downloadMapTiles()
    
    
    /**
     * Prints the message to send.
     * @param xmlString a String that is an xml string to send to a rest server.
     */
    private void printMessageToSend (String xmlString) {
    	Log.i(CLASSNAME, "What are we sending: " + xmlString);
    } // end printMessageToSend
	
    
    /**
     * Retrieves project information for an unregistered user.
     * 
     * @return a String that is an xml-document describing the project.
     */
    public String getUnregisteredUser() {
    	    	
    	httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost("/getmobileobjectunregistered");
    	httpPost.addHeader("Accept", "text/html");
    	httpPost.addHeader("Content-Type", "application/xml");
    	HttpResponse response = null;
    	String serverResponse = null;
    	
    	// TODO: Move this to mainmenu and make the string a string in the resources.
    	String xmlBody = "<credentials><userName></userName><password></password><sessionId></sessionId><projectId></projectId><argument1>" + PROJECT_NAME + "</argument1><argument2></argument2></credentials>";
    	try {
    		StringEntity entity = new StringEntity(xmlBody, "UTF-8");
	    	entity.setContentType("application/xml");
	    	httpPost.setEntity(entity);
	    	
	    	response = httpClient.execute(httpHost, httpPost);
	    	serverResponse = EntityUtils.toString(response.getEntity());
	    }
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}

    	httpClient.getConnectionManager().shutdown();
    	
    	return serverResponse;
    } // end getUnregisteredUser()

    
    /**
     * Retrieves project information and account information for registered 
     * user from the server.
     * 
     * @return a String that is an xml-document describing the project and account.
     */
    public String getRegisteredUser(String userName, String password) {
    	
    	httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost("/getmobileobjectregistered");
    	httpPost.addHeader("Accept", "text/html");
    	httpPost.addHeader("Content-Type", "application/xml");
    	HttpResponse response = null;
    	String serverResponse = null;
    	
    	// TODO: Move this to mainmenu and make the string a string in the resources.
    	String xmlBody = "<credentials><userName>" + userName + "</userName><password>" + password + 
    			         "</password><sessionId></sessionId><projectId></projectId>" +
    			         "<argument1>" + PROJECT_NAME + "</argument1><argument2></argument2>" + 
    			         "</credentials>";
    	try {
    		StringEntity entity = new StringEntity(xmlBody, "UTF-8");
	    	entity.setContentType("application/xml");
	    	httpPost.setEntity(entity);
	    	
	    	response = httpClient.execute(httpHost, httpPost);
	    	serverResponse = EntityUtils.toString(response.getEntity());
	    }
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	
    	return serverResponse;
    } // end getRegisteredUser()
	
} // end class RestClient
