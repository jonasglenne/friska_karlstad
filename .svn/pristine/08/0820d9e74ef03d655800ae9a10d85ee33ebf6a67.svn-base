package jonas.com.friskakarlstad.utilities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import jonas.com.friskakarlstad.pojos.Control;

import org.apache.http.Header;
import org.apache.http.HeaderIterator;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeaderIterator;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;


import android.util.Log;

public class RestClient {
	
	// The classname used as a tag for Log printouts.
	public static final String CLASSNAME = "RestClient";

	// http://privat.bahnhof.se/wb739361/GAETiles.zip
	// The location for the map tiles.
	final static String MAP_TILES_HTTP_LOCATION = "https://docs.google.com/uc?id=0BytBBI0ugXU-MDE2ODAzZDktMmM2OS00MjliLTllNGYtYmE2YWRiNmIwZTVm&export=download&authkey=CLXt3MkE&hl=sv";

	// The base directory for the map tiles.
	public final static String DATA_STORAGE_BASE = "/data/friska_karlstad/";

	// The GAE host.
	private HttpHost targetHostGAE;

	// The Stamford host.
	private HttpHost targetHostStamford;
	
	// The http client for this REST client class.
	DefaultHttpClient httpClient;
	
	// The expected number of cookies to be set by the Stamford server.
	private static int NR_OF_COOKIES = 2;
	
	
	/**
	 * Constructor for class RestClient
	 */
	public RestClient() {
		targetHostGAE = new HttpHost("checkpointserver.appspot.com", 443, "https");
	} // end constructor RestClient()

	
	/**
	 * Executes a POST request and returns the respone
	 * 
	 * @param postURI a String that is the URI for the request
	 * @param xmlBody a String that is the XML body for the request
	 * @return a HttpResponse containing the results from the request
	 */
	private HttpResponse executeGAEPostRequest(String postURI, String xmlBody) {

		printMessageToSend(xmlBody.toString());

		httpClient = new DefaultHttpClient();

		HttpPost httpPost = new HttpPost(postURI);
    	httpPost.addHeader("Accept", "text/html");
    	httpPost.addHeader("Content-Type", "application/xml");
    	HttpResponse response = null;
    	
    	try {
    		StringEntity entity = new StringEntity(xmlBody, "UTF-8");
	    	entity.setContentType("application/xml");
	    	httpPost.setEntity(entity);
	    	
	    	response = httpClient.execute(targetHostGAE, httpPost);
			//Log.i(CLASSNAME, "Statusline: " + response.getStatusLine());
			//Log.i(CLASSNAME, "Reasonphrase: " + response.getStatusLine().getReasonPhrase());
    	}
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}

    	return response;
	} // end executeGAERequest()
	
	
	/**
	 * Registers a control to the GAE server with a control object as argument.
	 * 
	 * @param theControlsToRegister an ArrayList of Controls that is the controls to register.
	 * @param registeredUser a RegisteredUser that is the user to register controls for.
	 * @return a boolean that indicates the result of the registration
	 */
	public boolean registerControls(ArrayList<Control> theControlsToRegister, String username, 
			                       String password, String projectName) {
		Log.i(CLASSNAME, "Register controls to the server");

		boolean result = false;

		StringBuilder xmlBody = new StringBuilder();

		xmlBody.append("<controls>");
		for (Control currentControl : theControlsToRegister) {
			xmlBody.append("<control>");
			xmlBody.append("<controlNumber>" + currentControl.getControlNumber() + "</controlNumber>");
			xmlBody.append("<clientType>" + currentControl.getClientType() + "</clientType>");
			xmlBody.append("<timeStamp>" + currentControl.getTimeStamp() + "</timeStamp>");
			xmlBody.append("<projectName>" + currentControl.getProjectName() + "</projectName>");
			xmlBody.append("</control>");
		}
		xmlBody.append("<userName>" + username +  "</userName>");
		xmlBody.append("<password>" + password + "</password>");
		xmlBody.append("<sessionId></sessionId>");
		xmlBody.append("<projectName>" + projectName + "</projectName>");
		xmlBody.append("</controls>");
    	
    	try {
	    	HttpResponse response = executeGAEPostRequest("/addcontrols", xmlBody.toString());
	    	
	    	// TODO: Check what the server response can be and update this part accordingly to indicate success or failure.
//    		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED) {
//    			Log.i(CLASSNAME, "Register control at GAE for user...success");
//    			result = true;
//    		}
//	    	else {
//	    		Log.e(CLASSNAME, "Register control at GAE for user...failed");
//    			result = false;
//	    	}
    	}
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	httpClient.getConnectionManager().shutdown();
    	
    	return result;
	} // end registerControl()	
	
	
	/**
	 * Registers a user at the GAE server.
	 * 
	 * @param userId a String that is the device id for the phone and serves as the unique id on GAE for this user.
	 * @param xmlMessage a String that is XML-body for the entity to send to the server
	 * @return a boolean that is the result of the registration of the user
	 */
	public boolean registerUser(String userId, String xmlMessage) {
		Log.i(CLASSNAME, "registerUser(): Registering user at the GAE server.");
		
		// Always assume the worst...
		boolean result = false;
		
		httpClient = new DefaultHttpClient();
		Log.i(CLASSNAME, "Create user at GAE...");
		Log.i(CLASSNAME, xmlMessage);

    	HttpPut httpPut = new HttpPut("/user/" + userId);
    	httpPut.addHeader("Accept", "text/html");
    	httpPut.addHeader("Content-Type", "application/xml");
    	try {
    		StringEntity entity = new StringEntity(xmlMessage, "UTF-8");
	    	entity.setContentType("application/xml");
	    	httpPut.setEntity(entity);
	    	
	    	HttpResponse response = httpClient.execute(targetHostGAE, httpPut);
	    	
	    	// If the return code is 201 then there was no previous account at gae.
	    	// If it is 200 then it means the user has already registered once but is 
	    	// now updating. Both are ok.
    		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED ||
    				response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
	    		Log.i(CLASSNAME, "Create user at GAE...Succeeded");
    			result = true;
    		}
	    	else {
	    		Log.i(CLASSNAME, "Create user at GAE...Failed");
    			result = false;
	    	}
			Log.i(CLASSNAME, "Statusline: " + response.getStatusLine());
			Log.i(CLASSNAME, "Reasonphrase: " + EntityUtils.toString(response.getEntity()));
    	}
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	httpClient.getConnectionManager().shutdown();

    	return result;
	} // end sendXMLToServer()
	
	
	/**
	 * Downloads the map tiles zip file from the web.
	 * 
	 * @param mapURL a String that is the address to the map file location.
	 * 
	 * @return a HttpEntity containing the stream to the zip file.
	 */
	public HttpEntity downloadMapTiles(String mapURL) {
		
		HttpResponse response = null;
	    HttpEntity zipfileEntity = null;

		httpClient = new DefaultHttpClient();

	    // Get the map tiles zip-file from the server.
	    HttpGet httpGet = new HttpGet(mapURL);
	    try {
			response = httpClient.execute(httpGet);
			zipfileEntity = response.getEntity();
		} 
	    catch (ClientProtocolException e) {
			e.printStackTrace();
		} 
	    catch (IOException e) {
			e.printStackTrace();
		}

		return zipfileEntity;
	} // end downloadMapTiles()
    
    
    /**
     * Prints the message to send.
     * @param xmlString a String that is an xml string to send to a rest server.
     */
    private void printMessageToSend (String xmlString) {
    	Log.i(CLASSNAME, "What are we sending: " + xmlString);
    } // end printMessageToSend
	
    
    /**
     * Retrieves project information for an unregistered user.
     * 
     * @return a String that is an xml-document describing the project.
     */
    public String getUnregisteredUser() {
    	
    	httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost("/getmobileobjectunregistered");
    	httpPost.addHeader("Accept", "text/html");
    	httpPost.addHeader("Content-Type", "application/xml");
    	HttpResponse response = null;
    	String serverResponse = null;
    	
    	// TODO: Move this to mainmenu and make the string a string in the resources.
    	String xmlBody = "<credentials><userName></userName><password></password><sessionId></sessionId><projectId></projectId><argument1>Friska Karlstad 2012</argument1><argument2></argument2></credentials>";
    	try {
    		StringEntity entity = new StringEntity(xmlBody, "UTF-8");
	    	entity.setContentType("application/xml");
	    	httpPost.setEntity(entity);
	    	
	    	response = httpClient.execute(targetHostGAE, httpPost);
			//Log.i(CLASSNAME, "Statusline: " + response.getStatusLine());
			//Log.i(CLASSNAME, "Reasonphrase: " + response.getStatusLine().getReasonPhrase());
	    	serverResponse = EntityUtils.toString(response.getEntity());
	    }
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}

    	
    	return serverResponse;
    } // end getUnregisteredUser()

    
    /**
     * Retrieves project information and account information for registered 
     * user from the server.
     * 
     * @return a String that is an xml-document describing the project and account.
     */
    public String getRegisteredUser(String userName, String password) {
    	
    	httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost("/getmobileobjectregistered");
    	httpPost.addHeader("Accept", "text/html");
    	httpPost.addHeader("Content-Type", "application/xml");
    	HttpResponse response = null;
    	String serverResponse = null;
    	
    	// TODO: Move this to mainmenu and make the string a string in the resources.
    	String xmlBody = "<credentials><userName>" + userName + "</userName><password>" + password + "</password><sessionId></sessionId><projectId></projectId><argument1>Friska Karlstad 2012</argument1><argument2></argument2></credentials>";
    	try {
    		StringEntity entity = new StringEntity(xmlBody, "UTF-8");
	    	entity.setContentType("application/xml");
	    	httpPost.setEntity(entity);
	    	
	    	response = httpClient.execute(targetHostGAE, httpPost);
			//Log.i(CLASSNAME, "Statusline: " + response.getStatusLine());
			//Log.i(CLASSNAME, "Reasonphrase: " + response.getStatusLine().getReasonPhrase());
	    	serverResponse = EntityUtils.toString(response.getEntity());
	    }
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	
    	return serverResponse;
    } // end getRegisteredUser()
	
} // end class RestClient
