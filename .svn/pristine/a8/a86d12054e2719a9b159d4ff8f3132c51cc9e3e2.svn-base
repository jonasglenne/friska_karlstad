package jonas.com.friskakarlstad.utilities;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

/**
 * A measure stick that will show the distance between two points.
 * 
 * @author jonast42@yahoo.com
 */
public class MeasureStickOverlay extends Overlay {

	public static final String CLASSNAME = "MeasureStickOverlay";
	
	// The context this class is working with.
	private Context mContext;
	
	// The start of the measure stick in the x-axis.
	private int xPosition;

	// The start of the measure stick in the y-axis.
	private int yPosition;
	
	// The length of the stick in pixels (converted from meters.
	private int pixelLength;
	
	// The Paint used to draw the measure stick.
	private Paint measureStickPaint;
	
	private int zoomLevel;
	
	private String measureStickText;
	
    /**
	 * Constructor for class MeasureStickOverlay.
	 * @param pixelLength an int that is the length in pixels of the selected length for the stick.
	 * @param context a Context which is the context in which this class is operating.
	 */
	public MeasureStickOverlay(Context context, int theXPosition, int theYPosition, int initialZoomLevel) {
		mContext = context;
		xPosition = theXPosition;
		yPosition = theYPosition;
		pixelLength = 0;
		zoomLevel = initialZoomLevel;

        measureStickPaint = new Paint();
        measureStickPaint.setDither(false);
        measureStickPaint.setColor(Color.WHITE);
        measureStickPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        measureStickPaint.setStrokeJoin(Paint.Join.ROUND);
        measureStickPaint.setStrokeCap(Paint.Cap.ROUND);
        measureStickPaint.setStrokeWidth(6);
	} // end MeasureStickOverlay()


	/**
	 * Updates the current zoom level this measure stick should take into consideration
	 * for the calculations.
	 * 
	 * @param newZoomLevel
	 */
	public void changeZoomLevel(int newZoomLevel)
	{
		zoomLevel = newZoomLevel;
	} // end changeZoomLevel()
	
	/**
	 * Calculates the meters to pixel ratio
	 * 
	 * @param theMapView the MapView on which to calculate the pixel length on.
	 */
	private void calculateMeasurements(MapView theMapView)
	{
		float meters = 500;
		if (zoomLevel < 13)
		{
			meters = 5000;
			measureStickText = "5 km";
		}
		else if (zoomLevel < 14)
		{
			meters = 2500;
			measureStickText = "2,5 km";
		}
		else if (zoomLevel < 15)
		{
			meters = 1000;
			measureStickText = "1 km";
		}
		else if (zoomLevel < 16)
		{
			meters = 500;
			measureStickText = "500 m";
		}
		else if (zoomLevel < 17)
		{
			meters = 250;
			measureStickText = "250 m";
		}
		else if (zoomLevel < 18)
		{
			meters = 125;
			measureStickText = "125 m";
		}
		else if (zoomLevel < 19)
		{
			meters = 72;
			measureStickText = "72 m";
		}
		// Calculate the pixel length with adjusting for latitude correction (1/ Math.cos(Math.toRadians(59.38738645732299))
		//@TODO Change the value in latitude to dynamic value or maybe at least an approximate dynamic value.
		pixelLength = (int)(theMapView.getProjection().metersToEquatorPixels(meters) * (1/ Math.cos(Math.toRadians(59.38738645732299))));
	} // end calculateMeasurements()
	
	
	@Override
    public void draw(Canvas canvas, MapView theMapView, boolean shadow){
        super.draw(canvas, theMapView, false);

        calculateMeasurements(theMapView);

        int halfCanvasWidth = canvas.getWidth() / 2;
        int canvasHeight = canvas.getHeight();
        
        canvas.drawLine(halfCanvasWidth + xPosition, canvasHeight - yPosition, 
        				halfCanvasWidth + xPosition + pixelLength, canvasHeight - yPosition, measureStickPaint);
        canvas.drawLine(halfCanvasWidth + xPosition, (canvasHeight - yPosition)  + 2.5f, 
        				halfCanvasWidth + xPosition, (canvasHeight - yPosition) - 2.5f, measureStickPaint);
        canvas.drawLine(halfCanvasWidth + xPosition + pixelLength, (canvasHeight - yPosition)  + 2.5f, 
        				halfCanvasWidth + xPosition + pixelLength, (canvasHeight - yPosition) - 2.5f, measureStickPaint);
        //canvas.drawPath(measureStickPath, measureStickPaint);
        canvas.drawText(measureStickText, (halfCanvasWidth + xPosition) + (pixelLength /2) - 5, 
        				(canvasHeight - yPosition) - 10, measureStickPaint);
        
        measureStickPaint.setColor(Color.RED);
        measureStickPaint.setStrokeWidth(2);

        canvas.drawLine(halfCanvasWidth + xPosition, canvasHeight - yPosition, 
        				halfCanvasWidth + xPosition + pixelLength, canvasHeight - yPosition, measureStickPaint);
        canvas.drawLine(halfCanvasWidth + xPosition, (canvasHeight - yPosition)  + 2.5f, 
        				halfCanvasWidth + xPosition, (canvasHeight - yPosition) - 2.5f, measureStickPaint);
        canvas.drawLine(halfCanvasWidth + xPosition + pixelLength, (canvasHeight - yPosition)  + 2.5f, 
        				halfCanvasWidth + xPosition + pixelLength, (canvasHeight - yPosition) - 2.5f, measureStickPaint);
        canvas.drawText(measureStickText, halfCanvasWidth + xPosition + (pixelLength /2) - 5, (canvasHeight - yPosition) - 10, measureStickPaint);

	} // end draw()

} // end class MapItemsOverlay
