package jonas.com.friskakarlstad.activities;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jonas.com.friskakarlstad.R;
import jonas.com.friskakarlstad.R.id;
import jonas.com.friskakarlstad.R.layout;
import jonas.com.friskakarlstad.R.menu;
import jonas.com.friskakarlstad.R.string;
import jonas.com.friskakarlstad.pojos.Control;
import jonas.com.friskakarlstad.utilities.ControlFileHandler;
import jonas.com.friskakarlstad.utilities.RestClient;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Class that handles validation and registration of a control.
 *
 * @author Jonas
 */
public class RegisterControlView extends Activity {

	// Tag used for Log printouts.
	public static final String CLASSNAME = "RegisterControlView";
	
	// Dialog id if it is found out that the control is already scanned.
	static final int DIALOG_ALREADY_SCANNED_ID = 0;

	// Dialog id if it is found out that the control is already stored on the server.
	static final int DIALOG_ALREADY_STORED_ID = 1;

	// An id for a progress dialog shown during post to server.
	static final int PROGRESS_DIALOG = 2;

	// An id for an Alert dialog shown when it was not possible to store the control to the server.
	static final int STORAGE_NOT_POSSIBLE = 3;

	// And id for an alert dialog shown when attempting to register a control for an unregistered user.
	static final int USER_NOT_REGISTERED = 4;
	
	// Id to handle the problem if a user does not have the QR-code reader installed.
	static final int BARCODE_SCANNER_NOT_INSTALLED = 5;
	
	// Heading for this view.
	private TextView tvResultScanHeading;
	
	// Heading for control number.
	private TextView tvControlNumberHeading;
	
	// Displays the control number.
	private TextView tvControlNumber;

	// Heading for control code
	private TextView tvControlCodeHeading;
	
	// Allows the user to fill in a control code manually.
	private EditText tvControlCode;
	
	// Shows the description for the location of the control.
	private TextView tvControlDescriptionHeading;
	
	// Shows the description for the location of the control.
	private TextView tvControlDescription;

	// A Picture of the company that sponsor the scanned control.
	private ImageView ivCompanyCommImage;
	
	// Posts the scanned control to server.
	private Button btnScan;
	
	// Communicates via the REST-API on google app-engine.
	private RestClient restClient;

	// The value stored in the QR-code which is scanned.
	private String scanCode;
	
	// The list of controls stored locally.
	private List<Control> controlList;
	
	// Progress dialog for lengthy operations.
	private ProgressDialog requestProgressDialog;
	
	// The control id of the currently selected control.
	private String controlId;
	
	// The control code (e.g. NA, ��) for the currently selected control.
	private String controlCode;
	
	// The time stamp when this control was scanned or typed in manually.
	private String timeStamp;
	
	// The user id for the application user.
	private String userId;

	private boolean scannerAppInstalled;
	
	// If true indicates that this wiev was started from the main menu. In this case we want special handling
	// in case the scanner application is not installed.
	private boolean viewStartedFromMain;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_control_view_layout);

        restClient = new RestClient();
        scannerAppInstalled = true; // Assume the user has this installed.
        
        // Load the control list from file for check agains entered/scanned control id and control code.
        try {
        	controlList = ControlFileHandler.getLocalControlFileList(openFileInput(ControlFileHandler.CONTROLLISTFILENAME));
		} 
        catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}        
        
        tvResultScanHeading = (TextView) findViewById(R.id.tvResultScanHeading);    
        
        tvControlNumberHeading = (TextView) findViewById(R.id.tvControlNumberHeading);
        tvControlNumber = (TextView) findViewById(R.id.tvControlNumber);
    	
        tvControlCodeHeading = (TextView) findViewById(R.id.tvControlCodeHeading);
    	tvControlCode = (EditText) findViewById(R.id.tvControlCode);
    	tvControlCode.addTextChangedListener(new TextWatcher(){
			@Override
            public void afterTextChanged(Editable s) {}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// If the user types the control code manually, set controlCode to this value.
				controlCode = tvControlCode.getText().toString();
				
				// If the user have filled in the control manually, change the text on the button to
				// allow the user to store the control on the server.
				if (tvControlCode.getText().toString().length() == 2) {
					btnScan.setText(getString(R.string.register_control_str));
				}
				else {
					btnScan.setText(getString(R.string.scan_control_str));
				}
			}
    	});
    	
        tvControlDescriptionHeading = (TextView) findViewById(R.id.tvControlDescriptionHeading);
        tvControlDescription = (TextView) findViewById(R.id.tvControlDescription);
        
        
        // This button will start up the scanner to scan a control.
        btnScan = (Button) findViewById(R.id.btnScan);
        btnScan.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	executeButtonClick(btnScan.getText().toString());
            }
        });

	    // Add possible advertisement in the imageview.
    	ivCompanyCommImage = (ImageView) findViewById(R.id.ivCompanyCommImage);
    	
        // If this view is created with an extra it means that the user selected a control
        // from the map or the control list view. In these cases, the control id will be 
        // fed in the extras to this view. Use this id to fill in the information about 
        // the control but leave the control code out. If the user selected the scan activity
        // directly from the main menu. The control id cannot be determined until after the
        // scan, then start the scan view directly.
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
	    	viewStartedFromMain = false;
	    	controlId = extras.getString("control_id");
	    	checkControlStatusFromMap();
	    	
	    	if (!controlList.get(Integer.parseInt(controlId) -1).getImageLink().equals("")) {
				new AsyncLoadAdvertisement().execute(controlList.get(Integer.parseInt(controlId) - 1).getImageLink());
	    	}
	    	
	    	// If the control is registered already, prohibit changes to code.
	    	if (controlList.get(Integer.parseInt(controlId) - 1).isRegisteredOnServer()) {
	    		tvControlCode.setEnabled(false);
	    	}
        }
	    else {
	    	viewStartedFromMain = true;
	    	startScanView();
	    }
	} // end onCreate()
	
	
	/**
	 * Selects correct method when pressing the button. What method to call is dependent on the 
	 * text of the button which represents the state of the selected control (scan or store).
	 */
	private void executeButtonClick(String buttonText) {

		if (buttonText.equals(getString(R.string.scan_control_str))) {
			startScanView();
		}
		else {
	    	// Check the user id. If it not possible to retrieve this value from the preferences it
	    	// means that the user is not registered and should be sent to the registration page.
	    	SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
	    	userId = settings.getString("user_id", null);
	    	
	    	// Check that the user is registered.
	    	if (userId != null) {
	    		
	    		// Check that the code + id match.
				if (validateControlCode()) {
					new StoreToServerTask().execute();
				}
	    	}
			else {
	    		// If the user is not registered, they have to do it first.
	    		showDialog(USER_NOT_REGISTERED);
	    	}
		}
	} // end executeButtonClick()

	
	/**
	 * Class that uses worker thread to download an advertisement image
	 * asynchronously.
s	 */
	private class AsyncLoadAdvertisement extends AsyncTask<String, Void, Drawable> {

		@Override
		protected void onPreExecute() {} // end onPreExecute()
	
	    @Override
		protected Drawable doInBackground(String... imageUrl) {

	    	InputStream is = null;
	    	Drawable image = null;
	    	URL imageURL = null;
			try {
				imageURL = new URL(imageUrl[0]);
				is = (InputStream)imageURL.getContent();
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
			
			if (is != null) {
		    	image = Drawable.createFromStream(is, "advertiser");;		
			}
			return image;
	    } // end doInBackground()
	    
	    @Override
	    protected void onProgressUpdate(Void... values) {}
	    
	    @Override
	    protected void onPostExecute(Drawable image) {
	    	ivCompanyCommImage.setImageDrawable(image);
	    } // end onPostExecute()

	} // end class StoreToServerTask
	
	
	/**
	 * Starts a view to allow scanning of a control. The view will return the scan result.
	 */
	private void startScanView() {
    	
		// Use the ZXING client to scan the control
		try {
	        Intent scanIntent = new Intent("com.google.zxing.client.android.SCAN");
	        scanIntent.setPackage("com.google.zxing.client.android");
	        scanIntent.putExtra("SCAN_MODE", "QR_CODE_MODE");
	        startActivityForResult(scanIntent, 0);
		}
		catch (ActivityNotFoundException e) {
			scannerAppInstalled = false;
			showDialog(BARCODE_SCANNER_NOT_INSTALLED);
			e.printStackTrace();
		}
	} // end startScanView()
 
    
    /**
     * Returns the current time in the system in the form of 2011-05-15T13:36:33.+02:00
     * 
     * @return a String that is a String representation of the Time object.
     */
    private String returnTimeStampAsString() {
    	
    	SimpleDateFormat dateFormatter = new SimpleDateFormat ("yyyy-MM-dd'T'HH:mm:ssZ");
    	// Because java uses format 2011-05-15T13:36:33.+0200 instead of 2011-05-15T13:36:33.+02:00 (with semi-colon in time difference.
    	timeStamp = dateFormatter.format(new Date());
    	timeStamp = timeStamp.substring(0,22) + ":" + timeStamp.substring(22);
    	// Set the local variable for time stamp
    	return timeStamp.toString();
    } // end returnTimeStampAsString()
    
    
    /**
     * Check that the control code exists before storing it on the server. This check is carried 
     * out against the local control list.
     * 
     * @return a boolean indicating that everything is ok with the selected control or not.
     */
    private boolean validateControlCode() {
    	//Always assume the worst.
    	boolean controlOk = false;
    		
		// Since the list is sorted numerically on control id it can be checked directly with index access 
		// against the control id for the currently selected control.
		if (controlList.get(Integer.parseInt(controlId) - 1).getCodeName().equalsIgnoreCase(controlCode)) {
			// Set the timestamp for the control
			controlList.get(Integer.parseInt(controlId) - 1).setTimeStamp(returnTimeStampAsString());
			controlOk = true;
		}
		else {
			showToast("Felaktig kontrollkod angiven. Prova igen att fylla i eller eller scanna kontrollen");
			tvControlCode.requestFocus();
		}

		return controlOk;
    } // end validateControlCode()

    
    /**
     * Registers a control, first on GAE and then on Stamford. The reason to store in
     * GAE is that it is easier to undo the changes to GAE than Stamford if one of them
     * fails.
     * 
     * @return a boolean that indicates if everything went ok on both servers.
     */
    private boolean storeControlOnServers() {
    	boolean bothOk = false;
  
    	// Get the stored username and password for Stamford for the user.
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
   		String stamfordUsername = settings.getString("stamford_username", null);
   		String stamfordPassword = settings.getString("stamford_password", null);
    	
   		if (stamfordUsername == null || stamfordPassword == null) {
   			startActivity(new Intent(this, LoginOrRegisterView.class));
   		}
   		else {
   			
   			Control controlToRegister = new Control(controlId, controlCode, timeStamp);
   			
	    	// If the control could be saved to GAE server, continue with Stamford attempt
	    	if (restClient.registerControl(controlToRegister, userId)) {
	
	    		if (restClient.registerControlAtStamford(controlId, controlCode, stamfordUsername, stamfordPassword)) {
	    	    	bothOk = true;
	    	    	
	        		int controlsTaken = settings.getInt("controls_taken", 0);
	        		SharedPreferences.Editor editor = settings.edit();
	        	    editor.putInt("controls_taken", ++controlsTaken);
	        	    editor.commit();
	    		}
	    		else {
	    			Log.e(CLASSNAME, "Kunde inte spara kontroll p� Stamford.");
	    		}
	    	}
	    	else {
	    		Log.e(CLASSNAME, "Kunde inte spara kontroll p� GAE.");
	    	}
   		}
    	return bothOk;
    } // end storeControlOnServers()


    /**
     * Posts a control to the server.
     * 
     * @param controlRegisterSuccess a boolean that indicates whether the storage on the server was successful or not.
     */
	private void updateControlStatus(boolean controlRegisterSuccess) {
		
		if (controlRegisterSuccess == true) {

			controlList.get(Integer.parseInt(controlId) - 1).setScanned(true);
			controlList.get(Integer.parseInt(controlId) - 1).setStoredOnServer(true);

			btnScan.setText(getString(R.string.control_already_registered_str));
			btnScan.setEnabled(false);
			btnScan.setBackgroundColor(Color.GRAY);
			btnScan.setTextColor(Color.DKGRAY);
		}
		else {
			controlList.get(Integer.parseInt(controlId) - 1).setScanned(true);
			controlList.get(Integer.parseInt(controlId) - 1).setStoredOnServer(false);
			showDialog(STORAGE_NOT_POSSIBLE);
		}
		
		saveUpdatedControlList();
		
	} // end postControlToServer()

	
	/**
	 * If the user opens this view calling from Activity MapNavigatorView, Control status is checked
	 * in this method.
	 */
	private void checkControlStatusFromMap() {
		
		Control selectedControl = controlList.get(Integer.parseInt(controlId) - 1);

		tvControlNumber.setText(selectedControl.getControlId());
        tvControlDescription.setText(selectedControl.getDescription());
		
		if (selectedControl.isScanned() && !selectedControl.isRegisteredOnServer()) {
			tvControlCode.setText(selectedControl.getCodeName());
			btnScan.setText(getString(R.string.register_control_str));
		}
		else if (selectedControl.isScanned() && selectedControl.isRegisteredOnServer()) {
			tvControlCode.setText(selectedControl.getCodeName());
			
			btnScan.setText(getString(R.string.control_already_registered_str));
			btnScan.setEnabled(false);
			btnScan.setBackgroundColor(Color.GRAY);
			btnScan.setTextColor(Color.DKGRAY);
		}
		else {
			btnScan.setText(getString(R.string.scan_control_str));
			// Remove the code so the user has to be at the control to scan it.
			tvControlCode.setText("");
		}
	} // checkControlStatusFromMap()
	
	
	/**
	 * Checks the status in the local control list based on a control id (e.g. 1-100).
	 */
	private void checkControlStatusAfterScan() {
		
		Control selectedControl = controlList.get(Integer.parseInt(controlId) - 1);
		String localControlCode = selectedControl.getCodeName();
		
		// TODO Remove this in next years edition of FK.
		// Because swedish characters are strangely encoded in the qr-codes, do a special 
		// check that allows only one character to match.
		if (localControlCode.substring(0, 1).equalsIgnoreCase(controlCode.substring(0,1)) ||
			localControlCode.substring(1, 2).equalsIgnoreCase(controlCode.substring(1,2))) {
				// Use the locally stored control code to rectify any incorrect character 
			    // (i.e. this years �,�.� problem in QR-code)
				controlCode = localControlCode;

				// Set the timestamp for the control
				controlList.get(Integer.parseInt(controlId) -1).setTimeStamp(returnTimeStampAsString());
				
				// Check if there is any advertisement connected to this control.
				if (!controlList.get(Integer.parseInt(controlId) -1).getImageLink().equals("")) {
					new AsyncLoadAdvertisement().execute(controlList.get(Integer.parseInt(controlId) - 1).getImageLink());
				}
				
				tvControlNumber.setText(selectedControl.getControlId());
		        tvControlDescription.setText(selectedControl.getDescription());
		        controlList.get(Integer.parseInt(controlId) -1).setScanned(true);
				
				if (selectedControl.isRegisteredOnServer() == false) {
					tvControlCode.setText(selectedControl.getCodeName());
					btnScan.setText(getString(R.string.register_control_str));
				}
				else if (selectedControl.isRegisteredOnServer()) {
					tvControlCode.setText(selectedControl.getCodeName());
					btnScan.setText(getString(R.string.control_already_registered_str));
					btnScan.setEnabled(false);
					btnScan.setBackgroundColor(Color.GRAY);
					btnScan.setTextColor(Color.DKGRAY);
				}
				else {
					btnScan.setText(getString(R.string.scan_control_str));
					// Remove the code so the user has to be at the control to scan it.
					tvControlCode.setText("");
				}
		}
	} // end checkControlId()

	
	/**
	 * If the user attempts to register a control without being registred, a view to allow
	 * new registration or synch with existing account at Stamford should be shown.
	 */
	private void openLoginOrRegisterUserView() {
		startActivity(new Intent (this, LoginOrRegisterView.class));
	} // end openLoginOrRegisterUserView()
	

	@Override
	protected Dialog onCreateDialog(int id) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		
		switch(id) {
        
        case PROGRESS_DIALOG:
        	requestProgressDialog = new ProgressDialog(this);
        	requestProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        	requestProgressDialog.setMessage("Sparar kontroll...");
            return requestProgressDialog;
            
        case STORAGE_NOT_POSSIBLE:
        	builder.setMessage("Kontrollen kunde inte registreras hos Friska Karlstad. Programmet kommer att spara kontrollen och g�ra ett nytt f�rs�k att registrera den automatiskt vid n�sta programstart.")
        	       .setCancelable(false)
        	       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	        	   dialog.cancel();
        	           }
        	       });
        	break;
        case USER_NOT_REGISTERED:
        	builder.setMessage("Du �r inte registrerad �nnu. Tryck p� OK om du vill g� till registrering.")
        	       .setCancelable(true)
        	       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	        	   dialog.cancel();
        	        	   openLoginOrRegisterUserView();
        	           }
        	       }).setNegativeButton("Avbryt", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	        	   dialog.cancel();
        	           }
        	       });
        	break;
        case BARCODE_SCANNER_NOT_INSTALLED:
        	builder.setMessage("Du saknar applikationen f�r scanning. Den �r gratis och finns p� Android Market. Vill du installera det?")
 	       .setCancelable(true)
 	       .setPositiveButton("Ja", new DialogInterface.OnClickListener() {
 	           public void onClick(DialogInterface dialog, int id) {
 	        	   dialog.cancel();			
 	        	   Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.zxing.client.android"));
 				   startActivity(marketIntent);
 				   finish();
 	           }
 	       }).setNegativeButton("Nej", new DialogInterface.OnClickListener() {
 	           public void onClick(DialogInterface dialog, int id) {
 	        	   dialog.cancel();
 	        	   // If the view was started from main menu we want to go back to that menu otherwise let the user finish registration manually
	        	   if (viewStartedFromMain == true) {
	        		   finish();
	        	   }
 	           }
 	       });
        	break;
		default:
            break;
		}
        return builder.create();
    } // end onCreateDialog()
	

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {
			
	        if (resultCode == RESULT_OK) {
	        	
	        	// Set the scan code to the current content read by the scanner.
	            scanCode = intent.getStringExtra("SCAN_RESULT");

	            // TODO Check if the scan is well formed i.e. NNNXX
	            // Check that the scanned value is well formed (NNNXX). This is only valid if
	            // the user chooses the scanner directly from the main menu.
	            Pattern p = Pattern.compile("\\d{3}\\w{2}", Pattern.CASE_INSENSITIVE);
				Matcher m = p.matcher(scanCode);
				boolean patternFound = false;
				while (m.find()) {
					patternFound = true;
				}
				
				if (patternFound) {
		            controlCode = scanCode.substring(3,5);
		            	
		            // To accomodate for the number format nnn (e.g 001) remove prefix zeores.
		            if (scanCode.startsWith("0", 1)) {
		            	controlId = scanCode.substring(2,3);
		            }
		            else if (scanCode.startsWith("0")) {
		            	controlId = scanCode.substring(1,3);
		            }
		            else {
		            	controlId = scanCode.substring(0,3);
		            }
		        	checkControlStatusAfterScan();
				}
				else {
					showToast("Felaktig kontrollkod. Prova att scanna kontrollen igen");
					btnScan.setText(getString(R.string.scan_control_str));
					tvControlCode.setEnabled(false);
				}
	        } 
	        else if (resultCode == RESULT_CANCELED) {
	            // If this activity was started directly from main menu. Return there on cancel.
	        	//if (getIntent().getExtras() == null) {
	        		if (scannerAppInstalled == true) {
		        		finish();
	        		}
	        	//}
	        }
	    }
	} // end onActivityResult()

	
    /**
     * Displays a toast message on screen
     * @param toastText a String that is the text to display.
     */
    private void showToast(String toastText) {
    	int duration = Toast.LENGTH_LONG;
    	Toast toast = Toast.makeText(this, toastText, duration);
    	toast.show();
    } // end showToast()
    
    @Override
    protected void onRestart() {
    	super.onRestart();
        // Load the control list from file for check agains entered/scanned control id and control code.
        try {
        	controlList = ControlFileHandler.getLocalControlFileList(openFileInput(ControlFileHandler.CONTROLLISTFILENAME));
		} 
        catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}   
    }
    
    
    /**
     * Class used to contact the server asynchronously in a separate thread from the 
     * UI-thread to register a control.
     */
    private class StoreToServerTask extends AsyncTask<Void, Void, Boolean> {
    	
    	@Override
		protected void onPreExecute() {
    		showDialog(PROGRESS_DIALOG);
        } // end onPreExecute()

        @Override
		protected Boolean doInBackground(Void... params) {
        	return storeControlOnServers();
        	
        } // end doInBackground()
		
        protected void onProgressUpdate() {}

        protected void onPostExecute(Boolean result) {
        	removeDialog(PROGRESS_DIALOG);
        	//requestProgressDialog.dismiss();
			updateControlStatus(result);
        } // end onPostExecute()
        
    } // end class StoreToServerTask
    
    
	/**
	 * Saves the updated control list to file.
	 */
	private void saveUpdatedControlList() {
		try {
			ControlFileHandler.writeLocalControlFile(controlList, 
					openFileOutput(ControlFileHandler.CONTROLLISTFILENAME, Context.MODE_PRIVATE));
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	} // end saveUpdatedControlList()
	
    
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			
			if (requestProgressDialog != null && requestProgressDialog.isShowing()) {
				requestProgressDialog.dismiss();
			}
			finish();
		}
		return super.onKeyDown(keyCode, event);
	} // end onKeyDown()
	
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.register_control_options_menu, menu);
        return true;
    } // end onCreateOptionsMenu()
    
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        	case R.id.register_control_options_help:
    			Intent browserIntent = new Intent(this, InformationView.class);
    			browserIntent.putExtra("browser_url", "file:///android_asset/reg_contr_menu_help.html");
    			startActivity(browserIntent);
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
        }
    } // end onOptionsItemSelected()
	
} // end class ScannerView