package jonas.com.friskakarlstad.activities;

import java.util.ArrayList;
import java.util.List;

import jonas.com.friskakarlstad.R;
import jonas.com.friskakarlstad.R.array;
import jonas.com.friskakarlstad.R.id;
import jonas.com.friskakarlstad.R.layout;
import jonas.com.friskakarlstad.utilities.RestClient;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class ChangeAccountInfoView extends Activity {


	// Tag for Log printouts.
	public static final String CLASSNAME = "ChangeAccountInfoView";
	
	// Identifier for the dialog shown if user already exists.
	static final int USER_ALREADY_REGISTERED = 1;
	
	// Identifier for the dialog shown if user did not accept PUL.
	static final int PUL_NOT_ACCEPTED = 2;
	
	//Identifier for the dialog to show when no new password has been given when user 
	// try to change password.
	static final int NEW_PASSWORD_TOO_SHORT = 3;
	
	//Identifier for the dialog to show when the old password was incorrect when a user 
	// tried to change password.
	static final int OLD_PASSWORD_INCORRECT = 4;

	// Identifier for a dialog that shows progressing of update to user account.
	static final int UPDATING_ACCOUNT_ALERT = 6;
	
	// Identifier for a dialog that informs user the update of the account info has failed.
	static final int FAILED_TO_UPDATE_ACCOUNT = 7;
	
	// Heading for the user registration view.
	private TextView tvRegisterUserHeading;
	
	// Informs the user that some of the fields are required to be filled in
	// to be able to register.
	private TextView tvFieldsRequiredInfo;
	
	// Heading for the real name input EditText.
	private TextView tvRegisterFirstNameHeading;

	// Enter the real name here.
	private EditText etUpdateRegisterFirstName;

	// Heading for the real name input EditText.
	private TextView tvRegisterLastNameHeading;

	// Enter the real name here.
	private EditText etUpdateRegisterLastName;
	
	// Heading for the address input EditText.
	private TextView tvRegisterAddressHeading;

	// Enter the real name here.
	private EditText etUpdateRegisterAddress;

	// Heading for the address input EditText.
	private TextView tvRegisterPostalCodeHeading;

	// Enter the real name here.
	private EditText etUpdateRegisterPostalCode;

	// Heading for the address input EditText.
	private TextView tvRegisterPostalAddressHeading;

	// Enter the real name here.
	private EditText etUpdateRegisterPostalAddress;

	// Heading for the phone number input EditText.
	private TextView tvRegisterEmailAddress;

	// Enter the real name here.
	private EditText etUpdateEmailAddress;
	
	// Heading for the phone number input EditText.
	private TextView tvRegisterPhoneHeading;

	// Enter the real name here.
	private EditText etUpdateRegisterPhone;

	// Heading for the phone number input EditText.
	private TextView tvRegisterMobilePhoneHeading;

	// Enter the real name here.
	private EditText etUpdateRegisterMobilePhone;
	
	// Heading for the user name input EditText.
	private TextView tvRegisterUserNameHeading;

	// Enter the real name here.
	private EditText etUpdateRegisterUserName;
	
	// Heading for the password input EditText.
	private TextView tvOldPasswordHeading;

	// Enter the real name here.
	private EditText etOldPassword;
	
	private TextView tvRegisterUpdatePasswordHeading;
	
	private EditText etUpdateRegisterPassword;
	
	// Heading for the password input EditText.
	private TextView tvRegisterBirthHeading;

	// Enter the real name here.
	private EditText etUpdateRegisterBirth;

	// Registers the user
	private Button accountUpdateButton;
	
	// Cancels the registration
	private Button cancelButton;
	
	// Communicates via the REST-API on google app-engine.
	private RestClient restClient;
	
	// Checkbox to determine whether user want newsletter or not.
	private CheckBox cbWantsNewsLetter;
	
	// Checkbox to determine whether user wants to accept storing personal informaiton according to PUL.
	private CheckBox cbPulAcceptance;
	
	// String that represents the existing e-mail address. Used for comparison to the new one if the user
	// updates the e-mail address.
	private String oldEMailAddress;

	// Heading for the spinner where the user selects which user type he or she is.
	private TextView tvUserType;
	
	// Lets the user select which type of account this user should have.
	private Spinner spUserType;
	
	private SharedPreferences settings;
	
	private ProgressDialog progressDialog;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_user_view_layout);

        restClient = new RestClient();
        
		// Retrieve stored login credentials for the Stamford server
		settings = PreferenceManager.getDefaultSharedPreferences(this);
		
        tvRegisterUserHeading = (TextView) findViewById(R.id.tvRegisterUserHeading);
    	
        tvFieldsRequiredInfo = (TextView) findViewById(R.id.tvFieldsRequiredInfo);
        
        tvRegisterFirstNameHeading = (TextView) findViewById(R.id.tvRegisterFirstNameHeading);
    	etUpdateRegisterFirstName = (EditText) findViewById(R.id.etUpdateRegisterFirstName);
    	etUpdateRegisterFirstName.setText(settings.getString("first_name", ""));
    	
    	tvRegisterLastNameHeading = (TextView) findViewById(R.id.tvRegisterLastNameHeading);
    	etUpdateRegisterLastName = (EditText) findViewById(R.id.etUpdateRegisterLastName);
    	etUpdateRegisterLastName.setText(settings.getString("last_name", ""));
    	
    	tvRegisterAddressHeading = (TextView) findViewById(R.id.tvRegisterAddressHeading);
    	etUpdateRegisterAddress = (EditText) findViewById(R.id.etUpdateRegisterAddress);
    	etUpdateRegisterAddress.setText(settings.getString("address", ""));
    	
    	tvRegisterPostalCodeHeading = (TextView) findViewById(R.id.tvRegisterPostalCodeHeading);
    	etUpdateRegisterPostalCode = (EditText) findViewById(R.id.etUpdateRegisterPostalCode);
    	etUpdateRegisterPostalCode.setText(settings.getString("postal_code", ""));
    	
    	tvRegisterPostalAddressHeading = (TextView) findViewById(R.id.tvRegisterPostalAddressHeading);
    	etUpdateRegisterPostalAddress = (EditText) findViewById(R.id.etUpdateRegisterPostalAddress);
    	etUpdateRegisterPostalAddress.setText(settings.getString("city", ""));
    	
    	tvRegisterEmailAddress = (TextView) findViewById(R.id.tvRegisterEmailAddressHeading);
    	oldEMailAddress = settings.getString("mailaddress", "");
    	etUpdateEmailAddress = (EditText) findViewById(R.id.etUpdateEmailAddress);
    	etUpdateEmailAddress.setText(settings.getString("mailaddress", ""));
    	
    	tvRegisterPhoneHeading = (TextView) findViewById(R.id.tvRegisterPhoneHeading);
    	etUpdateRegisterPhone = (EditText) findViewById(R.id.etUpdateRegisterPhone);
    	etUpdateRegisterPhone.setText(settings.getString("home_phone", ""));
    	
    	tvRegisterMobilePhoneHeading = (TextView) findViewById(R.id.tvRegisterMobilePhoneHeading);
    	etUpdateRegisterMobilePhone = (EditText) findViewById(R.id.etUpdateRegisterMobilePhone);
    	etUpdateRegisterMobilePhone.setText(settings.getString("mobile_phone", ""));
    	
    	tvRegisterUserNameHeading = (TextView) findViewById(R.id.tvRegisterUserNameHeading);

    	String stamfordUserUsername = settings.getString("stamford_username", null);
    	etUpdateRegisterUserName = (EditText) findViewById(R.id.etUpdateRegisterUserName);
    	etUpdateRegisterUserName.setText(stamfordUserUsername);
    	// It shall not be possible to change username.
    	etUpdateRegisterUserName.setEnabled(false);
    	
    	tvOldPasswordHeading = (TextView) findViewById(R.id.tvOldPasswordHeading);
    	etOldPassword = (EditText) findViewById(R.id.etOldPassword);
    	tvRegisterUpdatePasswordHeading = (TextView) findViewById(R.id.tvRegisterUpdatePasswordHeading);
    	etUpdateRegisterPassword = (EditText) findViewById(R.id.etUpdateRegisterPassword);
    	
    	tvRegisterBirthHeading = (TextView) findViewById(R.id.tvRegisterBirthHeading);
    	etUpdateRegisterBirth = (EditText) findViewById(R.id.etRegisterBirth);
    	etUpdateRegisterBirth.setText(settings.getString("birth_year", ""));
    	
    	tvUserType = (TextView) findViewById(R.id.tvUserType);
    	
    	spUserType = (Spinner) findViewById(R.id.spUserType);
    	ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.user_type_choices, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spUserType.setAdapter(adapter);
        // TODO Update this to fetch the real value when the whole list of possible values has been implemented for user type.
        String whichSelection = settings.getString("user_type", "2");
        spUserType.setSelection(Integer.parseInt(whichSelection) -1); // Correction for offset agains Stamford values.

    	cbWantsNewsLetter = (CheckBox) findViewById(R.id.cbWantsNewsLetter);
   		cbWantsNewsLetter.setSelected(settings.getBoolean("want_newsletter", false));

    	accountUpdateButton = (Button) findViewById(R.id.accountUpdateButton);
    	accountUpdateButton.setOnClickListener(new OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		new UpdateAccountInfoAsyncTask().execute();
        	} // end onClick()
        });

    	cancelButton = (Button) findViewById(R.id.btnCancelRegistration);
    	cancelButton.setOnClickListener(new OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		cancelAccountUpdate();
        	} // end onClick()
        });
	} // end onCreate()
	

	/**
	 * Registers a user using the information supplied in the EditText fields.
	 * Connects to the server.
	 * 
	 * TODO Also update the information in the GAE server.
	 */
	private String updateUserAccount() {
		
		String result = "success";
		
		// Get information about the account from the shared preferences.
		SharedPreferences.Editor editor = settings.edit();
		String userIdCookie = settings.getString("ANV_ID_C", null);
		String stamfordUserUsername = settings.getString("stamford_username", null);
		String stamfordUserPassword = settings.getString("stamford_password", null);

		// TODO Move this into the rest client.
		List<NameValuePair> formparams = new ArrayList<NameValuePair>();
		formparams.add(new BasicNameValuePair("what", "edit_user"));
		formparams.add(new BasicNameValuePair("firstname", etUpdateRegisterFirstName.getText().toString()));
		formparams.add(new BasicNameValuePair("lastname", etUpdateRegisterLastName.getText().toString()));
		formparams.add(new BasicNameValuePair("address", etUpdateRegisterAddress.getText().toString()));
		formparams.add(new BasicNameValuePair("postalcode", etUpdateRegisterPostalCode.getText().toString()));
		formparams.add(new BasicNameValuePair("postaladdress", etUpdateRegisterPostalAddress.getText().toString()));
		formparams.add(new BasicNameValuePair("homephone", etUpdateRegisterPhone.getText().toString()));
		formparams.add(new BasicNameValuePair("mobilephone", etUpdateRegisterMobilePhone.getText().toString()));
		formparams.add(new BasicNameValuePair("email", etUpdateEmailAddress.getText().toString()));
		formparams.add(new BasicNameValuePair("tcm_obj", String.valueOf( (spUserType.getSelectedItemPosition() + 1) )));
		formparams.add(new BasicNameValuePair("oldEmail", oldEMailAddress));
		formparams.add(new BasicNameValuePair("age", etUpdateRegisterBirth.getText().toString()));
		
		if (cbWantsNewsLetter.isChecked() == true) {
    		formparams.add(new BasicNameValuePair("news", "true"));
		}
		else {
    		formparams.add(new BasicNameValuePair("news", "false"));
		}
		
		formparams.add(new BasicNameValuePair("user", stamfordUserUsername));
		String extractedUserIdCookie = userIdCookie.substring(userIdCookie.indexOf("=") + 1, userIdCookie.length());
		formparams.add(new BasicNameValuePair("userobj", extractedUserIdCookie));
		
		// Check possible updates to password. If the old password is filled in and matches the stored one the user
		// wants to update the password.
		if (!etOldPassword.getText().toString().equals("") || !etUpdateRegisterPassword.getText().toString().equals("")) {

			// Check that the old password is correct
			if (etOldPassword.getText().toString().equals(stamfordUserPassword)) { 
				
				// Check that a new password has been given.
				if (!etUpdateRegisterPassword.getText().toString().equals("")) {
		    		formparams.add(new BasicNameValuePair("password", stamfordUserPassword));
		    		formparams.add(new BasicNameValuePair("newpassword", etUpdateRegisterPassword.getText().toString()));
				}
				else {
					result = "new_password_to_short";
				}
				
			}
			else {
				result = "password_incorrect";
			}
		}
    	else {
    		formparams.add(new BasicNameValuePair("password", ""));
    		formparams.add(new BasicNameValuePair("newpassword", ""));
    	}
		
		if (result.equals("success")) {
			// If the user has succesfully update the information login with password and username for the Stamford account and
			// update the user information.
			if (restClient.updateUserAccountStamford(stamfordUserUsername, stamfordUserPassword, formparams)) {
				
				// Save the updated information to the preferences.
				// TODO Also save it using RestClient,registerUser() which uses put so nothing will be overwritten.
		   		editor.putString("first_name", etUpdateRegisterFirstName.getText().toString());
		   		editor.putString("last_name", etUpdateRegisterLastName.getText().toString());
		   		editor.putString("address", etUpdateRegisterAddress.getText().toString());
				editor.putString("postal_code", etUpdateRegisterPostalCode.getText().toString());
				editor.putString("city", etUpdateRegisterPostalAddress.getText().toString());
				editor.putString("mailaddress", etUpdateEmailAddress.getText().toString());
		        
				editor.putString("user_type", String.valueOf( (spUserType.getSelectedItemPosition() + 1) ));
				editor.putString("home_phone", etUpdateRegisterPhone.getText().toString());
				editor.putString("mobile_phone", etUpdateRegisterMobilePhone.getText().toString());
				editor.putString("birth_year", etUpdateRegisterBirth.getText().toString());
				if (cbWantsNewsLetter.isChecked() == true) {
					editor.putString("wantsnewsletter", "true");
				}
				else {
					editor.putString("wantsnewsletter", "false");
				}
				// If password was updated, also change that.
				if (!etUpdateRegisterPassword.getText().toString().equals("")) {
					editor.putString("stamford_password", etUpdateRegisterPassword.getText().toString());
				}
				editor.commit();
			}
			else {
				result = "failed";
			}
		}
		return result;
	} // end registerUser()
	
	
    /**
     * Class used to contact the server asynchronously in a separate thread from the UI-thread 
     * to download an existing account for a Stamford user.
     */
    private class UpdateAccountInfoAsyncTask extends AsyncTask<Void, Integer, String> {

    	@Override
		protected void onPreExecute() {
    		progressDialog = new ProgressDialog(ChangeAccountInfoView.this);
    		progressDialog.setTitle("Friska Karlstad");
    		progressDialog.setMessage("Uppdaterar kontoinformation");
    		progressDialog.show();
    	} // end onPreExecute()
    	
		@Override
		protected String doInBackground(Void... params) {
			return updateUserAccount();
		}
    	
        protected void onPostExecute(String result) {
        	progressDialog.cancel();
        	if (result.equals("new_password_to_short")) {
				etUpdateRegisterPassword.requestFocus();
				showDialog(NEW_PASSWORD_TOO_SHORT);
        	}
        	else if (result.equals("password_incorrect")) {
				etOldPassword.requestFocus();
        		showDialog(OLD_PASSWORD_INCORRECT);
        	}
        	else if (result.equals("success")) {
				Intent data = new Intent();
				data.putExtra("update_account_result", true);
				setResult(RESULT_OK, data);
				finish();
        	}
        	else {
        		showDialog(FAILED_TO_UPDATE_ACCOUNT);
        	}
        } // end onPostExecute()
    }
	
	// Code to show dialog windows. The different dialogs are created using an int constant
	// to identify which one to open. This function is called through showDialog(int) call
	// in code.
    @Override
    protected Dialog onCreateDialog(int id) {
        
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	
    	switch (id) {
       		case PUL_NOT_ACCEPTED:
    			builder.setMessage("Du m�ste acceptera PUL!")
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int id) {
    			        	   removeDialog(PUL_NOT_ACCEPTED);
    			           }
    			       });
    			break;
       		case NEW_PASSWORD_TOO_SHORT:
    			builder.setMessage("Nytt l�senord m�ste vara minst 4 tecken!")
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int id) {
    			        	   removeDialog(NEW_PASSWORD_TOO_SHORT);
    			           }
    			       });
    			break;
       		case OLD_PASSWORD_INCORRECT:
    			builder.setMessage("Gammalt l�senord inkorrekt eller saknas!")
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int id) {
    			        	   removeDialog(OLD_PASSWORD_INCORRECT);
    			           }
    			       });
    			break;
       		case FAILED_TO_UPDATE_ACCOUNT:
    			builder.setMessage("Misslyckades uppdatera kontoinformation!")
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int id) {
    			        	   removeDialog(FAILED_TO_UPDATE_ACCOUNT);
    			           }
    			       });
    			break;
    		default:
    			break;
        }
    	
        return builder.create();
    } // end onCreateDialog()
	
	
	/**
	 * Cancels the registration of the user by returning to the previous view.
	 */
	private void cancelAccountUpdate()
	{
		finish();
	} // end cancelRegistration()

} // end class RegisterUserView