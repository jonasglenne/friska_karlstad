package jonas.com.friskakarlstad.activities;

import java.io.FileNotFoundException;
import jonas.com.friskakarlstad.R;
import jonas.com.friskakarlstad.pojos.RegisteredMobileUser;
import jonas.com.friskakarlstad.utilities.FileHandler;
import jonas.com.friskakarlstad.utilities.RegisteredUserXmlHandler;
import jonas.com.friskakarlstad.utilities.RestClient;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginUserView extends Activity {

	/**************************************************************
	 * Result codes for different methods called within the Activity.
	 **************************************************************/
	
	// Id for a dialog showing that this account already exist.
	public static final int REPLACE_ACCOUNT_QUESTION = 1;
	
	// Id for a dialog informing the user it was impossible to login to the server.
	public static final int LOGIN_FAILED_ALERT = 2;
	
	// Id for a dialog informing the user the incorrect credentials were used.
	public static final int INCORRECT_USER_DATA_ALERT = 3;

	// id for a dialog informing the user that the network connection could not be established to the server.
	public static final int NETWORK_CONNECTION_FAILED_ALERT = 4;
	
	// Tag for progress dialog indicating download from Stamford.
	public static final int PROGRESS_DIALOG = 1;
	
	/**************************************************************
	 * Widgets for this activity.
	 **************************************************************/
	
	// Heading for the login user view.
	private TextView tvLoginUserHeading;
	
	// Text describing the alternatives to login to an existing account or
	// make a new registration at server.
	private TextView tvLoginUserName;

	// EditText where the user enters the username for the existing account.
	private EditText etLoginUserName;
	
	// A label for the user password textfield.
	private TextView tvLoginPassword;

	// EditText where the user enters the password for the existing account.
	private EditText etLoginPassword;

	// Button for executing the login for an existing account.
	private Button btnLoginUser;
	
	/**************************************************************
	 * Utility attributes for this activity.
	 **************************************************************/
	
	// Communicates via the REST-API on google app-engine.
	private RestClient restClient;
	
	// Show that download of controls from Stamford and upload to GAE are in progress.
	private ProgressDialog downloadProgress;
	
	// Holds the username for the currently installed account.
	private String username;

	// A boolean that indicates that the user has answered yes to the question in the dialog
	// if it is ok to overwrite existing account on login.
	private boolean isOverWriteOk;
	
	private SharedPreferences sharedPreferences;
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_user_view_layout);
        
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		
        restClient = new RestClient();
        
        tvLoginUserHeading = (TextView) findViewById(R.id.tvLoginUserHeading);
        tvLoginUserName = (TextView) findViewById(R.id.tvLoginUserName);
        etLoginUserName = (EditText) findViewById(R.id.etLoginUserName);
        tvLoginPassword = (TextView) findViewById(R.id.tvLoginPassword);
        etLoginPassword = (EditText) findViewById(R.id.etLoginPassword);

        if (savedInstanceState != null) {
        	isOverWriteOk =  savedInstanceState.getBoolean("user_accepted_overwrite");
            etLoginUserName.setText(savedInstanceState.getString("loginview_username"));
            etLoginPassword.setText(savedInstanceState.getString("loginview_password"));
        }
        
        btnLoginUser = (Button) findViewById(R.id.btnLoginUser);
        btnLoginUser.setOnClickListener(new OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		
        		// Check that there exists some text in text fields
        		if (!etLoginUserName.getText().toString().equals("") && 
        			!etLoginPassword.getText().toString().equals("")) {
        			new DownloadExistAccountTask().execute();
        		}
        		else {
        			showToast("Ingen användarnamn eller lösenord angivet!");
        		}
        	} // end onClick()
        });
 
        checkForExistingAccount();
	} // end onCreate()

	
	/**
	 * Checks if an account already exists on the phone (indicated by an attempt to retrieve
	 * a string value for key stamford_username in the shared preferences return non null.
	 * In this case warn the user that login will overwrite existing information locally 
	 * in the phone and replace with the information stored at Stamford.
	 */
	private void checkForExistingAccount() {

		// Get the shared preferences and check if there is an account installed already.
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		username = settings.getString("username", null);

		if (username != null && isOverWriteOk == false) {
			showDialog(REPLACE_ACCOUNT_QUESTION);
		}
	} // end checkForExistingAccount()
	
	
    /**
     * Displays a toast message on screen
     * @param toastText a String that is the text to display.
     */
    private void showToast(String toastText) {
    	int duration = Toast.LENGTH_SHORT;
    	Toast toast = Toast.makeText(this, toastText, duration);
    	toast.show();
    } // end showToast()
	
	
    /**
     * Show a progress dialog during the download of account from Stamford.
     */
	private void showProgressDialog() {
		// Start the progress dialog for download of controls from Stamford.
		downloadProgress = ProgressDialog.show(this, "Loggar in på Friska Karlstad", "Vänligen vänta...", true);
	} // end showProgressDialog()

	
    /**
     * Class used to contact the server asynchronously in a separate thread from the UI-thread 
     * to download an existing account for a Stamford user.
     */
    private class DownloadExistAccountTask extends AsyncTask<Void, Void, Boolean> {
    	
    	@Override
		protected void onPreExecute() {
    		// Prevent screen rotation (which will kill the activity) during the download of 
    		// account from the server to avoid interrupted download.
    		lockScreenRotation();
    		showProgressDialog();
        } // end onPreExecute()

        @Override
		protected Boolean doInBackground(Void... params) {
    		
        	boolean result = getUserAccountFromServer();
        	return result;
        } // end doInBackground()
		
        protected void onProgressUpdate() {}

        protected void onPostExecute(Boolean result) {
    		downloadProgress.dismiss();
    		
    		if (result == true) {
    			Intent data = new Intent();
				data.putExtra("login_register_result", true);
				setResult(RESULT_OK, data);
				finish();
    		}
    		else {
    			showDialog(LOGIN_FAILED_ALERT);
    		}
    		// Restore the posibility to rotate the screen after the download of the account has finished.
    		unlockScreenRotation();
        } // end onPostExecute()
        
    } // end class StoreToServerTask
	
    
    /**
     * Retrieves a user account from the server together with project information
     * for the project the user is participating in.
     * 
     * @return a boolean indicating the result of the retrieval. True if all is ok, False if something failed.
     */
    private boolean getUserAccountFromServer() {
    	boolean result = false;
    	
    	// TODO: Make a method to check valid response in parser.
    	// Retrieve the user object from the server. Parse the response from the server and 
    	// assign to the registered user object to the created data.
    	String serverXmlData= restClient.getRegisteredUser(etLoginUserName.getText().toString(), etLoginPassword.getText().toString());
    	
    	RegisteredUserXmlHandler xmlParser = new RegisteredUserXmlHandler();
    	RegisteredMobileUser downloadedUser  = xmlParser.parseXml(serverXmlData);

    	// Write the newly downloaded user to disc.
    	if (downloadedUser != null) {
    		
    		Editor prefsEditor = sharedPreferences.edit();
    		prefsEditor.putString("username", etLoginUserName.getText().toString());
   			prefsEditor.putString("password", etLoginPassword.getText().toString());
   			prefsEditor.putInt("controls_taken", downloadedUser.getControls().size());
   			prefsEditor.putInt("total_nr_controls", downloadedUser.getControlInfos().size());
   			prefsEditor.commit();
   			
    		try {
				FileHandler.writeObjectToFile(downloadedUser, 
						openFileOutput(FileHandler.REGISTERED_USER_FILENAME, Context.MODE_PRIVATE));
			} 
			catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			result = true;
    	}
    	
    	return result;
    } // end getUserAccountFromServer()
    
    
	/**
	 *  Code to show dialog windows. The different dialogs are created using an int constant
	 * to identify which one to open. This function is called through showDialog(int) call
	 * in code.
	 */
    @Override
    protected Dialog onCreateDialog(int id) {
        
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	
    	switch (id) {
       		case REPLACE_ACCOUNT_QUESTION:
    			builder.setMessage("Observera att ett konto redan används på telefonen med användarnamn: " + 
       		                       username + ". Om du loggar in på ett annat konto kommer det nuvarande " +
    					           "att ersättas med det nya kontot i mobilen.")
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int id) {
    			        	   isOverWriteOk = true;
    			        	   dialog.cancel();
    			        	   dialog = null;
    			           }
    			       })
    			.setNegativeButton("Avbryt", new DialogInterface.OnClickListener() {
            	           public void onClick(DialogInterface dialog, int id) {
            	        	   dialog.cancel();
    			        	   finish();
            	           }
            	       });
    			break;
       		case LOGIN_FAILED_ALERT:
    			builder.setMessage("Misslyckades logga in på konto.")
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int id) {
    			        	   removeDialog(LOGIN_FAILED_ALERT);
    			           }
    			       });
    			break;
       		case INCORRECT_USER_DATA_ALERT:
    			builder.setMessage("Användarnamn/Lösenord felaktigt eller existerar inte.")
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int id) {
    			        	   removeDialog(INCORRECT_USER_DATA_ALERT);
    			           }
    			       });
    			break;
       		case NETWORK_CONNECTION_FAILED_ALERT:
    			builder.setMessage("Kunde inte kontakta servern.")
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int id) {
    			        	   removeDialog(NETWORK_CONNECTION_FAILED_ALERT);
    			           }
    			       });
    			break;
    		default:
                break;
        }
    	
        return builder.create();
    } // end onCreateDialog()
    
    
    protected void onSaveInstanceState(Bundle savedInstanceState) {
    	savedInstanceState.putString("loginview_username", etLoginUserName.getText().toString());
    	savedInstanceState.putString("loginview_password", etLoginPassword.getText().toString());
    	savedInstanceState.putBoolean("user_accepted_overwrite", isOverWriteOk);
    	removeDialog(REPLACE_ACCOUNT_QUESTION); // We do not want the dialog to reappear on screen orientation change.
    	super.onSaveInstanceState(savedInstanceState);
   	} // end onSaveInstanceState()
    
    
    /**
     * Locks any screen orientation changes. 
     */
    private void lockScreenRotation() {
      // Stop the screen orientation changing during an event
        switch (this.getResources().getConfiguration().orientation) {
	      case Configuration.ORIENTATION_PORTRAIT:
	        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	        break;
	      case Configuration.ORIENTATION_LANDSCAPE:
	        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	        break;
        }
    } // end lockScreenRotation()
    
    
    /**
     * Unlocks the screen orientation change ability. 
     */
    private void unlockScreenRotation() {
    	this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    } // end unlockScreenRotation()
    
} // end class LoginUserView