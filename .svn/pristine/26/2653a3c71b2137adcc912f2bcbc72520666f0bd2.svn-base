package jonas.com.friskakarlstad;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class LoginOrRegisterView extends Activity {

	// Tag for Log printouts.
	public static final String CLASSNAME = "LoginOrRegisterView";
	
	// Activity request code for the login and synchronization at Stamford.
	private static int RESULT_FOR_STAMFORD_SYNCH = 1;
	
	// Activity request code to be used in onActivityResult;
	public static int CHANGE_SETTINGS_REQ = 2;
	
	// Activity request code for creating a new account.
	public static int RESULT_FOR_NEW_ACCOUNT = 3;
	
	// Dialog id tag for a successful creation of a new user.
	static final int USER_CREATED_SUCCESSFULLY = 4;
	
	// Activity request code for updating of information for account on Friska Karlstad.
	static final int RESULT_FOR_ACCOUNT_UPDATE = 5;
	
	// Dialog id for a dialog telling the user an account update was successful.
	static final int ACCOUNT_UPDATED_SUCCESSFULLY = 6;
	
	// Dialog id for a dialog telling the user a login to existing account succeeded.
	static final int LOGIN_SUCCESSFULLY = 7;
	
	// Heading for the login or register view.
	private TextView tvLoginOrRegisterHeading;
	
	private TextView tvLoginExistingAccountHeading;
	private TextView tvLoginExistingAccountText;
	// Button for selecting login in at an existing account.
	private Button btnLoginExistingUser;
	
	private TextView tvNewAccountHeading;
	private TextView tvNewAccountText;
	//Button for selecting registering a new user.
	private Button btnNewAccount;
	
	private TextView tvUpdateAccountHeading;
	private TextView tvUpdateAccountText;
	//Button for selecting registering a new user.
	private Button btnUpdateExistingAccount;
	
	// Text describing the alternatives to login to an existing account or
	// make a new registration at server.
	private TextView tvLoginOrRegisterText;

	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_or_register_view_layout);
        
        tvLoginOrRegisterHeading = (TextView) findViewById(R.id.tvLoginOrRegisterHeading);

        tvLoginExistingAccountHeading = (TextView) findViewById(R.id.tvLoginExistingAccountHeading);
        tvLoginExistingAccountText = (TextView) findViewById(R.id.tvLoginExistingAccountText);
        btnLoginExistingUser = (Button) findViewById(R.id.btnLoginExistingUser);
        btnLoginExistingUser.setOnClickListener(new OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		openLoginExistingAccountView();
        	} // end onClick()
        });
        
        tvNewAccountHeading = (TextView) findViewById(R.id.tvNewAccountHeading);
        tvNewAccountText = (TextView) findViewById(R.id.tvNewAccountText);
        btnNewAccount = (Button) findViewById(R.id.btnNewAccount);
        btnNewAccount.setOnClickListener(new OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		openRegisterNewUserView();
        	} // end onClick()
        });
        
        tvUpdateAccountHeading = (TextView) findViewById(R.id.tvUpdateAccountHeading);
        tvUpdateAccountText = (TextView) findViewById(R.id.tvUpdateAccountText);
        btnUpdateExistingAccount = (Button) findViewById(R.id.btnUpdateExistingAccount);
        btnUpdateExistingAccount.setOnClickListener(new OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		openChangeAccountInfo();
        	} // end onClick()
        });
	} // end onCreate()


	private void openChangeAccountInfo() {
		startActivityForResult(new Intent(this, ChangeAccountInfoView.class), RESULT_FOR_ACCOUNT_UPDATE);
	} // end openChangeAccountInfo()
	
	
	/**
	 * Opens a view where the user can register a new account on Friska Karlstad.
	 */
	private void openRegisterNewUserView() {
		startActivityForResult(new Intent(this, RegisterUserView.class), RESULT_FOR_NEW_ACCOUNT);
	} // end openRegisterNewUserView()
	
	
	/**
	 * Opens a view where the user can login to an existing Friska Karlstad account.
	 */
	private void openLoginExistingAccountView() {
		startActivityForResult(new Intent(this, LoginUserView.class), RESULT_FOR_STAMFORD_SYNCH);
	} // end openLoginView()

	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == -1) {
			if (requestCode == RESULT_FOR_STAMFORD_SYNCH) {
				Bundle b = data.getExtras();
				boolean loginAndSynchResult = b.getBoolean("login_register_result");
				
				if (loginAndSynchResult == true) {
					showDialog(LOGIN_SUCCESSFULLY);
				}
			}
			if (requestCode == RESULT_FOR_NEW_ACCOUNT) {
				Bundle b = data.getExtras();
				boolean newAccountResult = b.getBoolean("new_register_result");
				
				if (newAccountResult) {
					showDialog(USER_CREATED_SUCCESSFULLY);
				}
			}
			if (requestCode == RESULT_FOR_ACCOUNT_UPDATE) {
				Bundle b = data.getExtras();
				boolean newAccountResult = b.getBoolean("update_account_result");
				
				if (newAccountResult) {
					showDialog(ACCOUNT_UPDATED_SUCCESSFULLY);
				}
			}
		}
		else {
			Log.e(CLASSNAME, "Unknown requestCode returned to onActivityResult");
		}
	} // end onActivityResult()
	
	
	// Code to show dialog windows. The different dialogs are created using an int constant
	// to identify which one to open. This function is called through showDialog(int) call
	// in code.
    @Override
    protected Dialog onCreateDialog(int id) {
        
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	
    	switch (id) {
			case USER_CREATED_SUCCESSFULLY:
			builder.setMessage("Du �r nu registrerad.")
			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			        	   removeDialog(USER_CREATED_SUCCESSFULLY);
			           }
			       });
			break; 
			case ACCOUNT_UPDATED_SUCCESSFULLY:
				builder.setMessage("Kontoinformationen �r nu uppdaterad.")
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				           public void onClick(DialogInterface dialog, int id) {
				        	   removeDialog(ACCOUNT_UPDATED_SUCCESSFULLY);
				           }
				       });
				break;
			case LOGIN_SUCCESSFULLY:
				builder.setMessage("Inloggning lyckades! Du kan nu b�rja registrera kontroller.")
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				           public void onClick(DialogInterface dialog, int id) {
				        	   removeDialog(LOGIN_SUCCESSFULLY);
				           }
				       });
				break;
			default:
        }
    	
        return builder.create();
    } // end onCreateDialog()
	
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.login_or_register_options_menu, menu);
        return true;
    } // end onCreateOptionsMenu()

	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
    		case R.id.options_settings:
    			startActivityForResult(new Intent(this, EditPreferences.class), CHANGE_SETTINGS_REQ);
    			return true;
        	case R.id.login_or_register_options_help:
    			Intent browserIntent = new Intent(this, InformationView.class);
    			browserIntent.putExtra("browser_url", "file:///android_asset/account_menu_help.html");
    			startActivity(browserIntent);
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
        }
    } // end onOptionsItemSelected()
	
} // end class LoginOrRegisterView