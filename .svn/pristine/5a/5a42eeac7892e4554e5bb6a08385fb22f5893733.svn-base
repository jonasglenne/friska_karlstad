package jonas.com.friskakarlstad.utilities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import jonas.com.friskakarlstad.activities.MainMenuView;
import jonas.com.friskakarlstad.pojos.Control;


import android.os.Environment;
import android.util.Log;

public class ControlFileHandler {

	private static final String CLASSNAME = "ControlFileHandler";
	
	public static final String CONTROLLISTFILENAME = "control_list";
	
	
	/**
	 * Takes an FileInputStream and reads it into an ArrayList of Control objects that
	 * is returned to the calling class in sorted order (sort key depends on object).
	 * 
	 * @param inStream a FileInputStream that is the received data from a server.
	 * @return a ArrayList<Control> that is a list of controls.
	 */
	@SuppressWarnings("unchecked")
	public static synchronized ArrayList<Control> getLocalControlFileList(FileInputStream inStream) {

		ObjectInputStream objIn = null;
		ArrayList<Control> tmpControlList = null;
			
		// Try to open the locally stored file. If it is the first time the program is started
		// this will fail since the file has not been created yet. In that case simply save all
		// controls that have been downloaded to the file.
		try {
			objIn = new ObjectInputStream(inStream);
			
			if (objIn != null) {
				tmpControlList = (ArrayList<Control>)objIn.readObject();
				
				// Always sort the control list before returning it.
				if (tmpControlList != null) {
					Collections.sort(tmpControlList);
				}
			}
			
		}
		catch (IOException e) {
			Log.e(MainMenuView.CLASSNAME, "Something went wrong when reading the list of controls to file.");
			e.printStackTrace();
		} 
		catch (ClassNotFoundException e) {
			Log.e(MainMenuView.CLASSNAME, "Could not find the file containing the controls.");
			e.printStackTrace();
		}
		finally {
			if (objIn != null) {
				try {
					objIn.close();
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return tmpControlList;
	} // end getLocalControlFileList()

	
	/**
	 * Takes an FileOutputStream and writes to an ObjectOutputStream which is saved in the internal storage
	 * of the application.
	 * 
	 * @param theControlList an ArrayList<Control> object which shall be written to internal storage.
	 * @param outStream an FileOutputStream used to write the control list to internal storage.
	 */
	public static synchronized void writeLocalControlFile(List<Control> theControlList, FileOutputStream outStream) {
		Log.i(CLASSNAME, "Writing the local control list to file.");
		
		ObjectOutputStream objOut = null;
		
		try {
			objOut = new ObjectOutputStream(outStream);

			// If the local list is null, it means that it is the first time the list has been downloaded
			// from the server. In that case write the complete list from the server to internal storage.
			// Otherwise, save the update local control list.
			if (theControlList != null) {
				objOut.writeObject(theControlList);
			}
		}
		catch (IOException e) {
			Log.e(MainMenuView.CLASSNAME, "Something went wrong when writing the list of controls to file.");
			e.printStackTrace();
		}
		finally {
			if (objOut != null) {
				try {
					objOut.close();
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	} // end writeLocalControlFile()
	
	/**
	 * Checks if everything if the external media is writeable
	 */
	public static String checkExternalStorageAvailability() {
		
		String sdCardState = "";
		
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_UNMOUNTED)) {
			Log.w(CLASSNAME, "Memory card not mounted");
			sdCardState = Environment.MEDIA_UNMOUNTED;
		}
		else if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED_READ_ONLY)) {
			Log.w(CLASSNAME, "Memory card not writeable");
			sdCardState = Environment.MEDIA_MOUNTED_READ_ONLY;
	    }
		else if (Environment.getExternalStorageState().equals(Environment.MEDIA_SHARED)) {
			Log.w(CLASSNAME, "Memory card shared via USB");
			sdCardState = Environment.MEDIA_SHARED;
	    } 
		else {
	    	Log.w(CLASSNAME, "No problems accessing SD card");
			sdCardState = Environment.MEDIA_MOUNTED;
	    }
		return sdCardState;
	} // end checkExternalStorageAvailability()
	
} // end class ControlFileHandler
