package jonas.com.friskakarlstad.utilities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import jonas.com.friskakarlstad.pojos.Control;

import org.apache.http.Header;
import org.apache.http.HeaderIterator;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeaderIterator;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;


import android.util.Log;

public class RestClient {
	
	// The classname used as a tag for Log printouts.
	public static final String CLASSNAME = "RestClient";

	// http://privat.bahnhof.se/wb739361/GAETiles.zip
	// The location for the map tiles.
	final static String MAP_TILES_HTTP_LOCATION = "https://docs.google.com/uc?id=0BytBBI0ugXU-MDE2ODAzZDktMmM2OS00MjliLTllNGYtYmE2YWRiNmIwZTVm&export=download&authkey=CLXt3MkE&hl=sv";

	// The base directory for the map tiles.
	public final static String DATA_STORAGE_BASE = "/data/friska_karlstad/";

	// The GAE host.
	private HttpHost targetHostGAE;

	// The Stamford host.
	private HttpHost targetHostStamford;
	
	// The http client for this REST client class.
	DefaultHttpClient httpClient;
	
	// The expected number of cookies to be set by the Stamford server.
	private static int NR_OF_COOKIES = 2;
	
	
	/**
	 * Constructor for class RestClient
	 */
	public RestClient() {
		targetHostGAE = new HttpHost("checkpointserver.appspot.com", 443, "https");
	} // end constructor RestClient()

	
	/**
	 * Executes a POST request and returns the respone
	 * 
	 * @param postURI a String that is the URI for the request
	 * @param xmlBody a String that is the XML body for the request
	 * @return a HttpResponse containing the results from the request
	 */
	private HttpResponse executeGAEPostRequest(String postURI, String xmlBody) {

		printMessageToSend(xmlBody.toString());

		httpClient = new DefaultHttpClient();

		HttpPost httpPost = new HttpPost(postURI);
    	httpPost.addHeader("Accept", "text/html");
    	httpPost.addHeader("Content-Type", "application/xml");
    	HttpResponse response = null;
    	
    	try {
    		StringEntity entity = new StringEntity(xmlBody, "UTF-8");
	    	entity.setContentType("application/xml");
	    	httpPost.setEntity(entity);
	    	
	    	response = httpClient.execute(targetHostGAE, httpPost);
			//Log.i(CLASSNAME, "Statusline: " + response.getStatusLine());
			//Log.i(CLASSNAME, "Reasonphrase: " + response.getStatusLine().getReasonPhrase());
    	}
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}

    	return response;
	} // end executeGAERequest()
	
	
	/**
	 * Queries the GAE server for the full list of controls and returns it to the calling
	 * class.
	 * 
	 * @return an ArrayList of Control objects containing all controls stored on the server.
	 */
	public ArrayList<Control> getControlList() {
		Log.i(CLASSNAME, "getControlList()");

    	String xmlMessage = "<controlInfos><controlInfo><controlId>999</controlId><code>QKhM4q9qmQlXKKHZbhAz</code><description/><latitude>0.0</latitude><longitude>0.0</longitude><imageLink></imageLink><webLink></webLink><color></color></controlInfo></controlInfos>";
    	
    	ArrayList<Control> tmpControlList = null;
    	try {
    		HttpResponse response = executeGAEPostRequest("/controlInfos", xmlMessage);
        	HttpEntity responseEntity = response.getEntity();
            tmpControlList = parseData(responseEntity);
    	}
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	httpClient.getConnectionManager().shutdown();

    	return tmpControlList;
	} // end getControlList()
	
	
	/**
	 * Registers a control to the GAE server with a control object as argument.
	 * 
	 * @param theControlToStore a Control that is the control to register.
	 * @param userId a String that is the id of the user which sends the control for registration
	 * @return a boolean that indicates the result of the registration
	 */
	public boolean registerControl(Control theControlToRegister, String userId) {
		Log.i(CLASSNAME, "Register control at GAE for user...");

		boolean result = false;
		
		StringBuilder xmlBody = new StringBuilder();
		xmlBody.append("<controls>");
		xmlBody.append("<control>");
		xmlBody.append("<controlId>" + theControlToRegister.getControlId() + "</controlId>");
		xmlBody.append("<controlCode>" + theControlToRegister.getCodeName().toUpperCase() + "</controlCode>");
		xmlBody.append("<timeStamp>" + theControlToRegister.getTimeStamp() + "</timeStamp>");
		xmlBody.append("</control>");
		xmlBody.append("</controls>");
    	
    	try {
	    	HttpResponse response = executeGAEPostRequest("/user/" + userId + "/controls", xmlBody.toString());
	    	
    		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED) {
    			Log.i(CLASSNAME, "Register control at GAE for user...success");
    			result = true;
    		}
	    	else {
	    		Log.e(CLASSNAME, "Register control at GAE for user...failed");
    			result = false;
	    	}
    	}
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	httpClient.getConnectionManager().shutdown();
    	
    	return result;
	} // end registerControl()

	
	/**
	 * Registers multiple controls to the GAE server at once with a list of control objects as argument.
	 * 
	 * @param theControlsToStore a List of control that is the control to register.
	 * @param userId a String that is the id of the user which sends the control for registration
	 * @return a boolean that indicates the result of the registration
	 */
	public boolean registerControlsAtGAE(List<Control> theControlsToStore, String userId) {

		boolean result = false;
		
		StringBuilder xmlBody = new StringBuilder();
		xmlBody.append("<controls>");
		
		for (Control currentControl : theControlsToStore) {
			xmlBody.append("<control>");
			xmlBody.append("<controlId>" + currentControl.getControlId() + "</controlId>");
			xmlBody.append("<controlCode>" + currentControl.getCodeName().toUpperCase() + "</controlCode>");
			xmlBody.append("<timeStamp>" + currentControl.getTimeStamp() + "</timeStamp>");
			xmlBody.append("</control>");
		}
		xmlBody.append("</controls>");

    	try {
    		HttpResponse response = executeGAEPostRequest("/user/" + userId + "/controls", xmlBody.toString());
     	
    		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED) {
    			result = true;
    		}
	    	else {
    			result = false;
	    	}
    	}
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	httpClient.getConnectionManager().shutdown();

    	return result;
	} // end registerControls()
	
	
	// TODO Change the return value to a String which will be added to a toast to the calling View.
	/**
	 * Logs in a user to an existing account on the Stamford server.
	 * 
	 * @param userId a String that is the user id for a user.
	 * @param loginXMLBody a String that is the XML data sent to the rest server.
	 * @return a boolean indicating the result of the operation.
	 */
	public boolean loginUser(String userId, String loginXMLBody) {
		Log.i(CLASSNAME, "loginUser(): Logging in user at the GAE server.");
		boolean result = false;
		
		httpClient = new DefaultHttpClient();

    	HttpPut httpPut = new HttpPut("/synchUser/" + userId);
    	httpPut.addHeader("Accept", "text/html");
    	httpPut.addHeader("Content-Type", "application/xml");
    	try {
    		StringEntity entity = new StringEntity(loginXMLBody, "UTF-8");
	    	entity.setContentType("application/xml");
	    	httpPut.setEntity(entity);
	    	
	    	HttpResponse response = httpClient.execute(targetHostGAE, httpPut);

			Log.i(CLASSNAME, "Statusline: " + response.getStatusLine());
			Log.i(CLASSNAME, "Reasonphrase: " + response.getEntity());

	    	// TODO Set better return codes depending on if user exists, could not be created or is created.
    		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED) {
    			result = true;
    		}
	    	else if (response.getStatusLine().getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
    			Log.i(CLASSNAME, "Statusline: " + response.getStatusLine());
    			Log.i(CLASSNAME, "Reasonphrase: " + response.getStatusLine().getReasonPhrase());
    			result = false;
	    	}
	    	else {
    			Log.i(CLASSNAME, "Statusline: " + response.getStatusLine());
    			Log.i(CLASSNAME, "Reasonphrase: " + response.getStatusLine().getReasonPhrase());
    			result = false;
	    	}
    	}
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	httpClient.getConnectionManager().shutdown();

    	return result;
	} // end sendXMLToServer()
	
	
	/**
	 * Registers a user at the GAE server.
	 * 
	 * @param userId a String that is the device id for the phone and serves as the unique id on GAE for this user.
	 * @param xmlMessage a String that is XML-body for the entity to send to the server
	 * @return a boolean that is the result of the registration of the user
	 */
	public boolean registerUser(String userId, String xmlMessage) {
		Log.i(CLASSNAME, "registerUser(): Registering user at the GAE server.");
		
		// Always assume the worst...
		boolean result = false;
		
		httpClient = new DefaultHttpClient();
		Log.i(CLASSNAME, "Create user at GAE...");
		Log.i(CLASSNAME, xmlMessage);

    	HttpPut httpPut = new HttpPut("/user/" + userId);
    	httpPut.addHeader("Accept", "text/html");
    	httpPut.addHeader("Content-Type", "application/xml");
    	try {
    		StringEntity entity = new StringEntity(xmlMessage, "UTF-8");
	    	entity.setContentType("application/xml");
	    	httpPut.setEntity(entity);
	    	
	    	HttpResponse response = httpClient.execute(targetHostGAE, httpPut);
	    	
	    	// If the return code is 201 then there was no previous account at gae.
	    	// If it is 200 then it means the user has already registered once but is 
	    	// now updating. Both are ok.
    		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED ||
    				response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
	    		Log.i(CLASSNAME, "Create user at GAE...Succeeded");
    			result = true;
    		}
	    	else {
	    		Log.i(CLASSNAME, "Create user at GAE...Failed");
    			result = false;
	    	}
			Log.i(CLASSNAME, "Statusline: " + response.getStatusLine());
			Log.i(CLASSNAME, "Reasonphrase: " + EntityUtils.toString(response.getEntity()));
    	}
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	httpClient.getConnectionManager().shutdown();

    	return result;
	} // end sendXMLToServer()
	
	
	/**
	 * Downloads the map tiles zip file from the web.
	 * 
	 * @return a HttpEntity containing the stream to the zip file.
	 */
	public HttpEntity downloadMapTiles() {
		
		HttpResponse response = null;
	    HttpEntity zipfileEntity = null;

		httpClient = new DefaultHttpClient();

	    // Get the map tiles zip-file from the server.
	    HttpGet httpGet = new HttpGet(MAP_TILES_HTTP_LOCATION);
	    try {
			response = httpClient.execute(httpGet);
			zipfileEntity = response.getEntity();
		} 
	    catch (ClientProtocolException e) {
			e.printStackTrace();
		} 
	    catch (IOException e) {
			e.printStackTrace();
		}

		return zipfileEntity;
	} // end downloadMapTiles()
	
	
    /**
     * Parses data received over HTTP and converts it to a list of control objects.
     * 
     * @param entity a HttpEntity that contains transport data.
     * 
     * @return an ArrayList<Control> that contains a list of objects extracted from the entity.
     */
    private ArrayList<Control> parseData(HttpEntity entity) {
    	ArrayList<Control> tmpControlList = null;
    	
        // Parse the XML-data in the response using a SAXParser.
        SAXParserFactory spf = SAXParserFactory.newInstance();
        SAXParser sp = null;
        XMLReader xr = null;

        try {
			sp = spf.newSAXParser();
			xr = sp.getXMLReader();
			RESTHandler restHandler = new RESTHandler();
			xr.setContentHandler(restHandler);
			xr.parse(new InputSource(entity.getContent()));

			tmpControlList = restHandler.getParsedData();
		}
		catch (ParserConfigurationException e1) {e1.printStackTrace();}
		catch (SAXException e) {e.printStackTrace();}
		catch(IOException e) {e.printStackTrace();}
		catch (IllegalStateException e2) {e2.printStackTrace();}
		
		try {
			entity.consumeContent();
		} 
		catch (IOException e) {e.printStackTrace();}
	
		return tmpControlList;
    } // end parseData()
    
    
    /**
     * Prints the message to send.
     * @param xmlString a String that is an xml string to send to a rest server.
     */
    private void printMessageToSend (String xmlString) {
    	Log.i(CLASSNAME, "What are we sending: " + xmlString);
    } // end printMessageToSend
	
    
    public String getUnregisteredUser() {
    	
    	httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost("/getmobileobjectunregistered");
    	httpPost.addHeader("Accept", "text/html");
    	httpPost.addHeader("Content-Type", "application/xml");
    	HttpResponse response = null;
    	String serverResponse = null;
    	
    	// TODO: Move this to mainmenu and make the string a string in the resources.
    	String xmlBody = "<credentials><userName></userName><password></password><sessionId></sessionId><projectId></projectId><argument1>Friska Karlstad 2012</argument1><argument2></argument2></credentials>";
    	try {
    		StringEntity entity = new StringEntity(xmlBody, "UTF-8");
	    	entity.setContentType("application/xml");
	    	httpPost.setEntity(entity);
	    	
	    	response = httpClient.execute(targetHostGAE, httpPost);
			//Log.i(CLASSNAME, "Statusline: " + response.getStatusLine());
			//Log.i(CLASSNAME, "Reasonphrase: " + response.getStatusLine().getReasonPhrase());
	    	serverResponse = EntityUtils.toString(response.getEntity());
	    }
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}

    	
    	return serverResponse;
    } // end getUnregisteredUser()

    /************************************************************************************************************
     * The following part are methods specific to Stamford server. If software is exported with a another 
     * database solution (e.g. only GAE), the following methods can be removed and the remaining code should 
     * be adjusted to reflect this.
     *************************************************************************************************************/
    
    
    /**
     * Login the user with the given username and password to Stamford server. The cookie in 
     * the server response is saved in the preferences to be used on successive 
     * requests.
     * 
     * @param userName a String that is the username for the user to login at Stamford.
     * @param passWord a String that is the password for the user to login at Stamford.
     * 
     * @return a String array containing the "Set-cookie" that where returned.
     */
	public String[] loginUserAtStamford(String userName, String password) {
		Log.i(CLASSNAME, "Login user at Stamford.");

		String[] cookieString = new String[NR_OF_COOKIES];
		
		httpClient = new DefaultHttpClient();

		HttpPost httpPost = new HttpPost("/scripts101c/cgiip.exe/WService=vhockey/oktyr/login.r");

		try {
    		List<NameValuePair> formparams = new ArrayList<NameValuePair>();
    		formparams.add(new BasicNameValuePair("btLogin", " Logga In "));
    		formparams.add(new BasicNameValuePair("user", userName));
    		formparams.add(new BasicNameValuePair("password", password));
    		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, HTTP.ISO_8859_1);
    		entity.setContentType("application/x-www-form-urlencoded");
	    	httpPost.setEntity(entity);

	    	HttpResponse response = httpClient.execute(targetHostStamford, httpPost);
	    	
	    	HeaderIterator iterator = new BasicHeaderIterator(response.getAllHeaders(), "Set-Cookie");
	    	
	    	int index = 0;
	    	while (iterator.hasNext()) {
	    		String cookieHeader = ((Header)iterator.next()).getValue();
	    		cookieString[index++] = cookieHeader.substring(0, cookieHeader.indexOf(";"));
	    	}
	    }
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}

        httpClient.getConnectionManager().shutdown();
        
        return cookieString;
	} // end loginUserAtStamford()
	
	
	/**
	 * Retrieves the information from the registration fields from Stamford server for a user.
	 * 
	 * @param userIdCookie a String that is the cookie for the user id at the Stamford server.
	 * @param userIdCrcCookie a String that is a cookie checksum for the user.
	 * @return a HashMap that contains the values in the registration page for a user.
	 */
	public HashMap<String,String> getUserInformationFromStamford(String userIdCookie, String userIdCrcCookie) {
		Log.i(CLASSNAME, "Retrieving user information from Stamford");
		
		HashMap<String,String> userInformation = new HashMap<String, String>();
		
		httpClient = new DefaultHttpClient();

    	try {
    		// Setup the POST request from the Stamford server.
    		HttpPost httpPost = new HttpPost("/scripts101c/cgiip.exe/WService=vhockey/oktyr/oktyr_ajax.r");
    		httpPost.setHeader("Cookie", userIdCookie + "; " + userIdCrcCookie);

       		List<NameValuePair> formparams = new ArrayList<NameValuePair>();
    		formparams.add(new BasicNameValuePair("what", "get_user"));
    		String anvCId = userIdCookie.substring(userIdCookie.indexOf("=") + 1, userIdCookie.length());
    		formparams.add(new BasicNameValuePair("userobj", anvCId));
    		String anvCRC = userIdCrcCookie.substring(userIdCrcCookie.indexOf("=") + 1, userIdCrcCookie.length());
    		formparams.add(new BasicNameValuePair("crc", anvCRC));
    		formparams.add(new BasicNameValuePair("rnd", "2240740"));
    		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, HTTP.ISO_8859_1);
    		entity.setContentType("application/x-www-form-urlencoded");
    		
	    	httpPost.setEntity(entity);
	    	
	    	HttpResponse response = httpClient.execute(targetHostStamford, httpPost);

	    	// Compile a HashMap of the information retrieved from the server.
	    	String serverResponse = EntityUtils.toString(response.getEntity());
	    	
	    	String[] userInfo = serverResponse.split("\\|�\\|");
	    	
	    	// No need to store username and password since these were stored in
	    	// the preferences when the user logged in to Stamford.
	    	userInformation.put("firstname", userInfo[0]);
	    	userInformation.put("lastname", userInfo[1]);
	    	userInformation.put("address", userInfo[2]);
	    	// Remove white space in postal code
	    	userInfo[3] = userInfo[3].replace(" ", "");
	    	userInformation.put("postalcode", userInfo[3]);
	    	userInformation.put("city", userInfo[4]);
	    	userInformation.put("homephone", userInfo[5]);
	    	userInformation.put("mobilephone", userInfo[6]);
	    	userInformation.put("mailaddress", userInfo[7]);
	    	userInformation.put("birtyear", userInfo[8]);
	    	userInformation.put("typeofperson", userInfo[9]);
	    	if (userInfo[10].equalsIgnoreCase("no")) {
		    	userInformation.put("wantsnewsletter", "false");
	    	}
	    	else {
		    	userInformation.put("wantsnewsletter", "true");
	    	}
	    	userInformation.put("username", userInfo[11]);
	    }
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	httpClient.getConnectionManager().shutdown();

		return userInformation;
	} // end sgetUserInformationFromStamford()
	
	
	/**
	 * Gets the html containing the current controls status from the Stamford server.
	 * 
	 * @param userIdCookie a String that is the cookie for the user id at the Stamford server.
	 * @param userIdCrcCookie a String that is a cookie checksum for the user.
	 * @return a String that is html code for the controls for the user
	 */
	public String getControlsFromStamford(String userIdCookie, String userIdCrcCookie) {
		Log.i(CLASSNAME, "getControlsFromStamford()");

		httpClient = new DefaultHttpClient();

		// String to hold the returned html body in the GET response from Stamford server.
		String controlsInHtmlForm = null;

    	try {
    		// Setup the GET request from the Stamford server.
    		HttpGet httpGet = new HttpGet("/scripts101c/cgiip.exe/WService=vhockey/oktyr/reg_kontr.r?mapid=1");
    		httpGet.setHeader("Content-Type", "text/html");
    		httpGet.setHeader("Cookie", userIdCookie + "; " + userIdCrcCookie);
	    	
	    	HttpResponse response = httpClient.execute(targetHostStamford, httpGet);
	    	controlsInHtmlForm = EntityUtils.toString(response.getEntity());
	    	
	    	Log.i(CLASSNAME, "This is the response: " + response.getStatusLine());
	    	//Log.i(CLASSNAME, "This is the response content: " + controlsInHtmlForm);
	    }
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	
        httpClient.getConnectionManager().shutdown();
		
		return controlsInHtmlForm;
	} // end getControlsFromStamford()
	
    
	/**
	 * Registers a control with the given controlId and control code at the Stamford server for the user.
	 * 
	 * @param controlId a String that is the control id for a control
	 * @param controlCode a String that is the control code for a control
	 * @return a boolean that indicates if the registration was successful
	 */
	public boolean registerControlAtStamford(String controlId, String controlCode, String userName, String password) {
		boolean result = false;
		
		// Login to get the session cookies.
		String[] cookieStrings = loginUserAtStamford(userName, password);
		
		httpClient = new DefaultHttpClient();
		
		// Setup the target host to send the xml data to.
		HttpPost httpPost = new HttpPost("/scripts101c/cgiip.exe/WService=vhockey/oktyr/oktyr_ajax.r");
		httpPost.setHeader("Cookie", cookieStrings[0] + "; " + cookieStrings[1]);

		try {
    		List<NameValuePair> formparams = new ArrayList<NameValuePair>();
    		formparams.add(new BasicNameValuePair("what", "save_control"));
    		formparams.add(new BasicNameValuePair("mapId", "1"));
    		formparams.add(new BasicNameValuePair("year", "2011"));
    		formparams.add(new BasicNameValuePair("ctrl", controlId));
    		formparams.add(new BasicNameValuePair("code", controlCode));
    		String anvCId = cookieStrings[0].substring(cookieStrings[0].indexOf("=") + 1, cookieStrings[0].length());
    		formparams.add(new BasicNameValuePair("userobj", anvCId));
    		String anvCRC = cookieStrings[1].substring(cookieStrings[1].indexOf("=") + 1, cookieStrings[1].length());
    		formparams.add(new BasicNameValuePair("crc", anvCRC));
    		formparams.add(new BasicNameValuePair("rnd", "3783367"));

    		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, HTTP.ISO_8859_1);
    		entity.setContentType("application/x-www-form-urlencoded");
	    	httpPost.setEntity(entity);
	    	
	    	HttpResponse response = httpClient.execute(targetHostStamford, httpPost);
	    	String responseString = EntityUtils.toString(response.getEntity());
	    	
			// Note! The pattern has to be changed if stamford changes the response code. 
	    	// Right now the server responds "Grattis! Koden �r korrekt.|@|OK|@|" if the save succeeds.
	    	// and "Tyv�rr! Fel kod.|@|OK|@|" if it is incorrect.
			Pattern p = Pattern.compile(".*Grattis.*", Pattern.CASE_INSENSITIVE);
			Matcher m = p.matcher(responseString);
			
			if (m.find()) {
				Log.w(CLASSNAME, "The code is registered at Stamford.");
				result = true;
			}
			else {
				Log.w(CLASSNAME, "The code does not match at Stamford");
			}
	    }
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}

        httpClient.getConnectionManager().shutdown();
		
		return result;
	} // end registerControlAtStamford()
	

	/**
	 * Updates the given user account at the Stamford server and the GAE server()
	 * 
	 * @param userName a String that is the username for the user.
	 * @param userPassword a String that is the password for the user.
	 * @param argFormparams a List<NameValuePair> that contains the information about the user to send to the server
	 * 
	 * @return a boolean that indicates whether the update was a success or not.
	 */
	public boolean updateUserAccountStamford(String userName, String userPassword, List<NameValuePair> argFormparams) {
		
		boolean result = false;

		// Login to get the session cookies.
		String[] cookieStrings = loginUserAtStamford(userName, userPassword);
		httpClient = new DefaultHttpClient();
		
		// Setup the target host to send the xml data to.
		HttpPost httpPost = new HttpPost("/scripts101c/cgiip.exe/WService=vhockey/oktyr/oktyr_ajax.r");
		httpPost.setHeader("Cookie", cookieStrings[0] + "; " + cookieStrings[1]);
		
		try {
    		List<NameValuePair> formparams = argFormparams;
    		String extractedCrcUserIdCookie = cookieStrings[1].substring(cookieStrings[1].indexOf("=") + 1, cookieStrings[1].length());
        	formparams.add(new BasicNameValuePair("crc", extractedCrcUserIdCookie));
    		Log.i(CLASSNAME, "extractedCrcUserIdCookie: " + extractedCrcUserIdCookie);
        	formparams.add(new BasicNameValuePair("rnd", "8315151"));
        	
    		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, HTTP.ISO_8859_1);
    		entity.setContentType("application/x-www-form-urlencoded");
	    	httpPost.setEntity(entity);
	    	
	    	HttpResponse response = httpClient.execute(targetHostStamford, httpPost);
	    	String responseString = EntityUtils.toString(response.getEntity());
	    	Log.i(CLASSNAME, "Responsestring: " + responseString);
			// Note! The pattern has to be changed if stamford changes the response code. 
	    	// Right now the server responds "Din �ndringar har sparats.|@|OK|@|" if the save succeeds.
	    	// and "Fel l�senord.|@|ERROR|@|" if it is incorrect.
			Pattern p = Pattern.compile(".*har sparats.*", Pattern.CASE_INSENSITIVE);
			Matcher m = p.matcher(responseString);
			
			if (m.find()) {
				result = true;
			}
			else {
				Log.e(CLASSNAME, "The password given does not match at Stamford");
			}
	    }
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}

        httpClient.getConnectionManager().shutdown();

		return result;
	} // end updateUserAccountStamford()
	
	
	/**
	 * Register a new user to the server at Stamford.
	 * 
	 * @param theFormparams a List<NameValuePair> that contains information about the user.
	 * @return a boolean indicating whether the registration was successful or not
	 */
	public String registerUserStamford(List<NameValuePair> theFormparams) {
		Log.i(CLASSNAME, "registerUserStamford() Registering new user.");

		String result = "";
		
		httpClient = new DefaultHttpClient();
		
		HttpPost httpPost = new HttpPost("/scripts101c/cgiip.exe/WService=vhockey/oktyr/oktyr_ajax.r");
    	try {
    		List<NameValuePair> formparams = theFormparams;
    		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, HTTP.ISO_8859_1);
    		entity.setContentType("application/x-www-form-urlencoded");
	    	httpPost.setEntity(entity);
	
	    	HttpResponse response = httpClient.execute(targetHostStamford, httpPost);
	    	String responseString = EntityUtils.toString(response.getEntity());
	    	Log.i(CLASSNAME, "This is the response: " + responseString);
	    	
			// Note! The pattern has to be changed if stamford changes the response code. 
	    	// Right now the server responds "Tack f�r att du registrerat dig p� Friska Karlstad. 
	    	// Nu kan du logga in.|@|OK|@|" if the save succeeds.
			Pattern p = Pattern.compile(".*Nu kan du logga in.*", Pattern.CASE_INSENSITIVE);
			Matcher m = p.matcher(responseString);
			if (m.find()) {
				result = "success";
			}
			
			Pattern p_err1 = Pattern.compile(".*redan.*", Pattern.CASE_INSENSITIVE);
			Matcher m2 = p_err1.matcher(responseString);
    		if (m2.find()) {
    			result = "email_exists";
    			Log.e(CLASSNAME, "The e-mail address is already in use");
			}
			
    		Pattern p_err2 = Pattern.compile(".*upptaget.*", Pattern.CASE_INSENSITIVE);
			Matcher m3 = p_err2.matcher(responseString);
    		if (m3.find()) {
    			result = "username_exists";
				Log.e(CLASSNAME, "The user name already exists");
			}
    	}
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}

        httpClient.getConnectionManager().shutdown();
        
        return result;
	} // end registerUserStamford()
	
} // end class RestClient
