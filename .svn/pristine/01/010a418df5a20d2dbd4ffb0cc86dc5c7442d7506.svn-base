<HTML>

  <HEAD>

    <TITLE>Om oss</TITLE>

  </HEAD>

  <BODY>

<h2>Hjälp - Huvudmeny</h2>

<p>
Det här är huvudmenyn f�r Friska Karlstad. Nedan finner du en kort genomg�ng av dom olika menyvalen.
</p>

<ul>
<li>
<a href="#karta">Karta</a>
</li>
<li>
<a href="#kontrollista">Kontrollista</a>
</li>
<li>
<a href="#nyheter">Nyheter</a>
</li>
<li>
<a href="#ruttinspelning">Ruttinspelning</a>
</li>
<li>
<a href="#konto">Konto</a>
</li>
<li>
<a href="#statistik">Statistik</a>
</li>
</ul>

<h3 id="karta">Karta <a href="#top">[Upp]</a></h3>
<p>H�r visas en orienteringskarta d�r dom olika kontrollerna �r utplacerade och visas i form av rosa ringar. Du kan zooma in 
och ut i kartan genom att anv�nda "pinch"-zoom eller med hj�lp av de zoom-knappar som syns l�ngst ner till v�nster p� sk�rmen. 
F�r att v�lja en kontroll p� kartan trycker du med fingret p� kontrollringen. Du kommer d� till kontrollregistreringen d�r du 
kan scanna (eller skriva in koden manuellt) och registrera kontrollen hos Friska Karlstad.
</p>

<p>
Observera att f�r att kartan ska synas m�ste du f�rst ladda ner den fr�n Internet. D� kartbitarna kr�ver en hel del utrymme (15Mb) 
�r det gjort s� att du f�r sj�lv v�lja om du vill ladda ner dom om du vill. I annat fall kan du anv�nda google maps befintliga kartor.
Ett tips �r att befinna dig uppkopplad mot ett Wifi-n�tverk n�r du g�r nedladdningen. P� s� s�tt sparar du lite p� din nedladdningskvot 
(vars storlek varierar beroende p� abbonemang). Det g�r dessutom fortare.
</p>

<p>Kartan har ett antal inst�llningar som du kan �ndra p� fr�n menyvalet "Inst�llningar".</p>

<ul>
<li>Visa min position - En bl� prick markerar din position p� kartan. Prickens position uppdateras ocks� n�r du flyttar 
	dig. Underl�ttar vid sv�ra kontroller.</li>
<li>Visa m�ttstock - Visar en liten m�ttstock s� att du kan uppskatta avst�ndet mellan tv� punkter p� ett ungef�r.</li>
<li>Visa kompass - Visar en liten kompass p� kartan. (Kompassen fungerar b�st n�r GPS �r p�slagen).</li>
<li>G�m registrerade kontroller - Med den h�r kan du g�mma redan registrerade kontrollers.</li>
</ul>

<h3 id="kontrollista">Kontrollista <a href="#top">[Upp]</a></h3>
<p>H�r visas alla kontroller i listform. Det �r markerat med ett kryss om du har registrerat kontrollen. Du v�ljer en kontroll 
genom att trycka p� den och kommer d� till vyn f�r att registrera den valda kontrollen.
</p>

<p>
Fr�n menyn (visar sig om du trycker p� menyknappen) g�r det att g�mma redan registrerade kontroller s� att listan blir 
kortare.
</p>

<h3 id="nyheter">Nyheter <a href="#top">[Upp]</a></h3>
<p>En sida d�r nyheter f�r Friska Karlstad publiceras. Nyheter som du inte sett f�rut kommer att dyka upp i en popup ruta 
f�r att du inte ska missa dom.
</p>

<p>
Fr�n menyn (visar sig om du trycker p� menyknappen) g�r det att g�mma redan registrerade kontroller s� att listan blir 
kortare.
</p>

<p>
I kontrollistan kan du ocks� att se om n�gon kontroll som du scannat inte har lyckats registrerats p� Friska Karlstads server. 
�r s� fallet kan du v�lja att f�rs�ka ladda upp dem genom att v�lja "Synkronisera med Friska Karlstad" i menyn. Att det inte 
har g�tt att ladda upp kontroller p� f�rsta f�rs�ket kan bero p� att det varit d�lig mobilt�ckning vid tidpunkten d� du 
scannade kontrollen och trycke registrera.
</p>

<h3 id="ruttinspelning">Ruttinspelning <a href="#top">[Upp]</a></h3>
<p>
Den h�r funktionen "spelar in" hur l�ngt du g�tt och l�gger det till din totala inspelade str�cka. Du startar den genom att trycka
p� knappen s� att den gr�na bollen blir r�d. N�r den �r r�d s� spelas rutten in. F�r att stoppa inspelningen, tryck en g�ng till p�
knappen s� att den blir gr�n. Den totala distans du tillryggalagt (nu + tidigare inspelningar) kommer att visas l�ngst ner till 
v�nster p� sk�rmen.
</p>

<p>
En f�ruts�ttning f�r att det ska bli en exaktare
uppskattning av din totala f�rdl�ngd �r att du sl�r p� GPSn n�r du anv�nder denna funktion. Observera att om du byter konto i din
telefon s� kommer den h�r informationen att nollst�llas eftersom informationen lagras lokalt i telefonen och �r knuten till det
nuvarande kontot.
</p>

<h3 id="konto">Konto <a href="#top">[Upp]</a></h3>
<p>
N�r du laddat ner och installerat applikationen p� telefonen s� �r du oregistrerad. F�r att kunna b�rja registrera kontroller 
beh�ver du logga in p� ett befintligt konto p� Friska Karlstad eller skapa ett nytt konto.
</p>
<h4>Skapa nytt konto</h4>
<p>
Tryck p� knappen m�rkt "Konto" i huvudmenyn och v�lj "Skapa nytt konto". Fyll i alla f�lt (f�lt markerade med * �r obligatoriska) och tryck p� 
registrera.
</p>
<h4>Logga in p� befintligt Friska Karlstad-konto</h4>
<p>
Tryck p� knappen m�rkt "Konto" i huvudmenyn och v�lj "Logga in". Ange ditt befintliga l�senord och anv�ndarnamn och tryck p� logga in. 
Dina registrerade kontroller och anv�ndarinformation kommer nu att h�mtas fr�n Friska Karlstad. Observera att det kan ta 
ett litet tag att synkronisera kontona f�rsta g�ngen.
</p>
<h4>�ndra ett befintligt konto</h4>
<p>
Om du beh�ver �ndra informationen f�r ett befintligt konto tryck p� knappen m�rkt "Konto" i huvudmenyn och v�lj 
"�ndra befintligt konto"-knappen. Dina uppgifter (f�rutsatt att du registrerat dig i applikationen med n�gon av ovanst�ende 
metoder innan) kommer att visas p� sk�rmen. Du kan uppdatera alla f�lt utom anv�ndarnamn. N�r du �r klar med dina �ndringar, 
tryck p� knappen uppdatera.
</p>

<h3 id="statistik">Statistik <a href="#top">[Upp]</a></h3>
<p>
Statistiksidan visar f�r n�rvarande de 100 deltagare som tagit flest kontroller. F�r att uppdatera listan tryck på knappen. Det 
finns mer statistik att hämta på Friska Karlstads hemsida (http://www.friskakarlstad.se) bl.a statistik för lagtävlingen.
</p>
</BODY>

</HTML>