package jonas.com.friskakarlstad.activities;

// TODO Make a check if the map tiles are alreay in place. Might require to store the location of the map tiles at first download.
// TODO Create a function that will report error printouts to my e-mail with the error printout as attachment. For debug.
// TODO Implement a function that shows an iformational dialog for specific events that have occurred, e.g. lost control.
//      special conditions or other news. Implement it as a check of a small information bit that says <WHAT><SEQINT>
//      <WHAT> could be controls or info. SEQINT is an increasing number so when the number increases so does the SEQINT
//      This should be available as a control in the admin app also. Make it manual to be able to handle it in a good way
//      e.g. put up multiple controls before publishing the update info.

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import jonas.com.friskakarlstad.R;
import jonas.com.friskakarlstad.R.drawable;
import jonas.com.friskakarlstad.R.id;
import jonas.com.friskakarlstad.R.layout;
import jonas.com.friskakarlstad.R.menu;
import jonas.com.friskakarlstad.pojos.Control;
import jonas.com.friskakarlstad.pojos.ControlInfo;
import jonas.com.friskakarlstad.pojos.RegisteredMobileUser;
import jonas.com.friskakarlstad.pojos.UnregisteredMobileUser;
import jonas.com.friskakarlstad.pojos.NewsFlash;
import jonas.com.friskakarlstad.utilities.FileHandler;
import jonas.com.friskakarlstad.utilities.RegisteredUserXmlHandler;
import jonas.com.friskakarlstad.utilities.RestClient;
import jonas.com.friskakarlstad.utilities.UnregisteredUserXmlHandler;

import org.apache.http.HttpEntity;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

/**
 * The main menu for this application. From here the user can navigate to the sub views to 
 * register controls, check the map etc.
 * 
 * This view also handles synchronization of information with the server. On create it will
 * download the user object for the project and go through the information to ensure it is
 * up to date.
 * 
 * @author jge
 */
public class MainMenuView extends Activity {
	
	// Tag for Log printouts.
	public static final String CLASSNAME = "MainMenuView";
	
	// Dialog id for alterting the user to downloading the map tiles from the server on first use.
	public static final int DOWNLOAD_MAP_TILES_ON_FIRST_USE_ALERT = 0;
	
	// Dialog id for alterting the user to downloading the map tiles from the server after first use.
	public static final int DOWNLOAD_MAP_TILES_AFTER_FIRST_USE_ALERT = 1;
	
	// Dialog id for alerting the user to switch on GPS.
	public static final int GPS_NOT_ENABLED_ALERT = 2;
	
	// Dialog id for alerting the user that the SD-card is not mounted.
	public static final int MEDIA_NOT_MOUNTED_ALERT = 3;
	
	// Dialog id for alerting the user that the SD-card is read only.
	public static final int MEDIA_READONLY_ALERT = 4;
	
	// Dialog id for alerting the user that the control list could not be downloaded/updated.
	public static final int SERVER_UNREACHABLE = 5;
	
	// Activity id for changing the settings for the GPS.
	public static int CHANGE_GPS_SETTING_REQ = 6;
	
	// Dialog id for alerting the user that the phone is locked for file transfer since 
	// it is shared with a host computer via USB
	public static final int MEDIA_USB_SHARED = 7;
	
	// Heading for this view.
	private TextView tvMainMenuLabel;
	
	// Text view notifying the user that he/she is not registered.
	private TextView tvCurrentUser;
	
	// Starts the map view.
	private Button btnOpenMapView;
	
	// Start the control list view.
	private Button btnOpenControlList;
	
	// Starts the view for scanning controls.
	private Button btnOpenScanControlView;
	
	// Starts the view for account administration.
	private Button btnOpenRegisterLoginView;
	
	// Starts the information view to display information about Friska Karlstad.
	private Button btnOpenInformationView;

	// Starts recording the route.
	private Button btnRecordRoute;
	
	// Http client that handles communication with remote servers.
	private RestClient restClient;
	
	private LocationManager locationManager;
	private LocationListener locationListener;
	
	// Indicates whether the user is currently recording the route or not.
	private boolean isRouteRecording;
	
	// Shows the total number of controls the user has registered so far.
	private TextView tvNrOfControlsTaken;
	
	// Progress dialog for download of map tiles.
	private ProgressDialog progressDialog;
	
	private SharedPreferences sharedPreferences;
	private SharedPreferences.Editor editor;
	
	// Used for distance calculation. This locaation is the previous location of the user.
	private Location previousLocation;
	
	// Shows the total distance accumulated by the user.
	private TextView tvTotalDistance;
	
	// The accumulated distance travelled for this user.
	private float currentTotalDistance;
	
	// An object containing project information for an unregistered user.
	private UnregisteredMobileUser unregisteredUser;
	
	// An object containing project information and account information for a registered user.
	private RegisteredMobileUser registeredUser;
	
	private ArrayList<NewsFlash> news;
		
	// Loaded from the preference attribute "user_name". If this is null after onCreate 
	// it means the user is not registered.
	private String userName;
	
    private PopupWindow newsPopUpWindow;

    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu_view_layout);
        
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		editor = sharedPreferences.edit();
		userName = sharedPreferences.getString("username", null);
    	isRouteRecording = false;
        
        
		setUpGraphicalComponents();
		
		// Show the registered user name if the user has been registered.
        if (userName == null) {
			tvCurrentUser.setText("Inte registrerad");
		}
		else {
			tvCurrentUser.setText("Inloggad: " + userName);
		}

		tvTotalDistance.setText("Distans: " + getTotalDistanceAccumulated() + " km");
        
		restClient = new RestClient();


		
		// Get the latest information from the server and the user account if available.
        // Do this in a separate thread to avoid blocking the UI.
		// TODO Refactor this into a function that checks for update (any update) and then updates the control list
		//      Form <WHAT><INT> So <CONTROLS><1> store the seqiental number and if increase, update the object.
		//      Could also be info.
		new SynchWithServerAsyncTask().execute();
		
    } // end onCreate()
    
    /**
     * Downloads project information for an unregistered user.
     * 
     * @return a boolean indication if the download was a success (true) or not (False)
     */
    private boolean downloadProjectForUnregisteredUser() {
    	boolean result = false;

    	String serverXMLResponse = restClient.getUnregisteredUser();
    	
    	// TODO: Make a method to check valid response in parser.
    	// Parse the response from the server and assign to the unregistered
    	// user object
    	UnregisteredUserXmlHandler xmlParser = new UnregisteredUserXmlHandler();
    	unregisteredUser = xmlParser.parseXml(serverXMLResponse);
    	
    	if (unregisteredUser != null) {
    		result = true;
    		
    		// TODO Save lat long and use it to place the map converted to tile index
		    // editor.putInt("lat_position_map", unregisteredUser.getMapObject().getLatitude());
		    // editor.commit("long_position_map" unregisteredUser.getMapObject().getLongitude());
    	}
    	return result;
    } // end downloadProjectForUnregisteredUser()
    
    
    /**
     * Downloads account and project information for a registered user
     * 
     * @return a boolean that indicates if the download was successful (true) or failed (false).
     */
    private boolean downloadProjectForRegisteredUser() {
    	boolean result = false;

		String password = sharedPreferences.getString("password", null);
    	
    	String serverXMLResponse = restClient.getRegisteredUser(userName, password);
    	
    	// TODO: Make a method to check valid response in parser.
    	// Parse the response from the server and assign to the unregistered
    	// user object
    	RegisteredUserXmlHandler xmlParser = new RegisteredUserXmlHandler();
    	registeredUser = xmlParser.parseXml(serverXMLResponse);
    	
    	return result;
    } // end downloadProjectForRegisteredUser()
    
    
    /**
     * Inner class used to contact the Friska Karlstad server to download the control list 
     * and the user account from the server asynchronously.
     * 
     */
    private class SynchWithServerAsyncTask extends AsyncTask<Void, Void, Boolean> {
    	
    	
    	@Override
		protected void onPreExecute() {} // end onPreExecute()

    	
        @Override
		protected Boolean doInBackground(Void... params) {
        	boolean result = false;
        	
        	// If the user is not registered, only download the project information.
        	// Else, download the registered user account.
        	if (userName == null) {
        		result = downloadProjectForUnregisteredUser();
        	}
        	else {
        		result = downloadProjectForRegisteredUser();
        	}
        	
        	return result;
        } // end doInBackground()
		
        
        protected void onProgressUpdate() {}

        
        protected void onPostExecute(Boolean result) {
        	if (result != true) {
				showDialog(SERVER_UNREACHABLE);
        	}
        	else {	
        			firstTimeCheckForMapTiles();
        			updateNews();
        			synchronizeRegisteredControls();
        			saveDownloadedProjectInformation();
        	}
        }        
    } // end class SynchWithServerAsyncTask

    
    /**
     * Saves the downloaded project information for an unregistered user locally
     * and project and account information for a registered user.
     */
    private void saveDownloadedProjectInformation() {
    	
    	String fileName;
    	
    	if (userName == null) {
    		fileName = FileHandler.UNREGISTERED_USER_FILENAME;
    	}
    	else {
    		fileName = FileHandler.REGISTERED_USER_FILENAME;
    	}
    	
		try {
			FileHandler.writeObjectToFile(unregisteredUser, openFileOutput(fileName, Context.MODE_PRIVATE));
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    	
    } // end saveDownloadedProjectInformation()
    
    
    /**
     * Opens a popup window to display news
     * 
     * @param newsTexts a String that contains the news to display.
     */
    private void initiatePopupWindow(String newsTexts) {
    	
        try {
//            //We need to get the instance of the LayoutInflater, use the context of this activity
//            LayoutInflater inflater = (LayoutInflater) MainMenuView.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            // create a 300px width and 470px height PopupWindow
//            newsPopUpWindow = new PopupWindow(inflater.inflate(R.layout.news_popup_layout, null, false), 300, 470, true);
//            
//            TextView tvPopupHeader = (TextView) newsPopUpWindow.getContentView().findViewById(R.id.newsPopupHeading);
//            TextView tvNewsBody = (TextView) newsPopUpWindow.getContentView().findViewById(R.id.newsPopupBodyText);
//             String getTheFuckingText = tvNewsBody.getText().toString();
//            Button btnCloseNewsPopup = (Button) newsPopUpWindow.getContentView().findViewById(R.id.btnCloseNewsPopup);
//            btnCloseNewsPopup.setOnClickListener(new View.OnClickListener() {
//                public void onClick(View v) {
//                	((PopupWindow)v.getParent()).dismiss();
//                }
//            });
//            
//            
//         // display the popup in the center
//            View popupView = findViewById(R.id.mainmenuview_root_container);
//            newsPopUpWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);  
//            
//            tvPopupHeader.setText("Bomanan");
//            tvNewsBody.setText("Bananas");
//         
//     
//                     

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    
    
    /**
     * Selects which news to display to the user at application startup. The selection is based on a saved
     * date which is taken from the latest shown news. Any news downloaded that are newer than this
     * date are displayed in a popup.
     */
    private void updateNews() {
    	
    	String latestnewsFlash = sharedPreferences.getString("latest_newsflash", "2012-01-01T12:00:00.000+0100");
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    	Date latestNewsEvent;
    	
    	StringBuilder newsFeed = new StringBuilder();
    	
		try {
			latestNewsEvent = dateFormatter.parse(latestnewsFlash);
	    	news = unregisteredUser.getNewsFlashes();
	    	
	    	for (NewsFlash currentNewsFlash : news) {

	    		Date currentNewsFlashDate = dateFormatter.parse(currentNewsFlash.getTimeStamp());
	    		
	    		if (latestNewsEvent.before(currentNewsFlashDate)) {
	    			newsFeed.append(currentNewsFlash.getHeader() + "\n");
	    			newsFeed.append(currentNewsFlash.getDescription() + "\n");
	    		}
	    	}
		}
    	catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		initiatePopupWindow(newsFeed.toString());
    	//2012-03-24T12:00:00.000+0100
    } // end updateNews()
	
	
	/**
	 * Syncronizes controls between the mobile application and
	 * the server. This method triggers when for example the 
	 * user has registered controls without contact with the 
	 * server (uploading controls to server) or when the user
	 * has registered controls from a PC (updating the registered
	 * controls locally.
	 */
	@SuppressWarnings("unchecked")
	private void synchronizeRegisteredControls() {
		
		if (userName != null) {
			
			// Get the locally stored controls.
			ArrayList<Control> localControls = null;
			try {
				
				localControls = 
						(ArrayList<Control>) FileHandler.getObjectFromFile(openFileInput(FileHandler.REGISTERED_USER_FILENAME));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
			// Get the controls downloaded from the server for this session.
			ArrayList<Control> downloadedControls = registeredUser.getControls();
			
			// Create a list of controls to send to the server. 
			ArrayList<Control> controlsToSendToServer = new ArrayList<Control>();
			
			if (localControls != null && downloadedControls != null) {
				
				// If the local registered control cannot be found in the server list
				// it means that it is only stored locally in the mobile application.
				// Add them to the list to be sent to the server for registration.
				for (Control currentLocalControl : localControls) {
					
					boolean controlFound = false;
					
					for (Control currentDownloadedControl : downloadedControls) {
						
						if (currentLocalControl.getControlNumber() == currentDownloadedControl.getControlNumber()) {
							controlFound = true;
							break;
						}
						
					}
					
					if (controlFound == false) {
						controlsToSendToServer.add(currentLocalControl);
					}
					
					
				}
			}
			
			// Send the controls that are only registered in the mobile to the server.
			String password = sharedPreferences.getString("password", null);
	    	restClient.registerControls(controlsToSendToServer, userName, password, registeredUser.getProjectName());
			
			// To find out which controls have been registered at a PC that we need to update
			// local control list with, simply merge the two lists.
			// This is accomplished by making a unique set of both control lists and store it 
			// in the user object if all registration updates went well.
			localControls.addAll(downloadedControls);
			
			@SuppressWarnings("unchecked")
			Set uniqueSet = new HashSet(localControls);
			
			editor.putInt("controls_taken", 0);
		    editor.putInt("total_nr_controls", uniqueSet.size());
		    editor.commit();		
			
			// Set the controlList to be the updated unique set.
			registeredUser.setControls(new ArrayList<Control>(uniqueSet));

		}
		
		updateControlsTaken();
	} // end synchronizeRegisteredControls()

    
    /**
     * Updates the textview showing numbers of controls already registered for this user.
     * TODO Replace this with a loop through the control list to get the latest picture.
     */
    private void updateControlsTaken() {
    	int controlsTaken = sharedPreferences.getInt("controls_taken", 0);
		int controlsTotal = sharedPreferences.getInt("total_nr_controls", 0);
    	tvNrOfControlsTaken.setText("Klara kontroller: " + controlsTaken + "/" + controlsTotal);
    } // end updateControlsTaken()
    
    
    /**
     * Class used to download map tiles as a Zip file and unpack them to the SD-card asynchronously 
     * in a separate thread from the UI-thread.
     */
    private class DownloadMapTilesAsyncTask extends AsyncTask<Void, Integer, String> {
    	
    	@Override
		protected void onPreExecute() {
    		showProgressDialog("Laddar ner karta", "Laddar ner kartan fr�n Friska Karlstad. V�nligen v�nta...");
        } // end onPreExecute()

        @Override
		protected String doInBackground(Void... params) {
            	
        	String result = "failed";
        	String sdCardState = FileHandler.checkExternalStorageAvailability();
        	
        	if (sdCardState.equals(Environment.MEDIA_UNMOUNTED) || 
        		sdCardState.equals(Environment.MEDIA_MOUNTED_READ_ONLY) ||
        		sdCardState.equals(Environment.MEDIA_SHARED)) {
        		result = sdCardState;
        	}
        	else {
    	
	        	// Create the base directory where we want to write the map tiles to.
			    File mapTilesBaseDiractory = new File(Environment.getExternalStorageDirectory() + 
			    		                     RestClient.DATA_STORAGE_BASE);
			    mapTilesBaseDiractory.mkdirs();
			    
			    try {
		        	// Get the zip file.
		        	HttpEntity theZipFile = restClient.downloadMapTiles(unregisteredUser.getMapObject().getMapLink());

		        	ZipEntry entry = null;
			    	ZipInputStream zin = new ZipInputStream(theZipFile.getContent());
			    	BufferedOutputStream outBuf = null;
			    	
			    	int fileCounter = 0;
			    	int BUF_SIZE = 1024;
			    	
			    	// Read through all the entries in the zip file and unzip to their respective directory
			    	while ((entry = zin.getNextEntry()) != null) {
				    	
			    		publishProgress(++fileCounter);
			    		
				    	File newFile = new File (mapTilesBaseDiractory.getPath() + "/" + entry.getName() + "/");
			    		
				    	//If it is a directory, create it else if it is a file, write that file.
				    	if (!newFile.getName().endsWith("png")) {
				    		newFile.mkdir();
				    	}
				    	else {
				    		outBuf = new BufferedOutputStream(new FileOutputStream(newFile), BUF_SIZE);
				    		
					    	int count;
				            byte data[] = new byte[BUF_SIZE];
				            
				            // write the files to the disk
				            while ((count = zin.read(data, 0, BUF_SIZE)) != -1) {
				            	outBuf.write(data, 0, count);
				            }
				            outBuf.flush();
				            outBuf.close();
				    	}
			    	}
			    
			    zin.close();
			    
	    		// Save the number of map tiles downloaded to SD-card. This will be used as an indicator for that all
			    // necessary map tiles have been downloaded.
	    		// TODO Create a constant which indicates the expected number of files (map tiles) to compare with fileCounter.
			    editor.putInt("nr_of_map_tiles", fileCounter);
			    editor.commit();
			    
			    result = "ok";
			    }
				catch (IOException e1) {
					e1.printStackTrace();
				}
        	}
			return result;
        } // end doInBackground()
		
        protected void onProgressUpdate(Integer... fileCount) {
        	// TODO Replace with constant instead of 1141
        	progressDialog.setMessage("Sparat: " + fileCount[0] + " av 1141 kartbitar.");
        }

        protected void onPostExecute(String result) {
        	progressDialog.dismiss();
        	
        	if (result.equals("media_not_mounted")) {
        		showDialog(MEDIA_NOT_MOUNTED_ALERT);
        	}
        	else if (result.equals("mounted_ro")) {
        		showDialog(MEDIA_READONLY_ALERT);
        	}
        	else if (result.equals("shared")) {
        		showDialog(MEDIA_USB_SHARED);
        	}
        	else if (result.equals("ok")) {
        		showToast("Nedladdningen �r klar.");
        	}
        	else {
        		showToast("Nedladdningen misslyckades.");
        	}
        }
        
    } // end class DownloadControlListAsyncTask
    
    
    /**
     * Displays a toast message on screen
     * @param toastText a String that is the text to display.
     */
    private void showToast(String toastText) {
    	int duration = Toast.LENGTH_SHORT;
    	Toast toast = Toast.makeText(this, toastText, duration);
    	toast.show();
    } // end showToast()
    
    
    /**
     * Starts the recording of accumulated distance. Measurement is preferrably measured by the GPS which is why
     * the user is asked to switch it on if not active.
     */
    private void startDistanceRecording() {
    	
    	btnRecordRoute.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.route_recording), null, null);

    	// Initiate the current total distance to the value saved from last recording session.
    	currentTotalDistance = sharedPreferences.getFloat("total_distance", (float)0.0);
    	
    	locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
    	locationListener = new LocationListener() {
			
		    public void onLocationChanged(Location newLocation) {
		    	
		    	if (previousLocation != null) {
		    		float newDistanceIncrease = previousLocation.distanceTo(newLocation);
		    		
    	        	// Sum up this sessions distance with the total distance and save it to preferences.
    	    		currentTotalDistance += newDistanceIncrease;
    	    		
    	    		// Save the update to the preferences.
    	    		editor.putFloat("total_distance", currentTotalDistance);
    			    editor.commit();
    			    
    			    // Update the total distance textview.
    			    tvTotalDistance.setText("Distans: " + getTotalDistanceAccumulated() + " km");
		    	}
		    	previousLocation = newLocation;
		    }

		    public void onStatusChanged(String provider, int status, Bundle extras){}
		    public void onProviderEnabled(String provider){}
		    public void onProviderDisabled(String provider){}
		};
    	
	    // Check if GPS is activated. If not, offer to the user to switch it on.
	    if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
	    	showDialog(GPS_NOT_ENABLED_ALERT);
   	    	locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 30000, 100, locationListener);
	    }
	    else {
	    	locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 30000, 100, locationListener);
	    }
	    
    	// Change the indicator for active recording.
    	isRouteRecording = true;
    } // end startDistanceRecording()
    
    
    /**
     * Stops recording the accumulated distance.
     */
    private void stopDistanceRecording() {

    	btnRecordRoute.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.route_not_recording), null, null);
    	locationManager.removeUpdates(locationListener);
    	previousLocation = null;
        isRouteRecording = false;
    } // end stopDistanceRecording()
    
    
    /**
     * Displays a progress dialog for the user.
     * 
     * @param title a String that is the title of the progress dialog.
     * @param description a String that is the descriptive text for the dialog.
     */
	private void showProgressDialog(String title, String description){

		// Start the progress dialog for download of controls from Stamford.
		progressDialog = ProgressDialog.show(this, title, description, 
												true, true, new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				progressDialog.dismiss();
			}
		});
	} // end showProgressDialog()

    public void synchronizeUserAccount() {
    	
    	// Check if the user is registered or not.
        String storedUserName = sharedPreferences.getString("user_name", null);
        
    	if (storedUserName != null) {}
    		
    	else {
    		
    		// Check if there has been any updates to the controlinfo list
        	int controlInfosVersion = sharedPreferences.getInt("control_informations_version", 0);
        	
        	// If the controlinfosversion is 0 it means this is the first time the user opens the
        	// program and the controlinfos should always be downloaded in this case.
        	if (controlInfosVersion == 0) {
        	}
        	else if (controlInfosVersion >= unregisteredUser.getControlInfosVersion().intValue()) {
        		
        	}
        	else {
        		updateLocalControls();
        	}
    	}
    } // end synchronizeUserAccount()
    
    private void updateLocalControls() {
    	
    	
    } // end updateLocalControls()

    
    /**
     * Starts an activity based on which TextView was clicked.
     * 
     * @param selection an integer that represents the view to open.
     */
    private void openSubView(int selection) {
    	switch(selection) {
    		case 0:
    			startActivity(new Intent (this, MapNavigatorView.class));
				break;
    		case 1:
    			startActivity(new Intent (this, ControlListView.class));
				break;
    		case 2:
    			startActivity(new Intent (this, RegisterControlView.class));
				break;
    		case 3:
    			startActivity(new Intent (this, LoginOrRegisterView.class));
				break;
    		case 4:
    			Intent browserIntent = new Intent(this, InformationView.class);
    			browserIntent.putExtra("browser_url", "file:///android_asset/index.html");
    			startActivity(browserIntent);
				break;

    		default:
    			Log.e(CLASSNAME, "No such view");
    			break;
    	}
    } // end openSubView()
	
    
    /**
     * The first time the user starts the application after installation, the user is asked if he/she wants 
     * to download the map tiles and store them on the SD-card right away or wait.
     */
    private void firstTimeCheckForMapTiles() {
    	
		// TODO: This must be moved until after project has been downloaded.
    	// If this is the first time the application is started. Offer the user to download the map
		// tiles to the external memory card. If user answers no, the user have to ask for download
		// of map tiles from menu.
		boolean firstTimeUse = sharedPreferences.getBoolean("first_time_use", true);
		if (firstTimeUse == true) {
			
			// Check that this is just not a re-installation because then all map files may be downloaded
			// already. This is checked by examining if the base directory for the map tiles exists on the SD-Card
			// TODO Move check of external storage to utilility file.
			String sdCardState = Environment.getExternalStorageState();
			
			if (!sdCardState.equals(Environment.MEDIA_UNMOUNTED) || 
	        		!sdCardState.equals(Environment.MEDIA_MOUNTED_READ_ONLY) ||
	        		!sdCardState.equals(Environment.MEDIA_SHARED)) {
				
				// TODO Improvement: Make this configurable
		    	File mapTilesBaseDiractory = new File(Environment.getExternalStorageDirectory() + RestClient.DATA_STORAGE_BASE);
				
		    	// TODO: FInd out way to determine if the user has already downloaded the map in this case instead of && !mapTilesBaseDiractory.exists() which did not work well.
				if (firstTimeUse == true ) {
					
					showDialog(DOWNLOAD_MAP_TILES_ON_FIRST_USE_ALERT);
					editor.putBoolean("first_time_use", false);
				    editor.commit();
				}
			}
			else {
				//TODO Implment dialog that tells user that the media could not be accessed.
			}
		}
    } // end checkForMapTiles()
    
    
    /**
     * Gets the total distance the user has recorded which is stored in the shared preferences.
     * 
     * @return a double that is the total distance in km recorded
     */
    private double getTotalDistanceAccumulated() {
    	
    	// Update the textview showing total distance recorded by user. Set to 0 if none.
		float totalDistanceAccumulated = sharedPreferences.getFloat("total_distance", (float)0.0);
		
		return Round((totalDistanceAccumulated / 1000), 2);
    } // end getTotalDistanceAccumulated()
    
    
    /**
     * Utility function for rounding a double.
     * 
     * @param number a double (or float) that is the number to process
     * @param decimalPlaces an integer that is the number of decimals
     * @return a double that is the rounded value
     */
    private double Round(double number, int decimalPlaces) {
    	double modifier = Math.pow(10.0, decimalPlaces);
    	return Math.round(number * modifier) / modifier;
	} // end Round()
    
    
	/**
	* Code to show dialog windows. The different dialogs are created using an int constant
	* to identify which one to open. This function is called through showDialog(int) call
	* in code.
	*/ 
    @Override
    protected Dialog onCreateDialog(int id) {
        
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);

    	switch (id) {
			case DOWNLOAD_MAP_TILES_AFTER_FIRST_USE_ALERT:
				File mapTilesBaseDiractory = new File(Environment.getExternalStorageDirectory() + RestClient.DATA_STORAGE_BASE);
    	
				String okText = null;
		    	if (mapTilesBaseDiractory.exists()) {
		    		okText = "Kartan verkar redan vara nedladdad? Forts�tta �nd�?";
		    	}
		    	else {
		    		okText = "Vill du ladda ner kartan (ca 15Mb)?";
		    	}
		    	
		    	builder.setMessage(okText)  
		    	.setCancelable(false)  
		    	.setPositiveButton("Ja", new DialogInterface.OnClickListener(){  
		    		public void onClick(DialogInterface dialog, int id){  
		    			new DownloadMapTilesAsyncTask().execute();  
		    		}  
		    	});  
		    	builder.setNegativeButton("Nej",  
		    	new DialogInterface.OnClickListener(){  
		    		public void onClick(DialogInterface dialog, int id){  
		    			removeDialog(DOWNLOAD_MAP_TILES_AFTER_FIRST_USE_ALERT);
		    		}  
		    	});  
		    	break;
			case SERVER_UNREACHABLE:
   		    	builder.setMessage("Ingen kontakt med servern. Ditt konto kunde inte h�mtas.")
   		    	.setCancelable(false)  
   		    	.setPositiveButton("Ok", new DialogInterface.OnClickListener(){  
   		    		public void onClick(DialogInterface dialog, int id){  
		    			removeDialog(SERVER_UNREACHABLE);
   		    		}  
   		    	});
   		    	break;
   			case MEDIA_NOT_MOUNTED_ALERT:
   		    	builder.setMessage("Inget minneskort tillg�ngligt")
   		    	.setCancelable(false)  
   		    	.setPositiveButton("Ok", new DialogInterface.OnClickListener(){  
   		    		public void onClick(DialogInterface dialog, int id){  
		    			removeDialog(MEDIA_NOT_MOUNTED_ALERT);
   		    		}  
   		    	});
   		    	break;
   			case MEDIA_READONLY_ALERT:
   			   	builder.setMessage("Inga skrivr�ttigheter p� minneskort.")
   		    	.setCancelable(false)  
   		    	.setPositiveButton("Ok", new DialogInterface.OnClickListener(){  
   		    		public void onClick(DialogInterface dialog, int id){  
		    			removeDialog(MEDIA_READONLY_ALERT);
   		    		}  
   		    	});  
   			   	break;
   			case MEDIA_USB_SHARED:
   			   	builder.setMessage("Minneskort monterat som disk via USB till dator. St�ll om till \"bara ladda\".")
   		    	.setCancelable(false)  
   		    	.setPositiveButton("Ok", new DialogInterface.OnClickListener(){  
   		    		public void onClick(DialogInterface dialog, int id){  
		    			removeDialog(MEDIA_USB_SHARED);
   		    		}  
   		    	});  
   			   	break;
       		case GPS_NOT_ENABLED_ALERT:
       	    	builder.setMessage("GPS �r inte p�slaget. F�r mer precis positionsbest�mning, sl� p� den.")  
       	    	.setCancelable(false)  
       	    	.setPositiveButton("Sl� p� GPS", new DialogInterface.OnClickListener(){  
       	    		public void onClick(DialogInterface dialog, int id){  
       	    			showGpsOptions();  
       	    		}  
       	    	});  
       	    	builder.setNegativeButton("Nej tack",  
       	    	new DialogInterface.OnClickListener(){  
       	    		public void onClick(DialogInterface dialog, int id){  
		    			removeDialog(GPS_NOT_ENABLED_ALERT);
       	    		}  
       	    	});  
   			   	break;
       		case DOWNLOAD_MAP_TILES_ON_FIRST_USE_ALERT:
       	    	builder.setMessage("F�r att kunna se orienteringskartan beh�ver du ladda ner den (ca 15 mb). Vill du g�ra det nu? Kartbitarna kommer att sparas p� din telefons minneskort. Du kan alltid ladda ner kartbitarna senare fr�n menyn.")
       	    	.setCancelable(false)  
       	    	.setPositiveButton("Ja", new DialogInterface.OnClickListener(){  
       	    		public void onClick(DialogInterface dialog, int id){
       	    			dialog.cancel();
       	    			new DownloadMapTilesAsyncTask().execute();
       	    		}  
       	    	});  
       	    	builder.setNegativeButton("Nej tack",  
       	    	new DialogInterface.OnClickListener(){  
       	    		public void onClick(DialogInterface dialog, int id){  
		    			removeDialog(DOWNLOAD_MAP_TILES_ON_FIRST_USE_ALERT);
       	    		}  
       	    	});   
   			   	break; 
    		default:
                break;
        }
        return builder.create();
    } // end onCreateDialog()
    
	
	@Override
	protected void onResume() {
		super.onResume();
        String storedUserid = sharedPreferences.getString("user_id", null);
		if (storedUserid != null) {
			tvCurrentUser.setText(sharedPreferences.getString("stamford_username", ""));
		}
		updateControlsTaken();
	} // end onResume()
	
	
	@Override
	protected void onRestart() {
		super.onRestart();
        String storedUserid = sharedPreferences.getString("user_id", null);
		if (storedUserid != null) {
			tvCurrentUser.setText(sharedPreferences.getString("stamford_username", ""));
		}
		updateControlsTaken();
	} // end onRestart()
    
    
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu_options_menu, menu);
        return true;
    } // end onCreateOptionsMenu()

	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
	    	case R.id.download_map_tiles_menu_option:
	    		showDialog(DOWNLOAD_MAP_TILES_AFTER_FIRST_USE_ALERT);
	        return true;
	        case R.id.options_main_help:
    			Intent browserIntent = new Intent(this, InformationView.class);
    			browserIntent.putExtra("browser_url", "file:///android_asset/main_menu_help.html");
    			startActivity(browserIntent);
	            return true;
	            
	        case R.id.options_settings:
	        	startActivity(new Intent(this, EditPreferences.class));
	            return true;
	            
	        default:
	            return super.onOptionsItemSelected(item);
        }
    } // end onOptionsItemSelected()
    
    
    /**
     * Opens the GPS-preference view.
     */
    private void showGpsOptions() {  
    	startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), CHANGE_GPS_SETTING_REQ);
    } // end showGpsOptions()
    
    
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		if (requestCode == CHANGE_GPS_SETTING_REQ) {
			
		    // Check if GPS was activated in the settings for GPS shown.
		    if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				locationManager.removeUpdates(locationListener);
				locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 30000, 100, locationListener);
		    }
		}
	} // end onActivityResult()
	
	
    /**
     * Sets up the graphical components for this activity.
     */
    public void setUpGraphicalComponents() {
    	
        tvMainMenuLabel = (TextView) findViewById(R.id.tvMainMenuHeading);
        
        // Only show this textview if user is not registered.
        tvCurrentUser = (TextView) findViewById(R.id.userName);
        
        btnOpenMapView = (Button) findViewById(R.id.btnMapNavigatorView);
        btnOpenMapView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	openSubView(0);
            }
        });
        
        btnOpenControlList = (Button) findViewById(R.id.btnControlList);
        btnOpenControlList.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	openSubView(1);
            }
        });
    	
        btnOpenScanControlView = (Button) findViewById(R.id.btnScanCheckpointView);
        btnOpenScanControlView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	openSubView(2);
            }
        });

        btnRecordRoute = (Button) findViewById(R.id.btnRecordRoute);
        btnRecordRoute.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	if (isRouteRecording == false) {
            		startDistanceRecording();
            	}
            	else {
            		stopDistanceRecording();
            	}
            }
        });

        btnOpenRegisterLoginView = (Button) findViewById(R.id.btnRegisterHandleView);
        btnOpenRegisterLoginView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	openSubView(3);
            }
        });
    	
        btnOpenInformationView = (Button) findViewById(R.id.btnInformationView);
        btnOpenInformationView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	openSubView(4);
            }
        });
        
		tvTotalDistance = (TextView) findViewById(R.id.tvTotalDistance);
		tvNrOfControlsTaken = (TextView) findViewById(R.id.tvNrOfControlsTaken);
    } // end setUpGraphicalComponents()
} // end class MainMenuView

