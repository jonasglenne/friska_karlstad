package friskakarlstad.com.activities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import friskakarlstad.com.R;
import friskakarlstad.com.pojos.UserStatistics;
import friskakarlstad.com.utilities.RestClient;
import friskakarlstad.com.utilities.Top100StatisticsXmlHandler;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

public class StatisticsView extends ListActivity {
	
	/**************************************************************
	 * Result codes for different methods called within the Activity.
	 **************************************************************/
	
	// Keep this result if no contact could be made with the server.
	static final int RESULT_UNDEFINED = -1;
		
	// Id for dialog to inform user the unlock was successful.
	static final int RESULT_WAS_OK = 1;
	
	// Dialog ID for a progress dialog.
	static final int PROGRESS_DIALOG_ID = 2;
		
	/**************************************************************
	 * Widgets for this activity.
	 **************************************************************/
	
	// Heading for this view.
	private TextView tvTop100Title;

	// Button to fetch latest update.
	private Button btnUpdateStatics;
	
	/**************************************************************
	 * Utility attributes for this activity.
	 **************************************************************/
	
	// A list with the newsflashes that is used for the list adapter.
	private ArrayList<UserStatistics> lvUserStatisticList;
	
	// A custom adapter for the listview that accepts a list of Control objects.
	private UserStatisticsListAdapter userStatisticsListAdapter;
	
	// Progress dialog to show during lengthy operations.
	private ProgressDialog requestProgressDialog;

	// Shared preferences for this application.
	private SharedPreferences sharedPreferences;
	
	// Loaded from the preference attribute "user_name". If this is null after onCreate 
	// it means the user is not registered.
	private String userName;
	
	// Http client that handles communication with remote servers.
	private RestClient restClient;
	
	@SuppressWarnings("unchecked")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.top100_statistics_listview_layout);
		
        restClient = new RestClient();
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

		// Check if there is a retained stateful object from a change in screen orientation. If not, load the list
		// from file.
		final Object retainedUserStatisticsList = getLastNonConfigurationInstance();
		
		if (retainedUserStatisticsList != null) {
	    	lvUserStatisticList = (ArrayList<UserStatistics>) retainedUserStatisticsList;
	    }
		else {
			lvUserStatisticList = new ArrayList<UserStatistics>();
		}
		
		// Find out if the user wants to hide already registered controls or not.
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        
        tvTop100Title = (TextView) findViewById(R.id.tvTop100Title);
        
        userStatisticsListAdapter = new UserStatisticsListAdapter(this, R.layout.news_listview_listrow_layout, lvUserStatisticList);
        setListAdapter(userStatisticsListAdapter);
        
        btnUpdateStatics = (Button) findViewById(R.id.btnUpdateStatics);
        btnUpdateStatics.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
        		new DownloadStatisticsAsyncTask().execute();
            }
        });
    } // end onCreate()

	
	/**
	 * Downloads top 100 users from the server.
	 * 
	 * @return an int that is the result from the fetch.
	 */
	private int downloadStatisticsFromServer() {
		
		int result = RESULT_UNDEFINED;
		String serverXMLResponse = null;
		
		userName = sharedPreferences.getString("username", null);
		String password = sharedPreferences.getString("password", null);

		if (userName != null) {
			serverXMLResponse = restClient.getTopOneHundredStatistics(userName, password);
		}
		
		else {
			serverXMLResponse = restClient.getTopOneHundredStatistics("", "");
		}
		
    	if (serverXMLResponse != null) {
    		result = RESULT_WAS_OK;
	    	// TODO: Make a method to check valid response in parser.
	    	// Parse the response from the server and assign to the unregistered
	    	// user object
	    	Top100StatisticsXmlHandler xmlParser = new Top100StatisticsXmlHandler();
	    	lvUserStatisticList = xmlParser.parseXml(serverXMLResponse);
    	}

	    return result;
	} // end downloadStatisticsFromServer()
	
	/**
	 * Updates the statistics view with fresh statistics from the server.
	 */
	private void updateUserStatisticsListView() {
		
		Collections.sort(lvUserStatisticList);
		Collections.reverse(lvUserStatisticList);
		
		userStatisticsListAdapter.clear();
    	
    	for (UserStatistics currentUserStatistics : lvUserStatisticList) {
    		userStatisticsListAdapter.add(currentUserStatistics);
    	}
    	userStatisticsListAdapter.notifyDataSetChanged();
	} // end updateUserStatisticsListView()
	
    /**
     * Asynch Task class used to get statistics from Friska Karlstad 
     */
    private class DownloadStatisticsAsyncTask extends AsyncTask<Void, Void, Integer> {
    	
    	@Override
		protected void onPreExecute() {
    		showDialog(PROGRESS_DIALOG_ID);
    	} // end onPreExecute()
    	
        @Override
		protected Integer doInBackground(Void... params) {
        	return downloadStatisticsFromServer();
        } // end doInBackground()
        
        protected void onProgressUpdate() {}
        
        protected void onPostExecute(Integer result) {
        	requestProgressDialog.dismiss();
        	if (result == RESULT_UNDEFINED) {
				showDialog(RESULT_UNDEFINED);
        	}
        	if (result == RESULT_WAS_OK) {
        		updateUserStatisticsListView();
        	}
        }        
    } // end class SynchWithServerAsyncTask
	
	/**
	 * Custom list Adapter for the listview that accepts UserStatistics objects and display their
	 * data in the list.
	 */
	private class UserStatisticsListAdapter extends ArrayAdapter<UserStatistics> {

        private ArrayList<UserStatistics> items;
        
        public UserStatisticsListAdapter(Context context, int textViewResourceId, ArrayList<UserStatistics> items) {
            super(context, textViewResourceId, items);
            this.items = items;
        } // end constructor for ControlAdapter
        
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.top100_statistics_listrow_layout, null);
            }
            
            UserStatistics currentUserStatistics = items.get(position);
            
            if (currentUserStatistics != null) {
            	
            	TextView tvTop100RowPosition = (TextView) v.findViewById(R.id.tvTop100RowPosition);
                TextView tvTop100RowUserName = (TextView) v.findViewById(R.id.tvTop100RowUserName);
                TextView tvTop100NoOfRegisteredControls = (TextView) v.findViewById(R.id.tvTop100NoOfRegisteredControls);
                
                if (tvTop100RowPosition != null) {
                	tvTop100RowPosition.setText(String.valueOf(position + 1));
        		}
                if (tvTop100RowUserName != null) {
                	tvTop100RowUserName.setText(currentUserStatistics.getUserName());
        		}
                if (tvTop100NoOfRegisteredControls != null) {
                	tvTop100NoOfRegisteredControls.setText(String.valueOf(currentUserStatistics.getNumberOfControlsRegistered()));
        		}

            }
            return v;
        }
	} // end class UserStatisticsListAdapter
    
    
	@Override
	protected Dialog onCreateDialog(int id) {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		
		switch(id) {
	        case PROGRESS_DIALOG_ID:
	        	requestProgressDialog = new ProgressDialog(this);
	        	requestProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	        	requestProgressDialog.setMessage("Hämtar statistik från Friska Karlstad...");
	            return requestProgressDialog;
	        case RESULT_UNDEFINED:
	        	builder.setMessage("Misslyckades hämta statistik.")
	        	       .setCancelable(false)
	        	       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	        	           public void onClick(DialogInterface dialog, int id) {
	        	        	   dialog.cancel();
	        	           }
	        	       });
	        	break;
	        default:
	        	break;
        }

        return builder.create();
    } // end onCreateDialog()
	
	
	// Android function to retain a stateful object if the activity is destroyed. This
	// stateful object can be retrieved if the activity restarts.
	@Override
	public Object onRetainNonConfigurationInstance() {
	    final List<UserStatistics> retainedUserStatisticsList = lvUserStatisticList;
	    return retainedUserStatisticsList;
	} // end onRetainNonConfigurationInstance()
    
} // end class StatisticsView
