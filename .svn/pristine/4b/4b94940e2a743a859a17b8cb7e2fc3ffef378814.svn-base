package jonas.com.friskakarlstad.pojos;

import java.io.Serializable;

/**
 * Class that represents a company. Is used for companies who have 
 * internal competitions within the Friska Karlstad competition.
 * 
 * @author jge
 */
public class Company implements Serializable {
	
	// Id value from the database.
	private String id;
    
	// The name of a specific company.
    private String companyName;

    /**
     * No argument constructor for class Company
     */
    public Company() {
    } // end constructor Company()

    /**
     * Constructor for class Company that takes the name of the company as argument.
     * 
     * @param theCompanyName a String that is the name of the company
     */
    public Company(String theCompanyName) {
    	companyName = theCompanyName;
    } // end constructor Company()

    // Accessors for the fields. JPA doesn't use these, but your application does.

    public String getId() {
        return id;
    }

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String theCompanyName) {
		companyName = theCompanyName;
	}
} // end class Company