package jonas.com.friskakarlstad.pojos;

import java.io.Serializable;

public class ControlInfo implements Serializable, Comparable<ControlInfo>{

	// The id for the project this control belongs to.
	// Used as identifier when submitting a registration of the control.
	private String projectId;
	
	// The id for this control.
	private String controlNumber;
	
	// The code writen on the control post.
	private String controlCode;
	
	// The description of the location for this post.
	private String description;

	// The latitude position of this control
	private double latitude;
	
	// The longitude position of this control
	private double longitude;
	
	// Specifies how many points this control is worth.
	private int controlPoint;
	
	// Indicates whether this control has been scanned by user or not.
	private boolean isScanned;
	
	// Indicates whether this control has been successfully stored on server
	// as scanned by user.
	private boolean isStoredOnServer;
	
	// A string that represents the time point the control was scanned.
	private String timeStamp;
	
	// A string that is an string representing an url to an advertisers homepage.
	private String imageLink;

	// A string that is a link to an advertisers home page.
	private String webLink;
	
	// A string that represents the difficulty level for the control (blue, green, red and black).
	private String controlColor;

    // The first date this control should be visible in the project.
    private String startDate;
    
    // The last date this control should be visible in the project.
    private String endDate;

    // Describes who can see this control.
    private String availableTo;
    
	// Constructor for class Control.
	public ControlInfo() {
	} // end constructor Control()

	// Overload constructor for class Control.
	public ControlInfo(String controlNumber, String controlCode, String timeStamp) {
		this.controlNumber = controlNumber;
		this.controlCode = controlCode;
		this.timeStamp = timeStamp;
	} // end constructor Control()
	
	
	
	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getAvailableTo() {
		return availableTo;
	}

	public void setAvailableTo(String availableTo) {
		this.availableTo = availableTo;
	}

	public int getControlPoint() {
		return controlPoint;
	}

	public void setControlPoint(int controlPoint) {
		this.controlPoint = controlPoint;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getImageLink() {
		return imageLink;
	}

	public void setImageLink(String imageLink) {
		this.imageLink = imageLink;
	}

	public String getWebLink() {
		return webLink;
	}

	public void setWebLink(String webLink) {
		this.webLink = webLink;
	}

	public String getControlColor() {
		return controlColor;
	}

	public void setControlColor(String controlColor) {
		this.controlColor = controlColor;
	}

	public String getControlNumber() {
		return controlNumber;
	}

	public void setControlNumber(String controlNumber) {
		this.controlNumber = controlNumber;
	}

	public String getControlCode() {
		return controlCode;
	}

	public void setControlCode(String controlCode) {
		this.controlCode = controlCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public boolean isScanned() {
		return isScanned;
	}

	public void setScanned(boolean isScanned) {
		this.isScanned = isScanned;
	}

	public boolean isRegisteredOnServer() {
		return isStoredOnServer;
	}

	public void setStoredOnServer(boolean isStoredOnServer) {
		this.isStoredOnServer = isStoredOnServer;
	}

	
	@Override
	public String toString() {
		return "Control [controlCode=" + controlCode + ", controlColor="
				+ controlColor + ", controlNumber=" + controlNumber + ", description="
				+ description + ", imageLink=" + imageLink + ", isScanned="
				+ isScanned + ", isStoredOnServer=" + isStoredOnServer
				+ ", latitude=" + latitude + ", longitude=" + longitude
				+ ", timeStamp=" + timeStamp + ", webLink=" + webLink + " projectId=" + projectId + "]";
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	@Override
	public int compareTo(ControlInfo another) {
		
        if (Integer.valueOf(this.controlNumber) == Integer.valueOf(another.controlNumber)) {
            return 0;
        }
        else if (Integer.valueOf(this.controlNumber) > Integer.valueOf(another.controlNumber)) {
            return 1;
        }
        else {
            return -1;
        }
	}
	
} // end class Control
